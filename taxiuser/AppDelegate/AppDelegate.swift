//
//  AppDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/18/16.
//
//

import UIKit
import Reachability
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var reachability: Reachability?
    
    //Mark:- APIs keys
    static let NearbyRadius =  500
    static let GoogleMapsAPIKey = "AIzaSyC6DvUsIahRLgFnTjUazmDMUB25NxKG1B0"
    static let GooglePlacesAPIKey = "AIzaSyC6DvUsIahRLgFnTjUazmDMUB25NxKG1B0"

    static let CountryCode = "+974"
    
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        

        window?.rootViewController = initialViewController()
        
        GMSServices.provideAPIKey(AppDelegate.GoogleMapsAPIKey)
        GMSPlacesClient.provideAPIKey(AppDelegate.GooglePlacesAPIKey)
        
        
        IQKeyboardManager.sharedManager().enable = true
        initReachability()
        
        Realm.appRealm.configureApp()
        
        return true
    }
    
    private func initialViewController() -> UIViewController? {
        let isUserLoggedIn = LoggedUser.shared != nil
        
        let userVerified =  LoggedUser.shared?.userVerified ?? false
        
        if AppConfigurations.appMode == .debug {
            print("LOGGED IN: \(isUserLoggedIn)")
            print("VERIFIED: \(userVerified)")
        }
        
        if !isUserLoggedIn ||  !userVerified {
            return AuthViewController.instanceFromStoryboard(storyboardName: "Authorization_Storyboard")
        } else {
            return SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main")
        }
    }
    
    //Reachability Object
    func initReachability(){
        self.reachability =  Reachability.forInternetConnection()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: NSNotification.Name.reachabilityChanged,
                                               object: nil)
        self.reachability?.startNotifier()
    }
    
    func reachabilityChanged(notification: NSNotification) {
        if self.reachability!.isReachableViaWiFi() || self.reachability!.isReachableViaWWAN() {
            print("Service avalaible!!!")
        } else {
            print("No service avalaible!!!")
        }
    }
}

