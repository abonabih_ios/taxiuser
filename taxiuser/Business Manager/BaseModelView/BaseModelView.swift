//
//  BaseModelView.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/7/15.
//   All rights reserved.
//

import UIKit

class BaseModelView
{
    
    lazy var cache = Cache()
    

    
    
        
    //MARK:- Intializer
    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(connectionDidBecomeDown),
                                               name: Notification.Name.notReachable,
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(connectionDidBecomeUp),
                                               name: Notification.Name.reachable,
                                               object: nil)

    }
    
    
    @objc func connectionDidBecomeDown(){
        //TODO:- pause execution here and tell the controller that Internet is down
        
        printLog(tag: .info, data: "Execution paused")
        
        pause()

         let tag =  cache.allTags().first() ?? ""
    
        let error = QLError(tag: tag, code: 0, message: StringConstant.MsgInternetDown)
        
        let userInfo = [BaseService.DataReceivedWithErrorKey:error, BaseService.ServiceName: tag] as [String : Any]

        let notification = Notification(name: Notification.Name.notReachable, userInfo: userInfo)
        
        errorDidReceived(notification: notification as NSNotification)
    }
    
    
    @objc func connectionDidBecomeUp(){
        //TODO:- resume and  execute pending services here and tell the controller to show loading if needed
        
        if cache.thereArePendingItems() {
            let notification = Notification(name: Notification.Name.reachable)
            dataDidReceived(notification: notification as NSNotification)
        }

        resume()
    }

    
    
    
    
    //MARK:- Pause/Resume excution
    func pause() {
        removeAllItemsNotification()
    }
    
    func resume() {
        for tag in cache.allTags() {
            let item = self.cache.itemWithTag(tag: tag)
           
            if let serviceName = item?.serviceClassName {
                if let serviceClass = NSClassFromString("\(Bundle.main.targetName!).\(serviceName)")  as? BaseService.Type {
                    let service = serviceClass.init()
                    
                    let params = item?.userData as? [String:Any] ?? [:]
                    
                    let headers = item?.headers ?? [:]

                    executeService(service: service, userData: params, headers: headers, tag: tag)
                }
            }

        }
    }
    
    
    private func removeAllItemsNotification(){
        for tag in cache.allTags() {

            if cache.itemWithTag(tag: tag)?.status == .onProgress {
                cache.updateStatus(tag: tag, status: .pending)
            }
            removeObserver(notificationName: tag)
        }
    }

    
    //MARK:-
    func  executeService(service: BaseService,userData: [String:Any], headers: [String:String], tag: String) {
        
        let cacheItem = cache.itemWithTag(tag: tag)
        
        if cacheItem != nil &&  cacheItem?.status == .success  && cacheItem?.response != nil {
            addObserver(notificationName: tag, callBack: #selector(didReceiveNotification))
            service.requestDidSucess(responseData: cacheItem!.response!, requestId: tag)
        }
        else {
            serviceWillStart(service: service, userData: userData, headers:headers, tag:tag)
            service.execute(data: userData, headers: headers, tag: tag)
        }
    }
    
    
    
    //MARK:-
    func serviceWillStart(service:BaseService, userData:[String:Any], headers: [String:String], tag: String) {
        let serviceClassName = "\(type(of: service))"
        cache.add(tag: tag,serviceClassName:serviceClassName ,userData: userData, headers:headers)
        addObserver(notificationName: tag, callBack: #selector(didReceiveNotification))
    }
    
    
    
    //MARK:- Registr and unregistr  notification
    private func addObserver(notificationName:String, callBack: Selector){
        NotificationCenter.default.addObserver(self, selector: callBack, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    private func removeObserver(notificationName:String){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    
    
    //MARK:- Selectors that get called on notifications received
    @objc
    func didReceiveNotification(notification: NSNotification){
        if let userInfo = notification.userInfo {
            if let _ = userInfo[BaseService.DataReceivedKey] {
                dataDidReceived(notification: notification)
            }
            else {
                errorDidReceived(notification: notification)
            }
        }
    }
    
    func dataDidReceived(notification: NSNotification) {
        serviceDidFinish(tag: notification.name.rawValue, responseData: notification.userInfo?[BaseService.DataReceivedKey] ?? [:],  requestStatus: .success)
    }
    
    func errorDidReceived(notification: NSNotification){
        serviceDidFinish(tag: notification.name.rawValue, responseData: notification.userInfo?[BaseService.DataReceivedWithErrorKey] ?? "", requestStatus: .fail)
    }
    
    
    func serviceDidFinish(tag:String, responseData: Any, requestStatus: RequestStatus){
        
        cache.updateItemResponse(tag: tag, response: responseData)
        cache.updateStatus(tag: tag, status: requestStatus)
        
        if shouldRemoveRequestFromCache(tag: tag) {
               cache.removeItem(tag: tag)
            removeObserver(notificationName: tag)
        }
    }

    
    
    //MARk:- Model must decide if request should be removed from cache
    func shouldRemoveRequestFromCache(tag:String)-> Bool {
        if let item = cache.itemWithTag(tag: tag) {
            switch(item.status) {
            case .pending, .onProgress: return false
            case .success, .fail: return true
            }
        }
        return true
    }
    
    
    
    //MARK:- Are there any requests execution in progress or pending
    func didFinishAllRequests()->Bool{
        for tag in cache.allTags() {
            if let item = cache.itemWithTag(tag: tag) {
                switch(item.status){
                case .pending, .onProgress:
                    return false
                case .success, .fail:
                    continue
                }
            }
        }
        return true
    }
    
    
    
    //MARK:- Release resources
    func removeAll(){
        NotificationCenter.default.removeObserver(self)
        cache.clear()
    }
    
    deinit {
        removeAll()
    }
    
}
