//
//  BaseResponse.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import ObjectMapper


class BaseResponse: Mappable
{
    
    
    enum ResultCode:Int {
        case failure
        case sucess
    }

    
    //MARK:- Properties
    var result:ResultCode = .failure
    var failReasons:[FailureReason] = []
    
    
    //MARK:- Intializer
    required init?(map: Map) {
    }
    
    
    //MARK:- Map
    func mapping(map: Map) {
        result <- (map["response_result"],EnumTransform<ResultCode>())
        failReasons <- map["fail_reasons"]
    }
}
