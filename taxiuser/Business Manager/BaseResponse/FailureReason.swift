//
//  FailureReason.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import ObjectMapper


class FailureReason: Mappable {
    
    //MARK:- Properties
    var id = 0
    var message:String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    //MARK:- Map
    func mapping(map: Map) {
        id <- map["fail_reason_id"]
        message <- map["message"]
    }
}
