//
//  BaseService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/2/15.
//  All rights reserved.
//

import UIKit


class BaseService
{
    static var className: String { return "\(self)" }
    class var notificationName:Notification.Name  { return Notification.Name(className) }
    
    
    var queue:[String] = []
    
    
    public static let DataReceivedKey = "Data Received Successfully"
    public static let DataReceivedWithErrorKey = "Data received with  errors"
    public static let ServiceName = "Service name"
    
    
    
    
    required init() {
    }
    
    func execute(data: [String: Any]?, headers:[String: String], tag:String) {
        
        let request = Request(tag: tag)
        
        let encodedParams = encodedParamters(parameters: data ?? [:])
        request.addParameters(parameters: encodedParams)
        request.addHeaders(headers: headers)
        
        if !AppConfigurations.forceProduction  {
            print("\n\(request.url) \nMethod: \(request.httpMethod) \nRepository: \(request.repository) \nTAG: \(tag)  \nParamters: \(data ?? [:])")
        }
        
        startRequest(request: request)
        
    }
    
    func encodedParamters(parameters:[String:Any])->[String:Any] {
        //Override this method to customize  paramters encoding
        var data = parameters
        
        data["device_id"] =  UIDevice.uuidString
        data["sim_ser"] = "4444"
        data["os_type"] =  "2"
        data["os_ver"] =  UIDevice.osVersion
        
        return data
    }
    
    
    func startRequest(request:Request){
        
        switch request.repository {
        case .offlineMockup:
            if let data = Bundle.readJson(fileName: request.tag) {
                processReceivedData(responseData: data, requestId: request.tag)
            }
        default:
            let appDelegate =  UIApplication.shared.delegate as? AppDelegate
            
            if appDelegate?.internetReachable() == true {
                HttpManager.execute(request: request, successClosure: {responseData in
                    
                    self.processReceivedData(responseData: responseData, requestId: request.tag)
                    
                }) { error in
                    self.requestDidFail(error: error, tag:request.tag)
                }
            }
            else {
                let error = QLError(tag: request.tag, code: 0, message: StringConstant.MsgInternetDown)
                self.requestDidFail(error: error, tag:request.tag)
            }
        }
        
    }
    
    func processReceivedData(responseData: Any, requestId: String){
        assertionFailure("You should override this method")
    }
    
    
    
    //MARK:- Sucess
    func requestDidSucess(responseData: Any, requestId: String){
        sendDataReceivedNotification(responseData: responseData, tag: requestId)
    }
    
    
    
    //MARK:- Do any logic related to failure case plus at end send notification
    func requestDidFail(error: Error,tag: String){
        sendErrorNotification(error: error, tag: tag)
    }
    
    
    
    //MARK:- Send notifications
    func sendDataReceivedNotification(responseData:Any, tag: String){
        
        let userInfo = [BaseService.DataReceivedKey: responseData, BaseService.ServiceName: tag] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: tag), object: self, userInfo: userInfo)
    }
    
    func sendErrorNotification(error:Error, tag:String){
        let userInfo = [BaseService.DataReceivedWithErrorKey:error, BaseService.ServiceName: tag] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: tag), object: self, userInfo: userInfo)
    }
    
    
    
    //MARK:- Registr and unregistr  notification
    func addObserver(notificationName:String, callBack: Selector){
        NotificationCenter.default.addObserver(self, selector: callBack, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    func removeObserver(notificationName:String){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    
    
    //MARK:- Selectors that get called on notifications received
    func didReceiveNotification(notification: NSNotification){
        if let userInfo = notification.userInfo {
            
            if let _ = userInfo[BaseService.DataReceivedKey] {
                requestDidSucess(responseData: userInfo[BaseService.DataReceivedKey]! as Any, requestId: notification.name.rawValue)
            }
            else {
                requestDidFail(error: userInfo[BaseService.DataReceivedWithErrorKey] as! Error, tag: notification.name.rawValue)
            }
        }
    }
}
