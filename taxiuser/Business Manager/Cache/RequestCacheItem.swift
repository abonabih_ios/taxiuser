//
//  RequestCacheItem.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/8/15.
//   All rights reserved.
//

import Foundation


enum RequestStatus {
    case onProgress
    case success
    case pending
    case fail
}


class RequestCacheItem
{
    
    
    var userData: Any?
    var headers: [String:String]?
    var response: Any?
    var serviceClassName:String
    var date:Date?
    var status: RequestStatus
    
    
    
    init(serviceClassName:String,status: RequestStatus) {
        self.status = status
        self.serviceClassName = serviceClassName
    }
}
