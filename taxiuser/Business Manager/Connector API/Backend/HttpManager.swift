//
//  HttpManager.swift
//  icity
//
//  Created by Abdelrahman Ahmed Shaheen on 12/1/15.
//   All rights reserved.
//

import Alamofire


typealias SuccessClosure = (Any)->Void
typealias FailureClosure = (Error)->Void

class HttpManager
{
    
    //MARK:- execute request with url
    class func execute(request:Request, successClosure:@escaping SuccessClosure, failureClosure:@escaping FailureClosure) {
        
        
        Alamofire.request(request.url, method: HTTPMethod(rawValue:request.httpMethod)!, parameters: request.parameters, encoding: URLEncoding.default, headers: request.headers)
            .validate(statusCode: 200..<300)
            .responseJSON{ response in
            
                printLog(tag: .info, data: response.debugDescription)

                switch response.result {
                case .success:
                    successClosure(response.result.value as Any)
                case .failure(let error):
                    failureClosure(QLError(error:error))
                }
        }
        
    }
    
    
    //MARK:- execute upload request
    class func upload(request:Request, successClosure:@escaping SuccessClosure, failureClosure:@escaping FailureClosure) {
        
    }
}
