//
//  EndPointConfiguration.swift
//  Studio
//
//  Created by Chestnut User on 2/22/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation
import SwiftyUserDefaults


class EndPointConfiguration
{
    
    enum HTTPMethod: String {
        case get     = "GET"
        case post    = "POST"
    }


    
    required init() {
    }
    

    
    //override if required
    var requestrRepository:Repository {return .live}
    var httpMethod: HTTPMethod { return .post}
    var path:String {return ""}
   
    private var baseUrl:String { return !Defaults[.baseUrl].isEmpty ? Defaults[.baseUrl] : "http://www.noon-it.com/alpha/users/"}
    
    var liveUrl: String { return  baseUrl }
    var demoUrl: String { return baseUrl }
    var devUrl: String { return baseUrl }
    var onlineMockupUrl: String { return baseUrl }

    
    //Final
    final var repository: Repository { return AppConfigurations.forceProduction ? AppConfigurations.repository : requestrRepository }
    final var url: String { return "\(initURL())"}

    
    private func initURL()->String {
        switch repository {
        case .live:
            return  liveUrl + path
        case .demo:
            return  demoUrl + path
        case .dev:
            return  devUrl + path
        case .onlineMockup:
            return  onlineMockupUrl + path
        case .offlineMockup:
            return path
        }
    }

}
