//
//  QLError.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 12/2/15.
//  All rights reserved.
//

import Foundation

struct QLError: Error
{

    var tag:String
    var message:String
    var code:Int
    
    
    
    //MARK:- Intializer
    init(tag:String ,code: Int, message: String) {
        self.tag = tag
        self.message = message
        self.code = code
        
    }
    
    init(error: Error) {
        message = error.localizedDescription
        tag = "0"
        code = 0
    }
}
