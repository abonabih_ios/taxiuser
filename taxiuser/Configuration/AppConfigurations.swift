//
//  AppConfigurations.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 10/30/16.
//   All rights reserved.
//

import Foundation


enum Mode:String {
    case debug, releaseDebug,  releaseProduction
}

enum Repository:String {
    case  onlineDev, onlineStage, onlineLive, onlineMockup, offlineMockup
}


struct AppConfigurations
{
    static var repository:Repository {
        guard let repositoryString = AppConfigurations.appConfigurations()["repository"] as? String
        else { return .onlineLive }
        
        guard let repository = Repository(rawValue: repositoryString)
            else { return .onlineLive }

        return repository
    }
    
    static var appMode: Mode {
        guard let modeString = AppConfigurations.appConfigurations()["appMode"] as? String
            else { return .releaseProduction }
        
        guard let mode = Mode(rawValue: modeString)
            else { return .releaseProduction }
        
        return mode
    }

    
    static var repositoryUrl: String {
        switch repository {
        case .onlineLive:
            return liveUrl
        case .onlineStage:
            return stageUrl
        case .onlineDev:
            return devUrl
        case .onlineMockup:
            return onlineMockupUrl
        case .offlineMockup:
            return ""
        }
    }

    static var liveUrl: String {
        guard let urlString = AppConfigurations.appConfigurations()["liveURL"] as? String
            else { return ""}
        return urlString
    }
    
    static var stageUrl: String {
        guard let urlString = AppConfigurations.appConfigurations()["stageURL"] as? String
            else { return ""}
        return urlString
    }

    static var devUrl: String {
        guard let urlString = AppConfigurations.appConfigurations()["devURL"] as? String
            else { return ""}
        return urlString
    }
    
    static var onlineMockupUrl: String {
        guard let urlString = AppConfigurations.appConfigurations()["onlineMockup"] as? String
            else { return ""}
        return urlString
    }


    static var enforceRealeaseDebug: Bool { return appMode == .releaseDebug }
    static var enforceRealeaseProduction: Bool { return appMode == .releaseProduction }

    private static func appConfigurations()-> [String:AnyObject] {
        let appConfigs = Bundle.contentsOfFile(plistName: "\(Bundle.main.targetName!)Configurations.plist", bundle: nil)
        return appConfigs
    }
}
