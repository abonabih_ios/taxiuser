//
//  AppRealm.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import RealmSwift

extension Realm
{
    private class var appConfig:Configuration {
        var config = Realm.Configuration(objectTypes: [AppSetting.self])
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(Bundle.main.targetName!)_AppSetting.realm")
        return config
    }
    
    var settings:AppSetting { return Realm.appRealm.objects(AppSetting.self).first!}
    
   class  var appRealm: Realm { return try! Realm(configuration: Realm.appConfig) }
    
    
    func setPolicyTipsFlag(flag:Bool) {
        try!  Realm.appRealm.write() {
            settings.policyTipsDidShowFirstTime = flag
        }
    }
    
    func configureApp(){
        try!  Realm.appRealm.write() {
            if Realm.appRealm.objects(AppSetting.self).first == nil {
                Realm.appRealm.create(AppSetting.self, value: ["policyTipsDidShowFirstTime":true])

            }
        }
    }
}


