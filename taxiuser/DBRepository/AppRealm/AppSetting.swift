//
//  AppConfiguration.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/5/17.
//
//

import RealmSwift
import ObjectMapper

class AppSetting: Object
{
    dynamic var policyTipsDidShowFirstTime:Bool = true
}
