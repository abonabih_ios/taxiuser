//
//  FavoritesRealm.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/30/16.
//
//

import RealmSwift

extension Realm
{
    private class var favoritePlacesConfig:Configuration {
        var config = Realm.Configuration(objectTypes: [Place.self])
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(Bundle.main.targetName!)_FavoritePlaces.realm")
        return config
    }
    
    class var favoritePlacesRealm: Realm {
        return try! Realm(configuration: Realm.favoritePlacesConfig)
    }
    

    
    
    func addPlace(place:Place) {
        Realm.favoritePlacesRealm.beginWrite()

        Realm.favoritePlacesRealm.add(place)

        try! Realm.favoritePlacesRealm.commitWrite()
    }
    
    
    func removePlace(place:Place) {
        Realm.favoritePlacesRealm.beginWrite()
        
        Realm.favoritePlacesRealm.delete(place)
        
        try! Realm.favoritePlacesRealm.commitWrite()
    }

}
