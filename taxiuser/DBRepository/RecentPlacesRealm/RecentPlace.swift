//
//  RecentPlace.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/14/17.
//
//

import RealmSwift
import ObjectMapper

class RecentPlace: Place
{
    
    dynamic var date: Date = Date()

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    override func mapping(map: Map) {
        name <- map["name"]
        address <- map["address"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        date <- (map["date"], DateTransform())
    }
}
