//
//  FavoritesRealm.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/30/16.
//
//

import RealmSwift

extension Realm
{
    private class var recentPlaceConfig:Configuration {
        var config = Realm.Configuration(objectTypes: [RecentPlace.self])
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(Bundle.main.targetName!)_RecentPlaces.realm")
        return config
    }
    
    class var recentPlacesRealm: Realm {
        return try! Realm(configuration: Realm.recentPlaceConfig)
    }
    

    
    
    func addRecentPlace(place:RecentPlace) {
        Realm.recentPlacesRealm.beginWrite()

        Realm.recentPlacesRealm.add(place)

        try! Realm.recentPlacesRealm.commitWrite()
    }
    
    
    func removeRecentPlace(place:RecentPlace) {
        Realm.recentPlacesRealm.beginWrite()
        
        Realm.recentPlacesRealm.delete(place)
        
        try! Realm.recentPlacesRealm.commitWrite()
    }

}
