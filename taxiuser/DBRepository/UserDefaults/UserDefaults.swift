//
//  UserDefaults.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/24/17.
//
//

import Foundation
import SwiftyUserDefaults


extension DefaultsKeys
{
    
    static let followupNumbers = DefaultsKey<[String]>("follow_up_numbers")    
}
