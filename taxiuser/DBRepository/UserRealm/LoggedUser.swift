//
//  LoggedUser.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/5/15.
//  All rights reserved.
//

import RealmSwift
import ObjectMapper





enum Gender: String {
    case male = "1", female = "2", boy = "3" ,  girl = "4"
}


class LoggedUser: Object, Mappable
{
    class var shared:LoggedUser? {
        return Realm.userRealm.currentUser
    }
    

    
    
    dynamic var id:String  = ""
    dynamic var name:String = ""
    dynamic var email:String?
    dynamic var gender:String = "1"
    
    dynamic var verified:String = "0"
    dynamic var bookedCount:Int = 0
    dynamic var balance:Double = 0.0

    
    var userGender:Gender { return Gender(rawValue: gender) ?? .male }
    var userVerified:Bool { return verified == "1" }

    
    
    required convenience init?(map: Map) {
        self.init()
    }
    


    func mapping(map: Map) {
        id <- map["user_id"]
        name <- map["name"]
        email <- map["email"]
        gender <- map["gender"]
        verified <- map["verified"]
        bookedCount <- map["booked_count"]
        balance <- map["balance"]
    }
}
