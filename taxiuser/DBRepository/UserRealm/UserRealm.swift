//
//  UserRealm.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import RealmSwift

extension Realm
{
    private class var userConfig:Configuration {
        var config = Realm.Configuration(objectTypes: [LoggedUser.self])
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(Bundle.main.targetName!).realm")
        return config
    }
    
    class var userRealm: Realm {
        return try! Realm(configuration: Realm.userConfig)
    }
    
    var currentUser:LoggedUser? {
        return Realm.userRealm.objects(LoggedUser.self).first
    }
    
    func createUpdateUser(userData:[String:Any]) {

        try!  Realm.userRealm.write() {
            
            Realm.userRealm.deleteAll()
            
            let user = Realm.userRealm.create(LoggedUser.self, value: userData)
            user.id = userData["user_id"] as? String ?? ""
            
        }
    }
    
    
    
    func reset() {
        
        try!  Realm.userRealm.write() {
            
            Realm.userRealm.deleteAll()
    
        }
    }

    func updateUserId(id:String) {
        try!  Realm.userRealm.write() {
            currentUser?.id = id
        }
    }
    
}
