//
//  EvaluationServiceConfiguration.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/17/17.
//
//

import Foundation

class AppEntryServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "user_app_entry.php"}
}
