//
//  SyncServiceConfiguration.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/21/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation




class BasicOrderLaterConfiguration: EndPointConfiguration
{
    override  var path:String {return "order_basic_later.php"}
}
