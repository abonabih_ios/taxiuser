//
//  CancelRideServiceConfiguration.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/21/17.
//

import Foundation




class CancelBookedRideServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "user_booked_cancel.php"}
}
