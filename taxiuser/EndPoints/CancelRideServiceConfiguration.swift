//
//  CancelRideServiceConfiguration.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/21/17.
//

import Foundation




class CancelRideServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "ride_user_cancel.php"}
}
