//
//  SyncServiceConfiguration.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/21/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation




class CancelVerificationServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "user_verify_cancel.php"}
}
