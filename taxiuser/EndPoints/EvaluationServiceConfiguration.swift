//
//  EvaluationServiceConfiguration.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/17/17.
//
//

import Foundation

class EvaluationServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "ride_user_eval.php"}
}
