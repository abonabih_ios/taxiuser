//
//  GetUserBalanceServiceConfiguration.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/17/17.
//
//

import Foundation

class GetUserBalanceServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "user_get_bal.php"}
}
