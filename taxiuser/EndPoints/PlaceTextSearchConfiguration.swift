//
//  SyncServiceConfiguration.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/21/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation




class PlaceTextSearchConfiguration: EndPointConfiguration
{
    override  var path:String {return "place/textsearch/json"}
    
    
    override var liveUrl: String { return "https://maps.googleapis.com/maps/api/" }
    override var demoUrl: String { return "https://maps.googleapis.com/maps/api/" }
    override var devUrl: String { return "https://maps.googleapis.com/maps/api/" }
    override var onlineMockupUrl: String { return "https://maps.googleapis.com/maps/api/" }
}
