//
//  RatesServiceConfiguration.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 4/26/17.
//
//

import Foundation


class RatesServiceConfiguration: EndPointConfiguration
{
    override  var path:String {return "user_get_rates.php"}
}
