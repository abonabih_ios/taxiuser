//
//  LoginService.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/8/15.
//   All rights reserved.
//

import RealmSwift


class LoginService: Service
{
    static let notificationName = Notification.Name(className)

    
    /*
    (0) : NO ERRORS
    (1) : UNEXPECTED ERROR : NOT "POST"
    (2) : SOME QUERY PAPRAMS ARE MISSED
    (3) : ERR
    (4) : User is disabled ( and must contact customer support )
    (5) : User Not Found OR wrong pass ( = ERR CRED )
   */
    enum ErrorCode:Int {
        case no
        case unexpected
        case missedParams
        case unknown
        case userDisabled
        case authentication
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown
        }
    }
    
    
    
    
    //MARK:- 
    override func processReceivedData(responseData: Any, requestId: String) {
        if let response =  responseData as? [String:AnyObject] {
            
            
            let valid = response["valid"] as? Bool ?? false
            let erroCode = response["err_code"] as? Int ?? ErrorCode.unknown.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .no:
                var data = response
                data.removeValue(forKey: "valid")
                data.removeValue(forKey: "err_code")
                
                Realm.userRealm.createUpdateUser(userData: data)
                super.requestDidSucess(responseData: valid, requestId: requestId)
            case .authentication:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.authentication.rawValue , message:"Wrong user name or password"), tag: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }
    
}
