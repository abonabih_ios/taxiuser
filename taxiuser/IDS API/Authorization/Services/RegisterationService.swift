//
//  RegisterationService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/2/15.
//  All rights reserved.
//

import Foundation
import RealmSwift


class RegisterationService: Service
{
    static let notificationName = Notification.Name(className)
    
    
    /*
    (0) : NO ERRORS
    (1) : UNEXPECTED ERROR : NOT "POST"
    (2) : SOME QUERY PAPRAMS ARE MISSED
    (3) : ERR
    (4) : USER already exists
    (5) : Invalid Email
    (6) : ERR
    (7) : ERR
     */
    enum ErrorCode:Int {
        case no
        case unexpected
        case missedParams
        case unknown1
        case userAlreadyExists
        case invalidEmail
        case unknown2
        case unknown3

        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown1
        }
    }
    
    
    //MARK:- 
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let _ = response["valid"] as? Bool ?? false
            let erroCode = response["err_code"] as? Int ?? ErrorCode.unknown1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .no:
                let userId = response["user_id"] as? Int ?? 0
                Realm.userRealm.updateUserId(id: "\(userId)")
                super.requestDidSucess(responseData: userId, requestId: requestId)
            case .userAlreadyExists:
                 Realm.userRealm.reset()
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.userAlreadyExists.rawValue , message:"User already exists"), tag: requestId)
            case .invalidEmail:
                Realm.userRealm.reset()
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.invalidEmail.rawValue , message:"Invalid email address"), tag: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }
    
    
    
    func saveUserData(data: [String:Any]) {
        Realm.userRealm.createUpdateUser(userData: data)
    }
}
