//
//  Cache.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/8/15.
//   All rights reserved.
//

import Foundation
import SwiftDate



class Cache
{
    lazy var queue: NSMutableDictionary = [:]
    
    
    
    //MARK:- Add to queue
    func add(tag:String, userData:Any, response: Any, status: RequestStatus) {
        let requestCacheItem = RequestCacheItem(status: status)
        requestCacheItem.userData = userData
        requestCacheItem.response = response
        queue.setObject(requestCacheItem, forKey: tag as NSCopying)
    }
    
    func add(tag: String, userData: Any){
        let requestCacheItem = RequestCacheItem(status: .Pending)
        requestCacheItem.userData = userData
        queue.setObject(requestCacheItem, forKey: tag as NSCopying)
    }
    
    
    //MARK:- Retrieve item
    func itemWithTag(tag:String)->RequestCacheItem?{
        if let item = queue.value(forKey: tag) as? RequestCacheItem {
            return item
        }
        return nil
    }
    
    
    
    //MARK:- Retrieve all items with tag containing string
    func allItemsWithTagPrefix(prefix:String)->[(tag:String, item:RequestCacheItem)] {
        var items:[(tag:String, item:RequestCacheItem)] = []
        for (tag, item) in queue {
            if (tag as! String).contains(prefix) {
                items.append((tag as! String, item: item as! RequestCacheItem))
            }
        }
        
        items.sort { $0.1.date! < $1.1.date! }
                
        return items
    }
    
    func mostEarlierCachedItemWithTagPrefix(prefix:String)->String {
        if let tag = allItemsWithTagPrefix(prefix: prefix).first?.tag {
            return tag
        }
        return ""
    }
    
    
    //MARK:- update existing request data
    func updateItemResponse(tag:String, response:Any) {
        let cacheItem = itemWithTag(tag: tag)
        
        if cacheItem?.response == nil {
            cacheItem?.date = Date()
            cacheItem?.response = response
        }
    }
    
    func updateStatus(tag:String, status: RequestStatus){
        if let cacheItem = itemWithTag(tag: tag){
            cacheItem.status = status
        }
    }
    
    
    
    //MARK:- Remove cached request data , clear all cached requests data
    func removeItem(tag: String) {
        queue.removeObject(forKey: tag)
    }
    
    func  clear() {
        queue.removeAllObjects()
    }
    
    func allTags()-> [String] {
        if let _ = queue.allKeys as? [String] {
            return queue.allKeys as! [String]
        }
        return []
    }
    
    func allItems()-> [RequestCacheItem] {
        if let _ = queue.allValues as? [RequestCacheItem] {
            return queue.allValues as! [RequestCacheItem]
        }
        return []
    }

    func isEmpty()->Bool {
        return queue.allKeys.isEmpty
    }
}
