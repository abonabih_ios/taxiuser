//
//  RequestCacheItem.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/8/15.
//   All rights reserved.
//

import Foundation

 enum RequestStatus {
    case Success
    case Pending
    case Relogin
    case Fail
}

class RequestCacheItem
{
    var userData: Any?
    var response: Any?
    var date:Date?
    var status: RequestStatus
    
    init(status: RequestStatus) {
        self.status = status
    }
}
