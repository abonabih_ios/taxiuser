//
//  Modules.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 7/26/16.
//   All rights reserved.
//

import Cent


protocol ModuleProtocol{
    var name:String { get}
    var storyboardName:String { get }
}

enum Module:String,ModuleProtocol {
    case ids = ""
    
    var storyboardName:String { return name.words().joined(separator: "_")}
    var name:String { return rawValue}
}
