//
//  LocationService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/15/16.
//
//


import CoreLocation





class LocationService: NSObject,CLLocationManagerDelegate
{
    static let sharedInstance = LocationService()
    
    private let locationManager = CLLocationManager()
    
    private let CurrentLocationNotificationKey = "Current Location Data"
    private let CurrentLocationErrorNotificationKey = "Current Location Error"
    
    private let CurrentLocationKey = "User Current Location"
    private let CurrentLocationErrorKey = "User Current Location Error"
    
    
    private var completionClosure:((CLLocationCoordinate2D)->Void)!
    private var failureClosure:((Error)->Void)!
    
    
    
    //MARK:- Intializer
    private override init() {
        super.init()
        locationManager.delegate = self
    }
    
    
    
    //MARK:- Intializer
    func didReceiveLocationNotification(notification: Notification){
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: CurrentLocationNotificationKey), object: nil)
        
        
        switch(notification.name){
        case Notification.Name(rawValue: CurrentLocationNotificationKey):
            if let location = notification.userInfo?[CurrentLocationKey] as? CLLocation {
                completionClosure(location.coordinate)
            }
        case Notification.Name(rawValue: CurrentLocationErrorNotificationKey):
            if let error = notification.userInfo?[CurrentLocationErrorKey] as? Error{
                failureClosure(error)
            }
        default:break
        }
        
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: CurrentLocationNotificationKey), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: CurrentLocationErrorNotificationKey), object: nil)

        locationManager.stopUpdatingLocation()

        
    }
    
    func getCurrentLocation(completion:@escaping (CLLocationCoordinate2D)->Void, failure:@escaping (Error)->Void){
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveLocationNotification), name: Notification.Name(rawValue: CurrentLocationNotificationKey), object: nil)
        
        completionClosure = completion
        failureClosure = failure
    }
    
    
    
    //MARK:- CLLocationManagerDelegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = manager.location {
            NotificationCenter.default.post(name: Notification.Name(rawValue: CurrentLocationNotificationKey), object: nil, userInfo: [CurrentLocationKey : location])
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: CurrentLocationErrorNotificationKey), object: nil, userInfo: [CurrentLocationErrorKey : error])
    }
    
    func canGetLocation()-> Bool {
        
        if !CLLocationManager.locationServicesEnabled() {
            return false
        }
        else{
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            case .restricted, .denied, .notDetermined:
                return false
            }
        }
    }
}
