//
//  Regex.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/22/17.
//
//

import Foundation
import Cent

extension Regex
{
    static let emailRegex = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$"
}
