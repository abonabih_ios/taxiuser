//
//  String.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/26/17.
//
//

import Foundation
import Cent


extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func validateWith(pattern:String)->Bool {
        let regex = Regex.init(pattern)
        return regex.test(testStr: self)
    }
    
    
    func isValidEmail()->Bool {
        return validateWith(pattern: Regex.emailRegex)
    }
}
