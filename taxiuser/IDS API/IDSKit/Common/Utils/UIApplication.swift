//
//  UIApplication.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/23/16.
//
//

import UIKit

extension UIApplication
{
    func tryOpenUrl(urls:[String]) {
        for urlString in urls {
            if let url = URL(string:urlString) {
                if canOPenUrl(urlString: urlString) {
                    openURL(url)
                    return
                }
            }
        }
    }
    
    func canOPenUrl(urlString:String)->Bool {
        if let url = URL(string:urlString) {
            return canOpenURL(url)
        }
        return false
    }
}
