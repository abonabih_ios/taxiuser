//
//  Environment.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/6/15.
//  All rights reserved.
//

import UIKit
import Foundation

extension UIDevice
{
    class var osVersion:String { return UIDevice.current.systemVersion }

    class var uuidString:String { return UIDevice.current.identifierForVendor?.uuidString ?? "" }
}
