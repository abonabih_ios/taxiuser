//
//  ModelView.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/7/15.
//   All rights reserved.
//

import UIKit

class ModelView
{
    
    lazy var cache = Cache()
    
    
    
    
    func  executeService(service: Service,userData: [String:Any], tag: String) {
        
        let cacheItem = cache.itemWithTag(tag: tag)
        
        if cacheItem != nil &&  cacheItem?.status == .Success  && cacheItem?.response != nil {
            addObserver(notificationName: tag, callBack: #selector(didReceiveNotification))
            service.requestDidSucess(responseData: cacheItem!.response!, requestId: tag)
        }
        else {
            serviceWillStart(userData: userData, tag:tag)
            service.execute(data: userData, tag: tag)
        }
    }
    
    
    
    //MARK:-
    func serviceWillStart(userData:[String:Any], tag: String) {
        cache.add(tag: tag, userData: userData)
        addObserver(notificationName: tag, callBack: #selector(didReceiveNotification))
    }
    
    
    
    //MARK:- Registr and unregistr  notification
    private func addObserver(notificationName:String, callBack: Selector){
        NotificationCenter.default.addObserver(self, selector: callBack, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    private func removeObserver(notificationName:String){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    
    
    //MARK:- Selectors that get called on notifications received
    @objc
    func didReceiveNotification(notification: NSNotification){
        if let userInfo = notification.userInfo {
            if let _ = userInfo[DataReceivedKey] {
                dataDidReceived(notification: notification)
            }
            else {
                errorDidReceived(notification: notification)
            }
        }
    }
    
    func dataDidReceived(notification: NSNotification) {
        serviceDidFinish(tag: notification.name.rawValue, responseData: notification.userInfo?[DataReceivedKey] ?? [:],  requestStatus: .Success)
    }
    
    func errorDidReceived(notification: NSNotification){
        serviceDidFinish(tag: notification.name.rawValue, responseData: notification.userInfo?[DataReceivedWithErrorKey] ?? "", requestStatus: .Fail)
    }
    
    
    func serviceDidFinish(tag:String, responseData: Any, requestStatus: RequestStatus){
        
        cache.updateItemResponse(tag: tag, response: responseData)
        cache.updateStatus(tag: tag, status: requestStatus)
        
        if shouldRemoveRequestFromCache(tag: tag) {
               cache.removeItem(tag: tag)
            removeObserver(notificationName: tag)
        }
    }

    
    
    //MARk:- Model must decide if request should be removed from cache
    func shouldRemoveRequestFromCache(tag:String)-> Bool {
        if let item = cache.itemWithTag(tag: tag) {
            switch(item.status) {
            case .Pending, .Relogin: return false
            case .Success, .Fail: return true
            }
        }
        return true
    }
    
    
    
    //MARK:- Are there any requests execution in progress or pending
    func didFinishAllRequests()->Bool{
        for tag in cache.allTags() {
            if let item = cache.itemWithTag(tag: tag) {
                switch(item.status){
                case .Pending, .Relogin:
                    return false
                case .Success, .Fail:
                    continue
                }
            }
        }
        return true
    }
    
    
    
    //MARK:- Release resources
    deinit {
        for tag in cache.allTags() {
            removeAllNotification(tag: tag)
        }
        cache.clear()
    }
    
    private func removeAllNotification(tag: String){
        removeObserver(notificationName: tag)
    }
}
