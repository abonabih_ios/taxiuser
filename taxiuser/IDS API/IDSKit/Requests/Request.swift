//
//  Request.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/2/15.
//  All rights reserved.
//

import Foundation

//MARK:- Request
class Request
{
    var id: String
    var tag: String
    var repository: Repository
    var path: String
    var httpMethod: String
    
    var url: String = ""
    
    
    private  var httpParameters:[String:Any]?
    private  var httpHeaders:[String:String]?
    
    
    var headers:[String:String]? { return httpHeaders }
    var parameters:[String:Any]? { return httpParameters}
    
    
    
    //MARK:- Intializer
    init(tag:String) {
        let requestConfigs = Bundle.contentsOfFile(plistName: "\(tag).plist", bundle: nil)
        
        //Set repository
        //Check if should enforce repository
        if AppConfigurations.enforceRealeaseDebug || AppConfigurations.enforceRealeaseProduction {
            repository = AppConfigurations.repository
        }
        else {
            if let repositoryString = requestConfigs["repository"] as? String {
                if let repository = Repository(rawValue: repositoryString) {
                    self.repository =  repository
                }
                else {
                    self.repository = AppConfigurations.repository
                }
            }
            else {
                self.repository = AppConfigurations.repository
            }
        }
        
        self.id = requestConfigs["id"] as! String
        self.tag = tag
        self.httpMethod = requestConfigs["httpMethod"] as! String
        self.path = requestConfigs["path"] as! String
        self.url = self.initURL()
    }
    
    init(tag:String, userData: [String:Any]) {
        self.httpParameters += userData
        
        let requestConfigs = Bundle.contentsOfFile(plistName: "\(tag).plist", bundle: nil)
        
        //Set repository
        //Check if should enforce repository
        if AppConfigurations.enforceRealeaseDebug || AppConfigurations.enforceRealeaseProduction {
            repository = AppConfigurations.repository
        }
        else {
            if let repositoryString = requestConfigs["repository"] as? String {
                if let repository = Repository(rawValue: repositoryString) {
                    self.repository =  repository
                }
                else {
                    self.repository = AppConfigurations.repository
                }
            }
            else {
                self.repository = AppConfigurations.repository
            }
        }
        
        self.id = requestConfigs["id"] as! String
        self.tag = tag
        self.httpMethod = requestConfigs["httpMethod"] as! String
        self.path = requestConfigs["path"] as! String
        self.url = self.initURL()
    }
    
    private func initURL()->String {
        switch repository {
        case .onlineLive:
            return  AppConfigurations.liveUrl + path
        case .onlineStage:
            return  AppConfigurations.stageUrl + path
        case .onlineDev:
            return  AppConfigurations.devUrl + path
        case .onlineMockup:
            return  AppConfigurations.onlineMockupUrl + path
        case .offlineMockup:
            return path
        }
    }
    
    
    //MARK:- Add http paramters
    func setParamters(parameter:Any, forParameterName:String){
        if let _ = httpParameters {
            httpParameters?[forParameterName] = parameter
        }
        else {
            httpParameters = [:]
            httpParameters?[forParameterName] = parameter
        }
    }
    
    func addParameters(parameters: [String:Any]?){
        httpParameters += parameters
    }
    
    
    
    //MARK:- Add http paramters
    func setHeaders(value:String, name:String){
        if let _ = httpHeaders {
            httpHeaders?[name] = value
        }
        else {
            httpHeaders = [:]
            httpHeaders?[name] = value
        }
    }
    
    func addHeaders(headers: [String:String]){
        httpHeaders += headers
    }
}
