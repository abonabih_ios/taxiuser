//
//  Service.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/2/15.
//  All rights reserved.
//

import UIKit


let DataReceivedKey = "Data Received Successfully"
let DataReceivedWithErrorKey = "Data received with  errors"
let ServiceName = "Service name"



class Service
{
    static var className: String { return "\(self)" }

    var queue:[String] = []

    
    func execute(data: [String: Any]?,tag:String) {
        
        let request = Request(tag: tag)

        request.addParameters(parameters: data)
        
        if AppConfigurations.appMode == .debug {
            print("Request \(tag)  paramters: \(data ?? [:])")
        }

        startRequest(request: request)

    }
    
    func encodedParamters(parameters:[String:Any])->[String:Any] {
        //Override this method to customize  paramters encoding
        var data = parameters
        
        data["device_id"] =  UIDevice.uuidString
        data["sim_ser"] = "4444"
        data["os_type"] =  "2"
        data["os_ver"] =  UIDevice.osVersion

        return data
    }
    
    
    func startRequest(request:Request){
        
        switch request.repository {
        case .offlineMockup:
            if let data = Bundle.readJson(fileName: request.url) {
                processReceivedData(responseData: data, requestId: request.tag)
            }
        default:
            HttpManager.execute(request: request, successClosure: {responseData in
                
                self.processReceivedData(responseData: responseData, requestId: request.tag)
                
                }) { error in
                    //TODO:- General errors goes here
                    self.requestDidFail(error: error, tag:request.tag)
                }
        }
    }
    
    
    
    func processReceivedData(responseData: Any, requestId: String){
        assertionFailure("You should override this method")
    }
    

    
    //MARK:- Sucess
    func requestDidSucess(responseData: Any, requestId: String){
         sendDataReceivedNotification(responseData: responseData, tag: requestId)
    }
    
    
    //MARK:- Do any logic related to failure case plus at end send notification
    func requestDidFail(error: Error,tag: String){
        sendErrorNotification(error: error, tag: tag)
    }
    
    
    
    //MARK:- Send notifications
    func sendDataReceivedNotification(responseData:Any, tag: String){
        
        let userInfo = [DataReceivedKey: responseData, ServiceName: tag] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: tag), object: self, userInfo: userInfo)
    }
    
    func sendErrorNotification(error:Error, tag:String){
        let userInfo = [DataReceivedWithErrorKey:error, ServiceName: tag] as [String : Any]

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: tag), object: self, userInfo: userInfo)
    }
    
    
    //MARK:- Registr and unregistr  notification
    func addObserver(notificationName:String, callBack: Selector){
        NotificationCenter.default.addObserver(self, selector: callBack, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    func removeObserver(notificationName:String){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
    
    
    
    //MARK:- Selectors that get called on notifications received
    func didReceiveNotification(notification: NSNotification){
        if let userInfo = notification.userInfo {

            if let _ = userInfo[DataReceivedKey] {
                requestDidSucess(responseData: userInfo[DataReceivedKey]! as Any, requestId: notification.name.rawValue)
            }
            else {
                requestDidFail(error: userInfo[DataReceivedWithErrorKey] as! Error, tag: notification.name.rawValue)
            }
        }
    }
}
