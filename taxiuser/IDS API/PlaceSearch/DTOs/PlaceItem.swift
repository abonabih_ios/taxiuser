//
//  PlaceItem.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/9/17.
//
//
import ObjectMapper


class PlaceItem:Mappable
{
    var lat:Double = 0
    var lng:Double = 0
    
    var name:String = ""
    var vicinity:String = ""
    var identifier:String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lat <- map["geometry.location.lat"]
        lng <- map["geometry.location.lng"]
        name <- map["name"]
        vicinity <- map["vicinity"]
        identifier <- map["place_id"]
    }
}
