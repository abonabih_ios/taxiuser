//
//  PlaceSearchService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/8/17.
//
//

import Foundation
import ObjectMapper


class PlaceSearchService: Service
{
    
    static let notificationName = Notification.Name(className)

    
    //MARK:- Execute
    override func execute(data: [String: Any]?,tag:String) {
        
        var request:Request!
        
        
        if let _ =  data?["keyword"]  {
            request = Request(tag: "PlaceTextSearch")
        }
        else  {
            request = Request(tag: "PlaceNearbySearch")
        }
        
        request.tag = String(describing: PlaceSearchService.self)
        request.url = request.path + encodedParamters(parameters: data!)
        startRequest(request: request)
        

    }

    
    func encodedParamters(parameters:[String:Any])->String {
        
        guard let keyword =  parameters["keyword"] else {
        
            return "?location=\(parameters["location"]!)&radius=\(parameters["radius"]!)&key=\(parameters["key"]!)"

        }
        
        return "?location=\(parameters["location"]!)&radius=\(parameters["radius"]!)&keyword=\(keyword)&key=\(parameters["key"]!)"
        
    }
    
    
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String){
        if responseData is [String:Any] {
            
            guard let response = Mapper<PlaceSearchResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
                return
            }
            
            switch response.status {
            case .ok:
                super.requestDidSucess(responseData: response.results, requestId: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
        }
    }
}
