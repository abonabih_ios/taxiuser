//
//  BookedRide.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import ObjectMapper



enum CarType:String {
    case economy = "1"
    case busniess = "2"
}

class BookedRide: Mappable
{
    
    
    
    var rideId:String = ""
    var rideTtype:BookingType = .basic
    var carType:CarType = .economy
    var gender:Gender = .male
    var dateTime:String = ""
    var pickLong:Double = 0
    var pick_lat:Double = 0
    
    var addressPick:String?
    
    var destLong:Double?
    var destLat:Double?
    var addresDrop:String?
    var notes:String?
    var followUpNumbers:[String]?

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        rideId <- map["ride_id"]
        rideTtype <- (map["ride_type"],EnumTransform<BookingType>())
        carType <- (map["car_type"],EnumTransform<CarType>())
        gender <- (map["gender"],EnumTransform<Gender>())
        dateTime <- map["date_time"]
        pickLong <- map["pick_long"]
        pick_lat <- map["pick_lat"]
        
        addressPick <- map["address_pick"]
        
        destLong <- map["dest_long"]
        destLat <- map["dest_lat"]
        addresDrop <- map["address_drop"]
        notes <- map["notes"]
        followUpNumbers <- map["follow_tel"]
    }
    
}
