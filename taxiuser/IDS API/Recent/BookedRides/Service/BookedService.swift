//
//  BookedService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import ObjectMapper



class BookedService: Service
{
    static let notificationName = Notification.Name(className)
    
    
    
    
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String){
        if responseData is [String:Any] {
            
            guard let response = Mapper<BookedRidesResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
                return
            }
            
            switch response.code {
            case .sucess:
                super.requestDidSucess(responseData: response.results, requestId: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:0 , message:"Something went wrong"), tag: requestId)
        }
    }
}
