//
//  RecentService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/14/17.
//
//

import ObjectMapper
import RealmSwift


class RecentService: Service
{
    class func recentPlaces()->[RecentPlace]{
        let places = Array(Realm.recentPlacesRealm.objects(RecentPlace.self))
        return places
    }
    
    class func addPlaceToRecent(place:RecentPlace){
        let realm = Realm.recentPlacesRealm
        realm.addRecentPlace(place: place)
    }
    
    class func removePlaceFromRecent(place:RecentPlace){
        let realm = Realm.recentPlacesRealm
        realm.removeRecentPlace(place: place)

    }
}

