//
//  OrderStateResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/17/17.
//
//

import ObjectMapper

class OrderStateResponse: Mappable
{
    
    
    enum OrderState:Int {
        case noDrivers = 1
        case searching = 2
        case assigned = 3
    }
    
    var orderState:OrderState = .noDrivers
    
    var rideId:String = ""
    
    var carLong:Double = 0
    var carLat:Double = 0

    var carDistance:String = ""

    var carTime:String = ""
    
    var carBrand:String = ""
    var carModel:String = ""

    var carYear:String = ""
    var carPlate:String = ""

    var driverPicUrl:String = ""

    var driverName:String = ""

    var driverMobile:String = ""


    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        orderState <- (map["order_state"],EnumTransform<OrderState>())
        rideId <- map["ride_id"]
        carLong <- map["car_long"]
        carLat <- map["car_at"]
        carDistance <- map["car_distance"]
        
        carTime <- map["car_time"]
        carBrand <- map["car_brand"]
        
        carModel <- map["car_model"]
        carYear <- map["car_year"]
        carPlate <- map["car_plate"]
        
        driverPicUrl <- map["driver_pic_url"]
        driverName <- map["driver_name"]
        driverMobile <- map["driver_mobile"]

    }
}
