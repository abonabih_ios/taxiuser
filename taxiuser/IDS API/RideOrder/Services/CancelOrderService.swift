//
//  CancelOrderService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/2/17.
//
//

import Foundation

class CancelOrderService: Service
{
    static let notificationName = Notification.Name(className)
    
    

    /*
     (0) err Req type
     (1) Success
     (2) err query param
     (3) ERROR
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknown
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unknown.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                //TODO:- Parse fare estimate
                super.requestDidSucess(responseData: "", requestId: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }

}
