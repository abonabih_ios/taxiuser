//
//  FareEstimateService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/2/17.
//
//

import Foundation

class FareEstimateService: Service
{
    static let notificationName = Notification.Name(className)
    
   
    /*
     (0) err Req type
     (1) Success
     (2) err query param
     (3) ERROR
     (4) ERROR
     (5) ERROR
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknown1
        case unknown2
        case unknown3

        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown1
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unknown1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                //TODO:- Parse fare estimate
                let fareEst = response["fare_est"] as? Double ?? 0.0

                super.requestDidSucess(responseData: fareEst, requestId: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }
}
