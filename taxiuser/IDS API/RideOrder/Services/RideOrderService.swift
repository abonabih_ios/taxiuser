//
//  RideOrderService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/2/17.
//
//

import Foundation
import RealmSwift

class RideOrderService: Service
{
    static let notificationName = Notification.Name(className)
    
    static let BasicOrderNowTag = "BasicOrderNow"
    static let KidsNowOrderTag = "KidsNowOrder"
    static let BasicOrderLaterTag = "BasicOrderLater"
    static let KidsOrderLaterTag = "KidsOrderLater"

    /*
     ( 0 ) err Req type
     ( 1 ) Success
     ( 2 ) err query param
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknown
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown
        }
    }
    
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            let codeKey = response.keys.filter { $0.contains("code")}.first ?? "res_code"
            
            let erroCode = response[codeKey] as? Int ?? ErrorCode.unknown.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                let orderId = response["order_id"] as? String ?? ""

                Realm.tripRealm.saveCurrentOrder(orderId: orderId)
                
                super.requestDidSucess(responseData: orderId, requestId: requestId)
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unknown.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }
    
    
    
    class func currentOrderId()->String? {
        return Realm.tripRealm.currentOrder?.orderId
    }
}
