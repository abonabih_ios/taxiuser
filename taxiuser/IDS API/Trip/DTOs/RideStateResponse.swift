//
//  RideStateResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/17/17.
//
//

import Foundation
import ObjectMapper

class RideStateResponse:Mappable
{
    
    enum RideState:Int {
        case booked
        case onWay
        case arrived
        case onTrip
        case finished
        case payed
    }
    
    var rideState:RideState = .finished
    var carDistance:String = ""
    var carTime:String = ""
    
    var carLong:Double = 0
    var carLat:Double = 0

    var cost:Double = 0
    var distance:Double = 0
    var time:Double = 0
    var waiting:Bool = false
    var paymentAmount:Double = 0
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        rideState <- (map["ride_state"],EnumTransform<RideState>())
        carDistance <- map["car_distance"]
        carTime <- map["car_time"]
        carLong <- map["car_long"]
        carLat <- map["car_lat"]
        
        cost <- map["cost"]
        distance <- map["distance"]
        
        time <- map["time"]
        waiting <- map["waiting"]
        paymentAmount <- map["payment_amount"]
    }
}
