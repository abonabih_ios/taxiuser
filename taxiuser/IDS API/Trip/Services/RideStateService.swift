//
//  RideStateService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/17/17.
//
//

import Foundation

class RideStateService: Service
{
    static let notificationName = Notification.Name(className)
    
    
    /*
     (0) : UNEXPECTED ERROR : NOT "POST"
     (1) : SUCCESS
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) : ERR DB CONNECTION
     (4) : RIDE DOES NOT EXIST
    */
    enum ErrorCode:Int {
        case unexpectedError
        case sucess
        case queryParamsMissed
        case errorDBConnection
        case rideNotExist
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unexpectedError
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:Any] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unexpectedError.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                //TODO:- Persiste data if needed
                if let rideStateResponse = RideStateResponse(JSON: response) {
                
                super.requestDidSucess(responseData: rideStateResponse, requestId: requestId)
                }
                else {
                    requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unexpectedError.rawValue , message:"Something went wrong"), tag: requestId)
                }
            default:
                requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unexpectedError.rawValue , message:"Something went wrong"), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: AAError(tag:requestId, code:ErrorCode.unexpectedError.rawValue , message:"Something went wrong"), tag: requestId)
        }
    }
}
