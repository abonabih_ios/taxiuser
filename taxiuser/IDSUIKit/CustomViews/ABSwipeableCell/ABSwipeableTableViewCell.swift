//
//  ABSwipeableTableViewCell.swift
//
//  Created by abdelrahman shaheen on 7/15/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit


class ABSwipeableTableViewCell: UITableViewCell
{
    
    var rightActions: [ABUITableViewCellRowAction]? { didSet { addRightButtons() } }
    
    var leftActions: [ABUITableViewCellRowAction]? { didSet { addLeftButtons() } }
    
    var draggedToFarLeft = false, draggedToFarRight = false
    
    var rightActionsHidden = true { didSet { updateRightButtons() } }
    
    var leftActionsHidden = true { didSet { updateLeftButtons() } }
    
    private var rightButtons:[UIButton]?
    private var leftButtons:[UIButton]?
    private var originalCenter = CGPoint()
    
    var cellActionsOpened: Bool { return (!rightActionsHidden || !leftActionsHidden) }
    
    
    //MARK:- Intializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView() {
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        recognizer.delegate = self
        contentView.addGestureRecognizer(recognizer)
        contentView.backgroundColor = .white
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    //MARK:- Add right action buttons
    private func addRightButtons() {
        if let cellRightActions = rightActions {
            removeRightButtonsIfExist()
            rightButtons = []
            for index in 0..<cellRightActions.count {
                let button = UIButton(rowAction: cellRightActions[index])
                button.isHidden = rightActionsHidden
                self.addSubview(button)
                rightButtons?.append(button)
            }
            bringSubview(toFront: contentView)
        }
    }
    
    private func removeRightButtonsIfExist() {
        if let _ = rightButtons {
            for button in rightButtons! {
                button.removeFromSuperview()
            }
        }
    }
    
    //MARK:- Add left action buttons
    private func addLeftButtons() {
        if let cellLeftActions = leftActions {
            removeLeftButtonsIfExist()
            leftButtons = []
            for index in 0..<cellLeftActions.count {
                let button = UIButton(rowAction: cellLeftActions[index])
                button.isHidden = leftActionsHidden
                self.addSubview(button)
                leftButtons?.append(button)
            }
            bringSubview(toFront: contentView)
        }
    }
    
    private func removeLeftButtonsIfExist() {
        if let _ = leftButtons {
            for button in leftButtons! {
                button.removeFromSuperview()
            }
        }
    }
    
    //MARK:- Update right action buttons frames
    private func updateRightButtons() {
        if let buttons = rightButtons {
            for index in 0..<buttons.count {
                buttons[index].isHidden = rightActionsHidden
                buttons[index].frame =  CGRect(x: bounds.width - (CGFloat(index + 1) * bounds.height), y: 0, width: contentView.bounds.height, height: contentView.bounds.height)
            }
        }
    }
    
    //MARK:- Update left action buttons frames
    private func updateLeftButtons() {
        if let buttons = leftButtons {
            for index in 0..<buttons.count {
                buttons[index].isHidden = leftActionsHidden
                buttons[index].frame = CGRect(x: CGFloat(index) * bounds.height, y: 0, width: contentView.bounds.height, height: contentView.bounds.height)
            }
        }
    }
    
    
    
    //MARK: - Horizontal pan gesture methods
    func handlePan(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            originalCenter = contentView.center
        case .changed:
            let translation = recognizer.translation(in: self)
            let newCenterX =  originalCenter.x + translation.x
            handleDrag(newCenterX: newCenterX)
        case .ended:
            onReleaseSwipingAnimation()
        default:break
        }
    }
    
    
    private func handleDrag(newCenterX: CGFloat) {
        if let _ = rightActions {
            handleRightToLeftDrag(newCenterX: newCenterX)
        }
        
        if let _ = leftActions {
            handleLeftToRightDrag(newCenterX: newCenterX)
        }
    }
    
    //MARK:- Left drag calculations
    private func handleRightToLeftDrag(newCenterX: CGFloat) {
        moveContentViewToLeft(newCenterX: newCenterX)
        rightActionsHidden = rightActionButtonsHidden(newCenterX: newCenterX)
    }
    
    private func rightActionButtonsHidden(newCenterX: CGFloat) -> Bool {
        return contentView.frame.origin.x >= 0
    }
    
    private func moveContentViewToLeft(newCenterX: CGFloat) {
        if canMoveLeft(newCenterX: newCenterX)
        {
            contentView.center = CGPoint(x:newCenterX, y:originalCenter.y)
        }
    }
    
    private func canMoveLeft(newCenterX: CGFloat) -> Bool {
        return newCenterX < cellCenterX()
    }
    
    private func cellCenterX() -> CGFloat {
        return bounds.width / 2
    }
    
    private func rightActionButtonsWidth() -> CGFloat {
        if let _ = rightActions {
            return CGFloat(rightActions!.count) * bounds.height
        }
        return 0
    }
    
    func isDraggedToFarLeft() -> Bool {
        if let _ = rightActions {
            return contentView.frame.origin.x < -rightActionButtonsWidth() + 3 * bounds.height / 4
        }
        return false
    }
    
    //MARK:- Right translatin calculation
    private func handleLeftToRightDrag(newCenterX: CGFloat) {
        moveContentViewToRight(newCenterX: newCenterX)
        leftActionsHidden = leftActionButtonsHidden(newCenterX: newCenterX)
    }
    
    private func leftActionButtonsHidden(newCenterX: CGFloat) -> Bool {
        return contentView.frame.origin.x <= 0
    }
    
    private func moveContentViewToRight(newCenterX: CGFloat) {
        if canMoveRight(newCenterX: newCenterX) {
            contentView.center = CGPoint(x: newCenterX, y:originalCenter.y)
        }
    }
    
    private func canMoveRight(newCenterX: CGFloat) -> Bool {
        return newCenterX > cellCenterX()
    }
    
    private func leftActionButtonsWidth() -> CGFloat {
        if let _ = leftActions {
            return CGFloat(leftActions!.count) * bounds.height
        }
        return 0
    }
    
    func isDraggedToFarRight() -> Bool {
        if let _ = leftActions {
            return contentView.frame.origin.x > leftActionButtonsWidth() -  3 * bounds.height / 4
        }
        return false
    }
    
    
    
    //MARK:- GestureRecognizer delegate methods
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: superview!)
            if fabs(translation.x) > fabs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    
    
    
    //MARK:- Handle touches when the cell is opend for editing or deletion
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
            if self.point(inside: point, with: event) {
                
                if tableView.otherCellCurrentlyOpened(activeCell: self) {
                    return nil
                }
                
                let pointInContentView = contentView.convert(point, from: self)
                
                if contentView.point(inside: pointInContentView, with: event) {
                    if touchingCellWhileActionsOpened(event: event) {
                        moveToOriginalFrameAnimation()
                        return nil
                    }
                    return contentView.touchedViewInside (point: pointInContentView, withEvent: event)
                }
                return touchedViewInside(point: point, withEvent: event)
            }
            else   {
                let count = event?.touches(for: tableView)?.count  ?? 0
                
                if cellActionsOpened || count > 0 {
                    moveToOriginalFrameAnimation()
                }
                return nil
            }
    }
    
    private func touchingCellWhileActionsOpened(event: UIEvent?) -> Bool {
        
        let count = event?.touches(for: tableView)?.count  ?? 0

        return cellActionsOpened || count > 0
    }

}
