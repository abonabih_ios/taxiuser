//
//  UITableViewUtility.swift
//  Medical Reminder
//
//  Created by abdelrahman shaheen on 7/20/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

extension UITableView
{
    func otherCellCurrentlyOpened(activeCell: ABSwipeableTableViewCell) -> Bool {
        for cell in visibleCells {
            if let swipeableCell = cell as? ABSwipeableTableViewCell {
                if swipeableCell.cellActionsOpened  && swipeableCell != activeCell {
                    return true
                }
            }
        }
        return false
    }
}
