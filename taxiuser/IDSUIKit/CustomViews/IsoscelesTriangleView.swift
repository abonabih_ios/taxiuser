//
//  IsoscelesTriangleView.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 3/22/16.
//   All rights reserved.
//

import UIKit

@IBDesignable
class IsoscelesTriangleView: UIView
{

    @IBInspectable var fillColor: UIColor = UIColor.red
    let path = UIBezierPath()

    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        fillColor.setFill()
        
        let p1: CGPoint = CGPoint(x: 0, y: bounds.height)
        let p2: CGPoint = CGPoint(x: bounds.width / 2, y: 0)
        let p3: CGPoint =  CGPoint(x: bounds.width, y: bounds.height)
        
        path.move(to: p1)
        path.addLine(to: p2)
        path.addLine(to: p3)
        
        path.lineCapStyle = .round
        path.lineJoinStyle = .round
        
        path.close()
        path.fill()
        path.addClip()
        
    }
}
