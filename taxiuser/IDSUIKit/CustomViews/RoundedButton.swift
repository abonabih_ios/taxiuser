//
//  RoundedButton.swift
//  Medical Reminder
//
//  Created by abdelrahman shaheen on 6/1/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class RoundedButton: UIButton
{
    @IBInspectable var cornerRadius: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var shadowRadius: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var borderWidth: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var borderColor: UIColor = UIColor.black { didSet{ updateLayer() } }
    
    
    override func draw(_ rect: CGRect)
    {
        super.draw(rect)
        updateLayer()
    }
    
    func updateLayer()
    {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
        clipsToBounds = true
    }
    
}
