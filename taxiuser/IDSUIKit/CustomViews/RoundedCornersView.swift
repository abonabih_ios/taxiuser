//
//  RoundedCornersView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/2/16.
//   All rights reserved.
//

import UIKit

@IBDesignable
class RoundedCornersView: UIView
{

    @IBInspectable var cornerRadius : CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var borderWidth : CGFloat = 0 { didSet{ updateLayer() } }
    
    @IBInspectable var topLeft: Bool = false { didSet{ updateLayer() } }
    @IBInspectable var topRight: Bool = false { didSet{ updateLayer() } }
    @IBInspectable var bottomLeft: Bool = false { didSet{ updateLayer() } }
    @IBInspectable var bottomRight: Bool = false { didSet{ updateLayer() } }
    @IBInspectable var borderColor: UIColor = UIColor.black { didSet{ updateLayer() } }
    @IBInspectable var fillColor: UIColor = UIColor.white { didSet{ updateLayer() } }
    
    
        @IBInspectable var shadowColor: UIColor = UIColor.black { didSet{ updateLayer() } }
        @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0) { didSet{ updateLayer() } }
        @IBInspectable var shadowOpacity: Float = 0 { didSet{ updateLayer() } }

    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateLayer()
        //clipsToBounds = true
    }
    
    func updateLayer() {
        borderColor.setStroke()
        fillColor.setFill()
        
        var corners:[UIRectCorner] = []
        
        if topLeft { corners.append(.topLeft) }
        if topRight { corners.append(.topRight) }
        if bottomLeft { corners.append(.bottomLeft) }
        if bottomRight { corners.append(.bottomRight) }
        
        var cornersUnion:UIRectCorner
        if corners.count > 0 {
            cornersUnion = corners[0]
            
            corners.forEach{ corner in
                cornersUnion = cornersUnion.union(corner)
            }
            
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: cornersUnion, cornerRadii: CGSize(width: cornerRadius,height: cornerRadius))
            
            path.fill()
            path.lineWidth = borderWidth
            path.stroke()
            
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            maskLayer.backgroundColor = fillColor.cgColor
            maskLayer.borderColor = borderColor.cgColor
            
            layer.mask = maskLayer
            
            layer.shadowColor = shadowColor.cgColor
            layer.shadowOffset = shadowOffset
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = path.cgPath

        }
    }

}
