//
//  RoundedLabel.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/20/15.
//   All rights reserved.
//

import UIKit

@IBDesignable
class RoundedLabel: UILabel
{
    
    @IBInspectable var cornerRadius:CGFloat = 0.0 { didSet{ updateLayer()}}
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    
    var insets: UIEdgeInsets {
        get {
            return UIEdgeInsetsMake(topInset, leftInset, bottomInset, rightInset)
        }
        set {
            topInset = newValue.top
            leftInset = newValue.left
            bottomInset = newValue.bottom
            rightInset = newValue.right
        }
    }
    

    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateLayer()
    }
    
    func updateLayer(){
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 0
        layer.masksToBounds = true
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var adjSize = super.sizeThatFits(size)
        adjSize.width += leftInset + rightInset
        adjSize.height += topInset + bottomInset
        
        return adjSize
    }
    
    override var intrinsicContentSize: CGSize {
        var contentSize = super.intrinsicContentSize
        contentSize.width += leftInset + rightInset
        contentSize.height += topInset + bottomInset
        
        return contentSize
    }

}
