//
//  RoundedView.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/21/15.
//   All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView
{
    @IBInspectable var cornerRadius: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var shadowRadius: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var borderWidth: CGFloat = 0 { didSet{ updateLayer() } }
    @IBInspectable var borderColor: UIColor = UIColor.black { didSet{ updateLayer() } }
    
//    @IBInspectable var shadowColor: UIColor = UIColor.black { didSet{ updateLayer() } }
//    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0) { didSet{ updateLayer() } }
//    @IBInspectable var shadowOpacity: Float = 0 { didSet{ updateLayer() } }



    
    var path:UIBezierPath?

    
    
    override func draw(_ rect: CGRect)
    {
        super.draw(rect)
        updateLayer()
    }
    
    func updateLayer()
    {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor

//        layer.shadowColor = shadowColor.cgColor
//        layer.shadowOffset = shadowOffset
//        layer.shadowOpacity = shadowOpacity
//        layer.shadowRadius = shadowRadius

        layer.masksToBounds = true
        clipsToBounds = true
        
        path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
    }
}
