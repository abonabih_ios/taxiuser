//
//  TrapezoidalView.swift
//  itnews
//
//  Created by abdelrahman shaheen on 10/21/15.
//  Copyright © 2015 NTG Clarity. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift


@IBDesignable
class TrapezoidalView: UIView
{
        
    @IBInspectable var fillColor: UIColor = UIColor(hex6: 0x3E92CC)
    
    @IBInspectable var cutAway: CGFloat = 0.8 {
        didSet {
            imageCutAway = cutAway - 0.05
        }
    }
    
    var imageCutAway: CGFloat = 0.75
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        fillColor.setFill()
        
        let trapezoidalpath = UIBezierPath()
        trapezoidalpath.move(to: CGPoint(x: 0, y: cutAway * bounds.height))
        trapezoidalpath.addLine(to: CGPoint(x: 0, y: 0))
        trapezoidalpath.addLine(to: CGPoint(x: bounds.width, y:  0))
        trapezoidalpath.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        
        trapezoidalpath.close()
        trapezoidalpath.fill()
        trapezoidalpath.addClip()
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = trapezoidalpath.cgPath
        maskLayer.backgroundColor = UIColor.white.cgColor
        layer.mask = maskLayer

        layer.masksToBounds = true

    }
}
