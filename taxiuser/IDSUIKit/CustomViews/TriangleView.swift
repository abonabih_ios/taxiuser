//
//  TriangleView.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 3/19/16.
//   All rights reserved.
//

import UIKit

@IBDesignable
class TriangleView: UIView
{
    @IBInspectable var fillColor: UIColor = UIColor.red
    @IBInspectable var aligment:Alighment = .ltr {didSet {setNeedsDisplay()}}

    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        fillColor.setFill()
        
        let p1: CGPoint = CGPoint(x: 0, y: 0)
        let p2: CGPoint = CGPoint(x: bounds.width, y: 0)
        let p3: CGPoint = aligment == .ltr ?  CGPoint(x: bounds.width, y: bounds.height) : CGPoint(x: 0, y: bounds.height)
        
        let path = UIBezierPath()
        path.move(to: p1)
        path.addLine(to: p2)
        path.addLine(to: p3)
        
        path.close()
        path.fill()
        path.addClip()

    }
}
