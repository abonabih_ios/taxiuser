//
//  UITableViewCellUtility.swift
//  Medical Reminder
//
//  Created by abdelrahman shaheen on 7/20/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

extension UITableViewCell
{
    var tableView:UITableView {return containerTableView()}
    
    
    private func containerTableView() -> UITableView {
        var view = self.superview
        while ((view != nil) && !view!.isKind(of: UITableView.self)) {
            view = view?.superview
        }
        
        return view as! UITableView
    }
}
