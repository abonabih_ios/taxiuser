//
//  UITextField.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/9/17.
//
//
import UIKit

@IBDesignable
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}
