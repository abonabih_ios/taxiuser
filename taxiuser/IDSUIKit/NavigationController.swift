//
//  NavigationController.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 10/3/16.
//   All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, UIViewControllerTransitioningDelegate, UIBarPositioningDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status bar white font
//        self.navigationBar.barStyle = UIBarStyle.black
//        self.navigationBar.tintColor = UIColor.white
    }
    
    private func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
