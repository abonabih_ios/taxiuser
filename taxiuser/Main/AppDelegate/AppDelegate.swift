//
//  AppDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/18/16.
//
//

import UIKit
import Reachability
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import RealmSwift
import DropDown



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var reachability: Reachability?
    
    var modelView: AppModelView?
    var router: AppRouter?

    //GOOGLE & LOC
    //Mark:- APIs keys
    static let NearbyRadius         =  500
    static let GoogleMapsAPIKey     = "AIzaSyCJYg-8uudTuIGt4XzWxhkCgJq5sqOkUoA"//"AIzaSyDET2-S_826eR4uFvQy3fH8DHGEFL9F-Oo"
    static let GooglePlacesAPIKey   = "AIzaSyCJYg-8uudTuIGt4XzWxhkCgJq5sqOkUoA"//"AIzaSyDET2-S_826eR4uFvQy3fH8DHGEFL9F-Oo"
//AIzaSyBnxrnMaBsJmeAh8jWzeP9Q1YY6ob4n_IA
    static let CountryCode          = "+974"
    
    
    //================================================================================================================
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        //Google
        GMSServices.provideAPIKey(AppDelegate.GoogleMapsAPIKey)
        GMSPlacesClient.provideAPIKey(AppDelegate.GooglePlacesAPIKey)
        
        //Keyboard
        IQKeyboardManager.sharedManager().enable = true
        
        //Reachability
        configureReachability()
        
        //Realm
        Realm.appRealm.configureApp()
        
        //DropDown
        DropDown.startListeningToKeyboard()
        
        //Language
        let currentLanguage = (UserDefaults.standard.array(forKey: "AppleLanguages") as? [String])?.first()?.components(separatedBy: "-").first() ?? "en"
        UserDefaults.standard.set([currentLanguage], forKey: "AppleLanguages")

        //Window & Router
        modelView   = AppModelView(window: window)
        router      = AppRouter(window: window)
        modelView?.requestAppEntryDate()
        UILabel.appearance().defaultFont = UIFont(name: "DroidArabicKufi", size: 10)

        
        return true
    }
    
    //================================================================================================================

    //Reachability Object
    private func configureReachability()
    {
        self.reachability = Reachability.forInternetConnection()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: nil)
        
        self.reachability?.startNotifier()
    }
    
    //================================================================================================================

    //MARK:-
    func reachabilityChanged(notification: Notification)
    {
        if self.reachability!.isReachable()
        {
            printLog(tag: .info, data: "Internet avalaible!!!")
            
            NotificationCenter.default.post(name: Notification.Name.reachable)
        }
        else
        {
            printLog(tag: .info, data: "Internet not avalaible!!!")
            
            NotificationCenter.default.post(name: Notification.Name.notReachable)
        }
    }
    
    //================================================================================================================

    func internetReachable()-> Bool
    {
        return  self.reachability!.isReachable()
    }
    
    //================================================================================================================

    
}

