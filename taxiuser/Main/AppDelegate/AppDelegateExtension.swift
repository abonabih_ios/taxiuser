//
//  AppDelegateExtension.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/30/17.
//
//

import UIKit
import SVProgressHUD

extension AppDelegate
{
    //================================================================================================================

    //MARK:- Show alert views
    func showThreeActionsAlert( title:String?,
                                firstActionButtontitle:String?,
                                secondActionButtontitle:String?,
                                thirdActionButtontitle:String?,
                                message:String?,
                                onCancel:(()->Void)? = nil,
                                secondActionClosure:@escaping ()->Void,
                                thirdActionClosure: @escaping ()->Void)
    {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let alertSecondAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            secondActionClosure()
        }
        
        alertController.addAction(alertSecondAction)
        
        let alertThirdAction = UIAlertAction(title:thirdActionButtontitle, style: .default) { (action) in
            thirdActionClosure()
        }
        
        alertController.addAction(alertThirdAction)
        
        
        let cancelAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true) {
        }
    }
    
    //================================================================================================================

    func showTwoActionsAlert ( title:String?,
                               firstActionButtontitle:String? ,
                               secondActionButtontitle:String?,
                               mesage:String?,
                               onCancel:(()->Void)? = nil,
                               actionClosure:@escaping ()->Void)
    {
        
        let alertController = UIAlertController(title: title, message: mesage, preferredStyle: .alert)
        
        
        let alertAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            actionClosure()
        }
        
        alertController.addAction(alertAction)
        
        
        let cancelAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
         window?.rootViewController?.present(alertController, animated: true) {
        }
    }
    
    //================================================================================================================

    func showDefaultAlert(title:String?,message:String?, actionBlock:(()->Void)? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: StringConstant.Ok, style: .cancel
        ) { (action) in
            alertController.dismiss(animated: true){
            }
            actionBlock?()
        }
        
        alertController.addAction(cancelAction)
        
         window?.rootViewController?.present(alertController, animated: true) {
        }
    }
    
    
    //================================================================================================================

    func showLoadingIndicator()
    {
        if let view = UIApplication.shared.keyWindow
        {
            SVProgressHUD.setContainerView(view)
            view.isUserInteractionEnabled = false
            SVProgressHUD.show()
        }
    }
    
    //================================================================================================================

    func hideLoadingIndicator()
    {
        SVProgressHUD.dismiss()
        UIApplication.shared.keyWindow?.isUserInteractionEnabled = true
    }
    
}
