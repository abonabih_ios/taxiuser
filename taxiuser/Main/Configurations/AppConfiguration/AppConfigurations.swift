//
//  AppConfigurations.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 10/30/16.
//   All rights reserved.
//

import Foundation


enum Mode:String {
    case debug, releaseDebug,  releaseProduction
}

enum Repository:String {
    case  dev, demo, live, onlineMockup, offlineMockup
}


struct AppConfigurations
{
    
    static var appMode: Mode { return .debug }

    static var forceProduction: Bool { return appMode == .releaseDebug }

    
    static var repository:Repository {
        
        let repository:Repository = .live
        
        return forceProduction ? .live : repository
    }
}
