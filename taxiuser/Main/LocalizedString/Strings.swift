//
//  Strings.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/2/16.
//   All rights reserved.
//

struct StringConstant
{
    static let Cancel = I18n.localizedString("button_cancel")
    static let Ok = I18n.localizedString("button_ok")
    static let Info = I18n.localizedString("info")

    static let HomeTitle = I18n.localizedString("home_title_home")

    static let TripTitle = I18n.localizedString("trip_title")
    static let DatePickerTitle = I18n.localizedString("date_picker")

    static let Now = I18n.localizedString("now")
    static let Later = I18n.localizedString("later")
    static let Back = I18n.localizedString("back")
    static let Next = I18n.localizedString("next")
    static let Start = I18n.localizedString("start")

    
    static let RideDetails = I18n.localizedString("ride_details_title")

    static let Home = I18n.localizedString("side_menu_item_home")
    static let BookedRides = I18n.localizedString("side_menu_item_booked_rides")
    static let FavoriteLocations = I18n.localizedString("side_menu_item_favorite_locations")
    static let Settings = I18n.localizedString("side_menu_item_settings")
    static let Help = I18n.localizedString("side_menu_item_help")
    static let About = I18n.localizedString("side_menu_item_about")
    static let Logout = I18n.localizedString("side_menu_item_logout")
    static let Communicate = I18n.localizedString("side_menu_item_communicate")
    static let Share = I18n.localizedString("side_menu_item_share")
    static let CallUs = I18n.localizedString("side_menu_item_call_us")
    static let EmailUs = I18n.localizedString("side_menu_item_email_us")

    
    static let LogoutMessage = I18n.localizedString("info_message_logout")

    static let Delete = I18n.localizedString("delete")
    static let Pick_place_msg = I18n.localizedString("pick_place_msg")
    static let Business = I18n.localizedString("business")
    
     static let  Rates = I18n.localizedString("rates")
     static let  BasicRide = I18n.localizedString("basic_ride")
     static let  KidsRide = I18n.localizedString("kids_ride")
     static let  Economy = I18n.localizedString("economy")
    
    static let  PlacePicker = I18n.localizedString("place_picker")
    static let  Map = I18n.localizedString("map")
    static let  Favorite = I18n.localizedString("favorite")
    static let  Nearby = I18n.localizedString("nearby")
    static let  Recent = I18n.localizedString("recent")
    
    static let  CannotGetLocationMsg = I18n.localizedString("cannot_get_location_msg")
    
    static let  MegRestart = I18n.localizedString("msg_restart")
    static let  Restart = I18n.localizedString("restart")
    static let  MsgChangeLanguage = I18n.localizedString("msg_change_language")


    static let  MsgInvalidCode = I18n.localizedString("msg_invalid_code")
    static let  MsgUserNotFound = I18n.localizedString("msg_user_not_found")

    static let  MsgSomethingWentWrong = I18n.localizedString("Something went wrong")

    static let  MsgUserDoesNotExist = I18n.localizedString("User does not exist")
    static let  MsgCannotDeleteVerifiedUser = I18n.localizedString("msg_cannot_delete_verified_user")

    static let  MsgOldPassMismatch = I18n.localizedString("msg_old_pass_mismatch")

    static let  MsgWrongUserName = I18n.localizedString("msg_wrong_user_name")

    static let  MsgRideNotExists = I18n.localizedString("msg_not_exists")

    static let  MsgUserAlreadyExists = I18n.localizedString("msg_user_already_exists")

    
    static let  MsgInvalidEmail = I18n.localizedString("msg_invalid_email")
    static let  MsgUserAlreadyVerified = I18n.localizedString("user_already_verified")
    static let  MsgVerificationCodeSent = I18n.localizedString("msg_verification_code_sent")
    static let  MsgEnterVerCode = I18n.localizedString("msg_enter_ver_code")

    static let  MsgValidatePassLen = I18n.localizedString("msg_validate_pass_len")
    static let  MsgEnterMobNum = I18n.localizedString("msg_enter_mob_num")

    static let  MsgEnterValidFullName = I18n.localizedString("msg_enter_valid_full_name")

    static let  MsgPickLoc = I18n.localizedString("msg_pick_loc")
    
    static let  TitleLegalNotes = I18n.localizedString("title_legal_notes")

    static let  MsgCheckGPS = I18n.localizedString("msg_check_gps")

    static let  MsgEnterValidEmail = I18n.localizedString("msg_enter_valid_email")

    static let  MsgEmailMismatch = I18n.localizedString("msg_email_mismatch")

    static let  MsgOldPassBValidation = I18n.localizedString("msg_old_pass_validation")

    static let  MsgNewPassLenValidation = I18n.localizedString("msg_new_pass_len_validation")

    static let  MsgPassChanged = I18n.localizedString("msg_pass_changed")

    static let  MsgEmailChanged = I18n.localizedString("msg_emial_changed")

    
    static let  MsgLookAtTaxiApp = I18n.localizedString("msg_look_at_taxi_app")

    
    static let  SupportMobile = I18n.localizedString("support_mobile")

    static let  CompanyWebsite = I18n.localizedString("company_website")

    static let  SupportEmail = I18n.localizedString("support_email")


    static let  MsgSelectGender = I18n.localizedString("msg_select_gender")

    static let  DateFormate = I18n.localizedString("date_formate")

    static let  MsgDestinationLocRequired = I18n.localizedString("msg_destination_loc_required")
    
    static let  DateLbl = I18n.localizedString("date_lbl")

    static let  Qar = I18n.localizedString("qar")

    static let  MsgTripBooked = I18n.localizedString("msg_trip_booked")

    static let  MsgNoNearbyDrivers = I18n.localizedString("msg_no_nearby_drivers")

    static let  FullName = I18n.localizedString("full_name")

    static let  VerCode = I18n.localizedString("ver_code")
    static let  MobileNum = I18n.localizedString("mobile_num")
    static let  EmailOptional = I18n.localizedString("email_optional")
    static let  Password = I18n.localizedString("password")
    static let  Register = I18n.localizedString("register")
    static let  SignIn = I18n.localizedString("sign_in")

    static let  ActivateAccount = I18n.localizedString("activate_account")
    static let  ResendCode = I18n.localizedString("resend_code")
    static let  Confirm = I18n.localizedString("confirm")
    static let  Time = I18n.localizedString("time")

    
    static let  PickupLoc = I18n.localizedString("pickup_loca")
    static let  DropLoc = I18n.localizedString("drop_loc")
    static let  CarType = I18n.localizedString("car_type")
    static let  Notes = I18n.localizedString("notes")

    
    
    static let  FollowUp = I18n.localizedString("follow_up")
    static let  EnterLocName = I18n.localizedString("enter_loc_name")
    static let  CurrLoc = I18n.localizedString("curr_loc")
    static let  Save = I18n.localizedString("save")

    
    static let  ShowRates = I18n.localizedString("show_rates")
    static let  ShowLegalNotes = I18n.localizedString("show_legal_notes")
    static let  ShowDetailedNotes = I18n.localizedString("show_detailed_notes")
    static let  ShowRidesRates = I18n.localizedString("show_rides_rates")

    
    static let  Starting = I18n.localizedString("starting")
    static let  Minimum = I18n.localizedString("minimum")
    static let  Penalty = I18n.localizedString("penalty")
    static let  PerKM = I18n.localizedString("per_km")

    static let  KM = I18n.localizedString("km")

    static let  Moving = I18n.localizedString("moving")
    static let  Waiting = I18n.localizedString("waiting")
    static let  PerHour = I18n.localizedString("per_hour")
    static let  Pick = I18n.localizedString("pick")

    
    static let  Name = I18n.localizedString("name")
    static let  Gender = I18n.localizedString("gender")
    static let  Male = I18n.localizedString("male")
    static let  Female = I18n.localizedString("female")

    
    static let  Email = I18n.localizedString("email")
    static let  ChangeLang = I18n.localizedString("change_lang")
    static let  En = I18n.localizedString("en")
    static let  Ar = I18n.localizedString("ar")
    static let  Ur = I18n.localizedString("ur")

    
    static let  ChangeEmail = I18n.localizedString("change_email")
    static let  EnterNewEmail = I18n.localizedString("enter_new_email")
    static let  ConfrimEmail = I18n.localizedString("confrim_email")
    static let  ChangePass = I18n.localizedString("change_pass")

    
    static let  OldPass = I18n.localizedString("old_pass")
    static let  NewPass = I18n.localizedString("new_pass")
    static let  SelectLang = I18n.localizedString("select_lang")
    static let  HowWasUrTrip = I18n.localizedString("how_was_ur_trip")

    
    static let  RideReport = I18n.localizedString("ride_report")
    static let  Cost = I18n.localizedString("cost")
    static let  Distance = I18n.localizedString("distance")
    static let  Payment = I18n.localizedString("payment")

    
    static let  Balance = I18n.localizedString("balance")
    static let  Done = I18n.localizedString("done")
    static let  FareEstimate = I18n.localizedString("fare_estimate")
    static let  JustEstimate = I18n.localizedString("just_estimate")
    
    static let  MsgInternetDown = I18n.localizedString("msg_internet_down")

    static let  MsgEnableLocServices = I18n.localizedString("msg_enable_loc_services")
    
    static let  FollowUpNumbers = I18n.localizedString("follow_up_numbers")

    
    static let OrderCancelledScucessfully = I18n.localizedString("order_cancelled_scucessfully")
    
    static let RideCancelledScucessfully = I18n.localizedString("ride_cancelled_scucessfully")

    

    
    static let MsgForgotPassCallSupport = I18n.localizedString("msg_forgot_pass_call_support")

    
    static let Yes = I18n.localizedString("button_yes")
    static let No = I18n.localizedString("button_no")
    
    static let TitleCancelRide = I18n.localizedString("title_cancel_ride")
    static let MsgCancelRide = I18n.localizedString("msg_cancel_ride")

    
    static let ReadPolicy = I18n.localizedString("read_policy")
    static let UsagePolicy = I18n.localizedString("usage_policy")
    
    static let MsgWillTellTheDriver = I18n.localizedString("msg_will_tell_the_driver")

    static let  MsgSelectDate = I18n.localizedString("msg_select_date")
    
    static let  MsgSelectTime = I18n.localizedString("msg_select_time")
    
    
    static let  NoBookedRidesForUser = I18n.localizedString("no_rides_for_user")

    static let  UserCancelledRide = I18n.localizedString("user_cancelled_ride")
    static let  DriverCancelledRide = I18n.localizedString("driver_cancelled_ride")
    
    static let  Minute = I18n.localizedString("minute")
    
    
    static let  MsgSpecifyRideEvaluationStars = I18n.localizedString("msg_specify_ride_evaluation_stars")
    
    
    static let  MessageForceUpdate = I18n.localizedString("message_force_update")
    
    static let  MessageOptionalUpdate = I18n.localizedString("message_optional_update")
    
    static let  Update = I18n.localizedString("update")

    static let MsgAppBlockedCallSupport = I18n.localizedString("msg_app_blocked_call_support")

    static let Call = I18n.localizedString("call")
    
    static let EnableGPS = I18n.localizedString("enable_gps")
    static let MsgEnableLocationService = I18n.localizedString("message_enable_location_servivce")


}
