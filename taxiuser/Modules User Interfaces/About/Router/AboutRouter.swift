//
//  AboutRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit

protocol AboutRouterInput{
}

class AboutRouter: SceneRouter, AboutRouterInput
{
    unowned let viewController: AboutViewController
    
    init(scene: AboutViewController) {
        self.viewController = scene
    }
}

