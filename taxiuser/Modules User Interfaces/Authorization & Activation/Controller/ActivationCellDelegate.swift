//
//  ActivationCellDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit


extension AuthViewController:ActivationCellDelegate
{
    func resendCode(code:String) {
        showLoadingIndicator()
        modelView?.resendVerificationCode(data:["user_id": LoggedUser.shared?.id ?? ""])
    }
}
