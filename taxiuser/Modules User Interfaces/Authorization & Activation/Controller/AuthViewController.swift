//
//  AuthViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/26/17.
//
//

import UIKit
import Cent
import PhoneNumberKit

protocol AuthViewControllerOuput {
    func didLoginOrActivatedASucessfully()
    func showActivation()
    func showLogin()
}



class  AuthViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,AuthViewControllerOuput
{
    @IBOutlet weak var collectionView:UICollectionView!
    
    
    @IBOutlet weak var firstActionImgView: UIImageView!
    @IBOutlet weak var firstActionLbl: UILabel!
    
    @IBOutlet weak var secondActionImgView: UIImageView!
    @IBOutlet weak var secondActionLbl: UILabel!
    
    
    @IBOutlet weak var carLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var carImgView: UIImageView!
    
    
    
    //Cells
    var loginCell:LoginCell?
    var registrationCell:RegistrationCell?
    var activationCell:ActivationCell?
    
    
    enum AuthStep:Int {
        case none
        case login
        case register
        case activation
        
        init(step:Int) {
            if step <  AuthStep.none.rawValue {
                self = .none
            }
            else if step >  AuthStep.activation.rawValue  {
                self = .activation
            }
            else {
                self = AuthStep(rawValue:step)!
            }
        }
    }
    
    
    var currentStep:AuthStep = .none {didSet { setActionsTitlesAndImages() }}
    
    
    
    
    var modelView:AuthModelView?
    var router:AuthRouter?
    
    
    
    
    //MARK:- Lifecycle
    override func viewDidLoad(){
        super.viewDidLoad()
        modelView = AuthModelView(scene: self)
        router = AuthRouter(scene: self)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if currentStep == .none {
            
            self.currentStep = LoggedUser.shared != nil && LoggedUser.shared?.userVerified == false ? .activation : .login
            
            moveCarToCenter()
            
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
                
            }){ completed in
                
                self.collectionView.scrollToItem(at: IndexPath(item:self.currentStep.rawValue, section:0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    
    
    func didLoginOrActivatedASucessfully(){
        moveCarToEnd()
        
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            
        }){ completed in
            self.router?.navigateToHomeScene()
        }
    }
    
    
    func showActivation(){
        currentStep = .activation
        router?.navigateToActivation()
    }
    
    
    func showLogin(){
        currentStep = .login
        router?.navigateToLogin()
    }
    
    
    private func moveCarToCenter(){
        carLeadingConstraint.constant = (view.bounds.width - carImgView.bounds.width)  / 2
    }
    
    
    private func moveCarToEnd(){
        carLeadingConstraint.constant = view.bounds.width
    }
    
    
    
    
    //MARK:- UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCell", for: indexPath)
            return cell
        }
        else if indexPath.item == 1 {
            loginCell = loginCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LoginCell.self), for: indexPath) as? LoginCell
            
            return loginCell!
        }
        else if indexPath.item == 2 {
            registrationCell = registrationCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RegistrationCell.self), for: indexPath) as? RegistrationCell
            return registrationCell!
        }
        else {
            activationCell = activationCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ActivationCell.self), for: indexPath) as? ActivationCell
            
            activationCell?.delegate = self
            return activationCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    
    
    
    //MARK:- Actions title & images
    func setActionsTitlesAndImages(){
        switch currentStep {
        case .login:
            firstActionImgView.image = #imageLiteral(resourceName: "login_register")
            secondActionImgView.image =  #imageLiteral(resourceName: "login_login")
            
            firstActionLbl.text = "Register"
            secondActionLbl.text = "Sign in"
        case .register:
            firstActionImgView.image = #imageLiteral(resourceName: "reg_back")
            secondActionImgView.image =  #imageLiteral(resourceName: "reg_register")
            
            firstActionLbl.text = StringConstant.Back
            secondActionLbl.text = "Register"
        case .activation:
            firstActionImgView.image = #imageLiteral(resourceName: "ver_back")
            secondActionImgView.image =  #imageLiteral(resourceName: "ver_confirm")
            
            firstActionLbl.text = StringConstant.Cancel
            secondActionLbl.text =  "Confirm"
        default:
            break
        }
    }
    
    //MARK:- Actions
    @IBAction func firstBtnDidTapped(_ sender: UIButton) {
        switch currentStep {
        case .login:
            currentStep  = .register
            router?.navigateToRegistration()
        case .register:
            currentStep  = .login
            router?.navigateToLogin()
        case .activation:
            showLoadingIndicator()
            modelView?.cancelVerification(data: ["user_id": LoggedUser.shared?.id ?? ""])
        default:
            break
        }
    }
    
    @IBAction func secondBtnDidTapped(_ sender: UIButton) {
        switch currentStep {
        case .login:
            loginCell?.validate()
            if loginCell?.valid == true {
                showLoadingIndicator()
                modelView?.login(data: ["mob": loginCell?.mobileTxtField.text ?? "",
                                        "pass":loginCell?.passwordTxtField.text ?? "",
                                        "device_id": UIDevice.uuidString,
                                        "sim_ser": "4444",
                                        "os_type": "2",
                                        "os_ver": UIDevice.osVersion])
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: loginCell?.validationMessage)
            }
        case .register:
            registrationCell?.validate()
            if registrationCell?.valid == true {
                showLoadingIndicator()
                
                var data = ["name":registrationCell?.nameTxtField.text ?? ""]
                data["mob"] =  registrationCell?.mobileTxtField.text ?? ""
                data["pass"] = registrationCell?.passwordTxtField.text ?? ""
                data["gender"] =  registrationCell?.gender ?? "1"
                data["email"] =  registrationCell?.emailTxtField.text ?? ""
                data["country_code"] =  AppDelegate.CountryCode
                data["device_id"] =  UIDevice.uuidString
                data["sim_ser"] = "4444"
                data["os_type"] =  "2"
                data["os_ver"] =  UIDevice.osVersion
                
                modelView?.register(data:data)
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: registrationCell?.validationMessage)
            }
        case .activation:
            activationCell?.validate()
            if activationCell?.valid == true {
                showLoadingIndicator()
                modelView?.activate(data:["user_id": LoggedUser.shared?.id ?? "",
                                          "ver_code": activationCell?.verificationCodeTxtField.text ?? ""])
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: activationCell?.validationMessage)
            }
        default:
            break
        }
    }
    
}
