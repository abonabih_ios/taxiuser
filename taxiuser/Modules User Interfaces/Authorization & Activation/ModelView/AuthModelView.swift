//
//  AuthModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import UIKit

class AuthModelView: ModelView
{
    unowned let scene: AuthViewController
    
    
    init(scene: AuthViewController) {
        self.scene = scene
    }

    
    //MARK:- Register
    func register(data:[String:Any]){
        let registerationService = RegisterationService()
        registerationService.saveUserData(data: data)
        executeService(service: registerationService, userData: data, tag: RegisterationService.className)
    }
    
    //MARK:- Login
    func login(data:[String:Any]){
        let loginService = LoginService()
        executeService(service: loginService, userData: data, tag: LoginService.className)
    }
    
    
    //MARK:- Activation
    func activate(data:[String:Any]){
        let activationService = ActivationService()
        executeService(service: activationService, userData: data, tag: ActivationService.className)
    }
    
    
    //MARK:- Activation
    func resendVerificationCode(data:[String:Any]){
        let resendCodeService = ResendVerificationCodeService()
        executeService(service: resendCodeService, userData: data, tag: ResendVerificationCodeService.className)
    }

    
    //MARK:- Activation
    func cancelVerification(data:[String:Any]){
        let cancelVerificationService = CancelVerificationService()
        executeService(service: cancelVerificationService, userData: data, tag: CancelVerificationService.className)
    }

    
    //MARK:-
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case LoginService.notificationName:
            let userVerified = LoggedUser.shared?.userVerified ?? false
            
            if userVerified {
                scene.didLoginOrActivatedASucessfully()
            }
            else {
                scene.showActivation()
            }
        case RegisterationService.notificationName:
            scene.showActivation()
        case ActivationService.notificationName:
            scene.didLoginOrActivatedASucessfully()
        case CancelVerificationService.notificationName:
            scene.showLogin()
        case ResendVerificationCodeService.notificationName:
            scene.showDefaultAlert(title: StringConstant.Info, message: "Verification code has been sent to your mobile number")
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[DataReceivedWithErrorKey] as? AAError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)

//        switch notification.name {
//        case LoginService.notificationName:
//            scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
//        case RegisterationService.notificationName:
//            break
//        case ActivationService.notificationName:
//            break
//        default:
//            break
//        }
        super.errorDidReceived(notification: notification)
    }
}
