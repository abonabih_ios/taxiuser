//
//  AuthRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import UIKit

protocol AuthRouterInput{
    func navigateToLogin()
    func navigateToRegistration()
    func navigateToActivation()
    func navigateToHomeScene()
}


class AuthRouter: SceneRouter, AuthRouterInput
{
    unowned let viewController: AuthViewController
    
    init(scene: AuthViewController) {
        self.viewController = scene
    }
    
    func navigateToLogin() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.login.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }
    
    func navigateToRegistration() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.register.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }

    func navigateToActivation() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.activation.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }

    func navigateToHomeScene() {
        self.viewController.present(SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main")!, animated: true, completion: nil)
    }
}
