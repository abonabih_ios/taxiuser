//
//  ActivationCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit


protocol ActivationCellDelegate:class {
    func resendCode(code:String)
}


class ActivationCell: UICollectionViewCell
{
    
    @IBOutlet weak var verificationCodeTxtField: UITextField!
    
    weak var delegate:ActivationCellDelegate?
    
    
    var valid:Bool = false
    var validationMessage:String = ""

    
    
    @IBAction func resendCode(_ sender: Any) {
        delegate?.resendCode(code: verificationCodeTxtField.text ?? "")
    }
    
    
    
    //MARK:- Validate
    func validate() {
        
        let length = verificationCodeTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).length ?? 0
        
        if length == 0 {
            valid = false
            validationMessage = "Please enter verification code"
        }
        else {
            valid = true
            validationMessage = ""
        }
    }
    
}
