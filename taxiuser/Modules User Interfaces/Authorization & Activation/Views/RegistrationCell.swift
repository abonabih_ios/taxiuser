//
//  RegistrationCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit
import PhoneNumberKit



protocol RegistrationCellDelegate:class{
}

class RegistrationCell: UICollectionViewCell
{
    @IBOutlet weak var maleBtnView: RoundedCornersView!
    @IBOutlet weak var femaleBtnView: RoundedCornersView!
    @IBOutlet weak var passwordImgView: UIImageView!

    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!

    
    

    
    var valid:Bool = false
    var validationMessage:String = ""
    
    var gender:String = "1"

    
    
    @IBAction func selectGender(_ sender: UIButton) {
        gender = "\(sender.tag)"
        
        if sender.tag == 1 {
            maleBtnView.backgroundColor = CustomStyle.themeColor
            femaleBtnView.backgroundColor = .clear
        }
        else {
            maleBtnView.backgroundColor = .clear
            femaleBtnView.backgroundColor = CustomStyle.themeColor
        }
    }
    
    
    @IBAction func showHidePassword(_ sender: UIButton) {
        let count = passwordTxtField.text?.characters.count ?? 0
        
        if count > 0 {
            passwordTxtField.isSecureTextEntry = !passwordTxtField.isSecureTextEntry
            
            passwordImgView.image =  passwordTxtField.isSecureTextEntry ? #imageLiteral(resourceName: "login_show_password") : #imageLiteral(resourceName: "login_hide_password")
        }
    }
    
    
    
    //MARK:- Validate
    func validate() {
        
        if nameTxtField.text?.validateWith(pattern: "[a-zA-Z]{3,20}") != true {
            valid = false
            validationMessage = "Please enter valid full name"
        }
        else {
            do {
                let phoneNumberKit = PhoneNumberKit()
                
                let mobile = mobileTxtField.text ?? ""
                
                _ = try phoneNumberKit.parse("\(AppDelegate.CountryCode)\(mobile)")
                
                let emailLength = emailTxtField.text?.length ?? 0
                
                if emailTxtField.text?.validateWith(pattern: "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$") != true && emailLength > 0 {
                    valid = false
                    validationMessage = "Please enter valid email"
                }
                else if passwordTxtField.text?.validateWith(pattern: "[0-9a-zA-Z]{6,}") != true {
                    valid = false
                    validationMessage = "Password must be at least 6 characters"
                }
                else {
                    valid = true
                    validationMessage = ""
                }
            }
            catch {
                valid = false
                validationMessage = "Please enter valid mobile number"
            }
        }
        
    }

}
