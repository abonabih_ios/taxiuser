//
//  BookedRideDetialsViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol BookedRideDetialsViewControllerOuput {
    func confirm(_ sender: UIButton)
    func back(_ sender: UIButton)

}


class BookedRideDetialsViewController:UIViewController, BookedRideDetialsViewControllerOuput
{
    var modelView: BookedRideDetailsModelView?
    var router: BookedRideDetailsRouter?
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItems()
        router = BookedRideDetailsRouter(scene: self)
        modelView = BookedRideDetailsModelView(scene: self)

    }

    
    private func setUpNavigationItems() {
        self.title = StringConstant.RideDetails
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        backBtn.contentMode = .center
       backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "back_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }

    @IBAction func back(_ sender: UIButton){
        router?.backToRidesList()
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        
    }
}
