//
// BookedRideDetailsParentRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol BookedRideDetailsRouterInput{
    func backToRidesList()
}

class BookedRideDetailsRouter: SceneRouter, BookedRideDetailsRouterInput
{
    unowned let viewController: BookedRideDetialsViewController
    
    init(scene: BookedRideDetialsViewController) {
        self.viewController = scene
    }
    
    func backToRidesList() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
