//
//  BookedRidesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol BookedRidesViewControllerOuput {
}


class BookedRidesViewController:UIViewController,UITableViewDelegate, UITableViewDataSource, BookedRidesViewControllerOuput
{
        
    @IBOutlet weak var tableView:UITableView!
    
    
    
    var modelView: BookedRidesModelView?
    var router: BookedRidesRouter?
    
    var bookKedRides:[BookedRide]? {didSet{tableView.reloadData()}}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItems()
        router = BookedRidesRouter(scene: self)
        modelView = BookedRidesModelView(scene: self)
        
        showLoadingIndicator()
        modelView?.fetchBookedRidesList()
    }

    
    private func setUpNavigationItems() {
        self.title = StringConstant.BookedRides
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        backBtn.contentMode = .scaleAspectFit
       backBtn.addTarget(self, action: #selector(showSideMenu), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }

    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    
    //MARK:- UITableView Delegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.openRideDetails()
    }
    //MARK:- UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookKedRides?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BookedRideCell.classForCoder())) as? BookedRideCell
        
        if let ride = bookKedRides?[indexPath.row] {
            cell?.tripTypeImgView.image = ride.rideTtype == .basic ? #imageLiteral(resourceName: "bok_list_basic") : #imageLiteral(resourceName: "bok_list_kids")
            
            if ride.rideTtype == .basic {
                cell?.tripImgView.image = ride.gender == .male ? #imageLiteral(resourceName: "bok_trip_profiles_man") : #imageLiteral(resourceName: "bok_trip_profiles_woman")
 
            }
            else {
                cell?.tripImgView.image = ride.gender == .boy ? #imageLiteral(resourceName: "bok_trip_profiles_boy") : #imageLiteral(resourceName: "bok_trip_profiles_girl")
            }

            
            cell?.tripTypeLbl.text = ride.rideTtype == .basic ?  "Basic trip" : "Kids trip"
            cell?.pickupLocLbl.text = ride.addressPick
            cell?.dropOffLocLbl.text = ride.addresDrop
            
            
            cell?.timeLbl.text = ride.dateTime



        }
        return cell!
    }

    
}
