//
//  BookedRidesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol BookedRidesRouterInput{
    func openRideDetails()
}

class BookedRidesRouter: SceneRouter, BookedRidesRouterInput
{
    unowned let viewController: BookedRidesViewController
    
    init(scene: BookedRidesViewController) {
        self.viewController = scene
    }
    
    func openRideDetails() {
        let bookedRidesVC = BookedRideDetialsViewController.instanceFromStoryboard(storyboardName: "BookedRides_Storyboard")
        viewController.navigationController?.pushViewController(bookedRidesVC!, animated: true)
    }
}
