//
//  Strings.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/2/16.
//   All rights reserved.
//

struct StringConstant
{
    //-----------------------------Common------------------------------
    //MARK:- Common
    static let Cancel = "button_cancel".localized()
    static let Ok = "button_ok".localized()
    static let Info = "info".localized()

    
    
    //-----------------------------Home--------------------------------
    //MARK:-- Home
    static let HomeTitle = "home_title_home".localized()

    
    
    
    //------------------------------Trip-------------------------------
    //MARK:-- Trip
    static let TripTitle = "trip_title".localized()
    static let DatePickerTitle = "date_picker".localized()

    static let Now = "now".localized()
    static let Later = "later".localized()
    static let Back = "back".localized()
    static let Next = "next".localized()
    static let Start = "start".localized()

    
    
    
    //--------------------------Booked Rides----------------------------
    //MARK:-- Booked Rides
    //Title
    static let RideDetails = "ride_details_title".localized()


    
    
    //---------------------------Side Menu------------------------------
    //MARK:- Side Menu

    //section 1
    static let Home = "side_menu_item_home".localized()
    static let BookedRides = "side_menu_item_booked_rides".localized()
    static let FavoriteLocations = "side_menu_item_favorite_locations".localized()
    static let Settings = "side_menu_item_settings".localized()
    static let Help = "side_menu_item_help".localized()
    static let About = "side_menu_item_about".localized()
    static let Logout = "side_menu_item_logout".localized()
    static let Communicate = "side_menu_item_communicate".localized()
    static let Share = "side_menu_item_share".localized()
    static let CallUs = "side_menu_item_call_us".localized()
    static let EmailUs = "side_menu_item_email_us".localized()

    
    //Side menu messages
    static let LogoutMessage = "info_message_logout".localized()

    
    
    //---------------------------Favorites-------------------------------
    static let Delete = "delete".localized()
    static let Pick_place_msg = "pick_place_msg".localized()
    static let Business = "business".localized()
    

    //--------------------------------Rates------------------------------
     static let  Rates = "rates".localized()
     static let  BasicRide = "basic_ride".localized()
     static let  KidsRide = "kids_ride".localized()
     static let  Economy = "economy".localized()
    
    
    
    //----------------------------Place picker---------------------------
    static let  PlacePicker = "place_picker".localized()
    static let  Map = "map".localized()
    static let  Favorite = "favorite".localized()
    static let  Nearby = "nearby".localized()
    static let  Recent = "recent".localized()
    
    //-----------------------------Messages-----------------------------
    static let  CannotGetLocationMsg = "cannot_get_location_msg".localized()
    

}
