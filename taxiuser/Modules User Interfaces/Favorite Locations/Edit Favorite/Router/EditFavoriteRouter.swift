//
//  EditFavoriteRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

protocol EditFavoriteRouterInput:class {
    
}


class EditFavoriteRouter: SceneRouter,EditFavoriteRouterInput
{
    unowned let viewController: EditFavoriteViewController
    
    init(scene: EditFavoriteViewController) {
        self.viewController = scene
    }

}
