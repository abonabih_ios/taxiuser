//
//  EditFavoriteViewControllerDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/8/17.
//
//


extension FavoritePlacesViewController:EditFavoriteViewControllerDelegate
{
    func didEndEditFavoritePlace(place:Place){
        model?.addFavoritePlace(place:place)
        model?.loadFavoritePlaces()
        router?.dismissEditPopupVC()
    }
    
    func pickFavoritePlace(){
        router?.didEndEditFavorite()
    }
}
