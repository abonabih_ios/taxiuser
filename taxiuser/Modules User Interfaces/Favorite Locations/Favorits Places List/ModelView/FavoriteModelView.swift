//
//  FavoriteModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

class FavoriteModelView: ModelView
{
    
    unowned let scene: FavoritePlacesViewController
    
    
    init(scene: FavoritePlacesViewController) {
        self.scene = scene
    }
    

    
    //MARK:- Load Favorites
    func loadFavoritePlaces(){
        scene.favoritePlaces = FavoriteService.favoritePlaces()
    }
    
    
    func addFavoritePlace(place:Place){
        FavoriteService.addPlaceToFavorites(place: place)
    }

    func removePlace(place:Place){
        FavoriteService.removePlaceFromFavorites(place: place)
    }
    
}
