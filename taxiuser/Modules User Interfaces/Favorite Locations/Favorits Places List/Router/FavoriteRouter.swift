//
//  FavoriteRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import PopupController



protocol FavoriteRouterInput:class{
    func showPlacePicker()
    func showEditFavorite(place:Place?)
}


class FavoriteRouter: SceneRouter, FavoriteRouterInput
{
    unowned let viewController: FavoritePlacesViewController
    
    var editFavoritePopupVC:PopupController?
    

    init(scene: FavoritePlacesViewController) {
        self.viewController = scene
    }
    
    
    
    //MARK:- FavoriteRouterInput
    func showPlacePicker() {
        let  placePickerViewController = PlacePickerViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as? PlacePickerViewController
        
        placePickerViewController?.tabs = [.map, .nearby, .recent]
        
        placePickerViewController?.didSelectPlaceHandler = { placeData in
            
            let place = Place(value:placeData)
            
            self.showEditFavorite(place: place)
        }
        
        self.viewController.navigationController?.pushViewController(placePickerViewController!, animated: true)
    }
    
    func showEditFavorite(place:Place? = nil){
        let editFavoriteVC = EditFavoriteViewController.instanceFromStoryboard(storyboardName: "Favorites_Storyboard") as? EditFavoriteViewController
        
        editFavoriteVC?.place = place
       editFavoriteVC?.delegate = viewController
        
        editFavoritePopupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.bottom),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(editFavoriteVC!)
    }
    
    func dismissEditPopupVC(){
        editFavoritePopupVC?.closePopup(){
            
        }
    }
    
    
    func didEndEditFavorite(){
        editFavoritePopupVC?.closePopup(){[weak self] in
            self?.showPlacePicker()
        }
    }

}
