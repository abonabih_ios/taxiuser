//
//  HelpRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol HelpRouterInput{
     func navigateLegalNotes()
}

class HelpRouter: SceneRouter, HelpRouterInput
{
    unowned let viewController: HelpViewController
    
    init(scene: HelpViewController) {
        self.viewController = scene
    }
    
    
    func navigateLegalNotes() {
        let legalNotesVC = LegalNotesViewController.instanceFromStoryboard(storyboardName: "Help_Storyboard")
        viewController.navigationController?.pushViewController(viewController: legalNotesVC!){
        }
    }

}
