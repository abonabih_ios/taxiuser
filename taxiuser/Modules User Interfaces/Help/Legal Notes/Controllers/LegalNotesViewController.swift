//
//  LegalNotesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit

class LegalNotesViewController: UIViewController
{
    
    var router: LegalNotesRouter?

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationItems()
        
        router = LegalNotesRouter(scene: self)

    }
    
    
    private func setUpNavigationItems() {
        self.title = "Legal Notes"
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "back_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        router?.back()
    }
    
    
    
}
