//
//  LegalNotesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//
import UIKit


protocol LegalNotesRouterInput{
    func back()
}

class LegalNotesRouter: SceneRouter, LegalNotesRouterInput
{
    unowned let viewController: LegalNotesViewController
    
    init(scene: LegalNotesViewController) {
        self.viewController = scene
    }
    
    func back() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
