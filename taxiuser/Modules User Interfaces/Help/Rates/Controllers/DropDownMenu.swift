//
// DropDownMenu.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import MKDropdownMenu



extension RatesViewController: MKDropdownMenuDataSource, MKDropdownMenuDelegate
{
    //MARK:- MKDropdownMenuDataSource
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 1
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        guard let selectedItemView = view as? RateMenuCell
            else {
                
                let menuItemView = UINib(nibName:  String(describing: RateMenuCell.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RateMenuCell
                
                if dropdownMenu ==  tripTypeMenu {
                    configureTripTypeMenuCell(view: menuItemView ,row: row)
                }
                else if dropdownMenu ==  tripCostMenu  {
                    configureCostMenuuCell(view: menuItemView ,row: row)
                }
                
                return menuItemView
        }
        
        
        if dropdownMenu ==  tripTypeMenu {
            configureTripTypeMenuCell(view: selectedItemView ,row: row)
        }
        else if dropdownMenu ==  tripCostMenu  {
            configureCostMenuuCell(view: selectedItemView ,row: row)
        }

        
        return selectedItemView
        
    }
    
    internal func configureTripTypeMenuCell(view: RateMenuCell, row:Int) {
        
        switch row {
        case 0:
            view.textLabel.text = StringConstant.BasicRide
            view.imgView.image = UIImage(named:"rates_business")
        case 1:
            view.textLabel.text = StringConstant.KidsRide
            view.imgView.image = UIImage(named:"rates_kids")
        default:
            break
        }
    }
    
    
    internal func configureCostMenuuCell(view: RateMenuCell, row:Int) {
        
        switch row {
        case 0:
            view.textLabel.text = StringConstant.Business
            view.imgView.image = UIImage(named:"rates_business")
        case 1:
            view.textLabel.text = StringConstant.Economy
            view.imgView.image = UIImage(named:"rates_econmy")
        default:
            break
        }
    }

    
    //MARK:- MKDropdownMenuDelegate
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        
        tripTypeMenu.closeAllComponents(animated: true)
        tripCostMenu.closeAllComponents(animated: true)

        if dropdownMenu ==  tripTypeMenu {
            configureTripTypeMenuCell(view: selctedTypeItemView ,row: row)
        }
        else if dropdownMenu ==  tripCostMenu  {
            configureCostMenuuCell(view: selctedCostTypeItemView ,row: row)
        }
    }
}
