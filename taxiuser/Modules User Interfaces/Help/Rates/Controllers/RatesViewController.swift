//
//  RatesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import MKDropdownMenu


protocol RatesViewControllerOuput {
}


class RatesViewController:UITableViewController,RatesViewControllerOuput
{

    @IBOutlet weak var tripTypeMenu: MKDropdownMenu!
    @IBOutlet weak var tripCostMenu: MKDropdownMenu!

    @IBOutlet weak var selctedTypeItemView: RateMenuCell!
    
    @IBOutlet weak var selctedCostTypeItemView: RateMenuCell!

    
    var modelView: RatesModelView?
    var router: RatesRouter?

    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        router = RatesRouter(scene: self)
        setUpNavigationItems()
        
        configureMenus()
    }
    
    
    fileprivate func configureMenus(){
        configureTripTypeMenuCell(view: selctedTypeItemView ,row: 0)
        configureCostMenuuCell(view: selctedCostTypeItemView ,row: 0)
    }
    
    private func setUpNavigationItems() {
        self.title = StringConstant.Rates
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "back_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        tripTypeMenu.closeAllComponents(animated: false)
        tripCostMenu.closeAllComponents(animated: false)
        router?.back()
    }
    
    // MARK: - Actions
    @IBAction func selectRideType(_ sender: UIButton) {
        
    }
    
    @IBAction func selectLevel(_ sender: UIButton) {
    }
}
