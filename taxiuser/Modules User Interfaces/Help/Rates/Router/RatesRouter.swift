//
//  RatesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol RatesRouterInput{
    func back()
}

class RatesRouter: SceneRouter, RatesRouterInput
{
    unowned let viewController: RatesViewController
    
    init(scene: RatesViewController) {
        self.viewController = scene
    }
    
    func back() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
