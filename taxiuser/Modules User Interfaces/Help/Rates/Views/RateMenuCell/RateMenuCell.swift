//
//  RateMenuCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/13/17.
//
//

import UIKit

class RateMenuCell: UIView
{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    
}
