//
//  FavoritePickerViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import XLPagerTabStrip




class FavoritePickerViewController:UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource
{

    
    @IBOutlet weak var tableView:UITableView!
    
    
    var itemInfo = IndicatorInfo(title: "-")
    
    
    private var favoritePlaces:[Place]?
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoritePlaces =  FavoriteService.favoritePlaces()
        tableView.reloadData()
    }
    
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = favoritePlaces?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell") as? PlaceCell
        
        cell?.placeNameLbl.text = favoritePlaces?[indexPath.row].name
        cell?.addressLbl.text = favoritePlaces?[indexPath.row].address
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let place = favoritePlaces?[indexPath.row]
            else { return }
        let parentVC = parent as? PlacePickerViewController
        
        
        let data:[String:Any] = ["latitude": place.latitude, "longitude": place.longitude, "name": place.name, "address": place.address]
        
        navigationController?.popViewControllerWithHandler(){
            parentVC?.didSelectPlaceHandler?(data)
        }
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
