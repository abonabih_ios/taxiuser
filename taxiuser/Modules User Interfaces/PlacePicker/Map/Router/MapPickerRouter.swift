//
//  MapPickerRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit

protocol MapPickerRouterInput{
    func pickLocation()
}


class MapPickerRouter: SceneRouter,MapPickerRouterInput
{
    
    unowned let viewController: PickerMapViewController
    
    init(scene: PickerMapViewController) {
        self.viewController = scene
    }

    
    func pickLocation() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
