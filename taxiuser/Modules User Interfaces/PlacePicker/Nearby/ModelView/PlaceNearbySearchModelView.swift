//
//  PlaceNearbySearchModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/9/17.
//
//
import Foundation


class PlaceNearbySearchModelView: ModelView
{
    unowned let scene: NearbyViewController
    
    
    init(scene: NearbyViewController) {
        self.scene = scene
    }
    
    
    func searchNearby(data:[String:Any]){
        executeService(service: PlaceSearchService(), userData: data, tag: PlaceSearchService.className)
    }
    
    
    //MARK:-
    override func dataDidReceived(notification: NSNotification) {
        switch notification.name {
        case PlaceSearchService.notificationName:
            scene.results = notification.userInfo?[DataReceivedKey] as? [PlaceItem]
            scene.tableView.reloadData()
            scene.hideLoadingIndicator()
            
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        switch notification.name {
        case LoginService.notificationName:
            break
        default:
            break
        }
        super.errorDidReceived(notification: notification)
    }
}
