//
//  NearbyPlaceCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit

class NearbyPlaceCell: UITableViewCell
{
    @IBOutlet weak var placeNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
}
