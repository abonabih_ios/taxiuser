//
//  RatesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import XLPagerTabStrip
import GooglePlaces


protocol PlacePickerViewControllerOuput {
}


class PlacePickerViewController: ButtonBarPagerTabStripViewController, PlacePickerViewControllerOuput
{
    @IBOutlet weak var shadowView: UIView!

    
    var didSelectPlaceHandler: ((_ placeData:[String:Any]) -> Void)?
    var didCancelHandler: ((Void) -> Void)?

    
    var modelView: PlacePickerModelView?
    var router: PlacePickerRouter?
    
    
    
    enum PlacePickerTab:Int{
        case map
        case favorite
        case nearby
        case recent
    }

    
    var tabs:[PlacePickerTab] = [.map, .favorite, .nearby, .recent]
    
    
    override func viewDidLoad() {
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = .darkGray
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .red
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .red
            newCell?.label.textColor = .darkGray
        }
        super.viewDidLoad()
        
        router = PlacePickerRouter(scene: self)
        modelView = PlacePickerModelView(scene: self)

        setUpNavigationItems()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moveToViewController(at: 0)
        settings.style.selectedBarHeight = 3.0
        containerView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.PlacePicker
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "place_pick_back"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        router?.back()
    }

    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        
        var tabsControllers:[UIViewController] = []
        
        for tab in tabs {
            switch tab {
            case .map:
                let mapVC = PickerMapViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as! PickerMapViewController
                mapVC.itemInfo = IndicatorInfo(title: StringConstant.Map)
                tabsControllers.append(mapVC)
            case .favorite:
                let favoriteVC = FavoritePickerViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as! FavoritePickerViewController
                favoriteVC.itemInfo = IndicatorInfo(title: StringConstant.Favorite)
                tabsControllers.append(favoriteVC)
            case .nearby:
                let nearbyVC = NearbyViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as! NearbyViewController
                nearbyVC.itemInfo = IndicatorInfo(title: StringConstant.Nearby)
                tabsControllers.append(nearbyVC)
            case .recent:
                let recentVC = RecentPlacesViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as! RecentPlacesViewController
                recentVC.itemInfo = IndicatorInfo(title: StringConstant.Recent)
                tabsControllers.append(recentVC)
            }
        }
        
        
        return tabsControllers
    }
    
    
    @IBAction func search(_ sender: UIButton) {
        //TODO:- Persent Search Result Controler
    }
    
}
