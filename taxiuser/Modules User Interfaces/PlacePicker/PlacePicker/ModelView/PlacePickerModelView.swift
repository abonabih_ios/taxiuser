//
//  RatesModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

class PlacePickerModelView: ModelView
{
    unowned let scene: PlacePickerViewController
    
    
    init(scene: PlacePickerViewController) {
        self.scene = scene
    }

        
    func searchByText(data:[String:Any]){
        // location=-33.8670522,151.1957362&radius=500&keyword=cruise&key=YOUR_API_KEY
        executeService(service: PlaceSearchService(), userData: data, tag: "PlaceTextSearch")
    }
}
