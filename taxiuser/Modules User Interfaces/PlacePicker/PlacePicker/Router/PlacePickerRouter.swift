//
//  RatesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol PlacePickerRouterInput{
    func back()
}

class PlacePickerRouter: SceneRouter, PlacePickerRouterInput
{
    unowned let viewController: PlacePickerViewController
    
    init(scene: PlacePickerViewController) {
        self.viewController = scene
    }
    
    func back() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
