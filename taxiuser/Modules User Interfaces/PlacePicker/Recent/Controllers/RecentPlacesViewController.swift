//
//  NearbyController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import XLPagerTabStrip




class RecentPlacesViewController:UIViewController, IndicatorInfoProvider,UITableViewDelegate, UITableViewDataSource
{

    
    @IBOutlet weak var tableView:UITableView!
    
    
    var itemInfo = IndicatorInfo(title: "-")
    
    
    var modelView: RecentPlacesModelView?
    
    
    var recentPlaces:[RecentPlace]?
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        modelView = RecentPlacesModelView(scene: self)
        
    
        modelView?.loadRecentPlaces()
        tableView.reloadData()
        
    }
    
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentPlaces?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentPlaceCell") as? RecentPlaceCell
        
        let place = recentPlaces?[indexPath.row]
        
        
        cell?.placeNameLbl.text = place?.name
        cell?.addressLbl.text = place?.address
        
        let delteAction = ABUITableViewCellRowAction(title: StringConstant.Delete) {
            self.modelView?.removeRecentPlace(place: place!)
            self.recentPlaces?.remove(value: place!)
            self.tableView.reloadData()
        }
        delteAction.backgroundColor  = .red
        cell?.rightActions = [delteAction]

        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let place = recentPlaces?[indexPath.row]
            else { return }
        let parentVC = parent as? PlacePickerViewController
        
        
        let data:[String:Any] = ["latitude": place.latitude, "longitude": place.longitude, "name": place.name, "address": place.address]
        
        navigationController?.popViewControllerWithHandler(){
            parentVC?.didSelectPlaceHandler?(data)
        }
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
