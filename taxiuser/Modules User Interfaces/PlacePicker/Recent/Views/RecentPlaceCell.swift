//
//  RecentPlaceCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit

class RecentPlaceCell: ABSwipeableTableViewCell
{
    @IBOutlet weak var placeNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!

    
    
    
    @IBAction func showActions(_ sender: UIButton) {
        let leftActionsCount = leftActions?.count ?? 0
        let rightActionsCount = rightActions?.count ?? 0
        
        if leftActionsCount > 0 {
            moveRightAnimation()
        }
        else if rightActionsCount > 0 {
            moveLeftAnimation()
        }
    }
    
}
