//
//  ChangeEmailViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController
import RealmSwift



class ChangeEmailViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((_ data:Any)->Void)?
    
    
    
    @IBOutlet weak var newEmailTxtField: UITextField!
    @IBOutlet weak var oldEmailTxtField: UITextField!
    
    
    
    var valid:Bool = false
    var validationMessage:String = ""
    
    
    
    //MARK:- Validate
    func validate() {
        
        if newEmailTxtField.text?.isValidEmail() != true {
            valid = false
            validationMessage = "Please enter valid emial"
        }
        else if oldEmailTxtField.text?.isValidEmail() != true {
            valid = false
            validationMessage = "Please enter valid email"
        }
        else if oldEmailTxtField.text != newEmailTxtField.text {
            valid = false
            validationMessage = "Email confirmation mismatch"
        }
        else {
            valid = true
            validationMessage = ""
        }
        
    }
    
    
    
    //MARK:- Actions
    @IBAction func save(_ sender: UIButton) {
        
        validate()
        
        if valid {
            completionHandler?(["user_id": Realm.userRealm.currentUser?.id ?? "", "new_email":newEmailTxtField.text ?? ""])
        }
        else {
            showDefaultAlert(title: StringConstant.Info, message: validationMessage)
        }
    }
    
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 230)
    }
}

