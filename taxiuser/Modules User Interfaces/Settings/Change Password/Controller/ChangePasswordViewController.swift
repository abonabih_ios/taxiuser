//
//  ChangePasswordViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController
import RealmSwift

class ChangePasswordViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((_ data:Any)->Void)?
    
    
    
    @IBOutlet weak var newPasswordTxtField: UITextField!
    @IBOutlet weak var oldPasswordTxtField: UITextField!
    
    
    
    var valid:Bool = false
    var validationMessage:String = ""
    
    
    
    //MARK:- Validate
    func validate() {
        
        if oldPasswordTxtField.text?.validateWith(pattern: "[0-9a-zA-Z]{6,}") != true {
            valid = false
            validationMessage = "Old Password must be at least 6 characters"
        }
        else if newPasswordTxtField.text?.validateWith(pattern: "[0-9a-zA-Z]{6,}") != true {
            valid = false
            validationMessage = "New Password must be at least 6 characters"
        }
        else {
            valid = true
            validationMessage = ""
        }
        
    }
    
    
    
    
    //MARK:- Actions
    @IBAction func showHidePassword(_ sender: UIButton) {
        
        if sender.tag == 1 {
            let count = oldPasswordTxtField.text?.characters.count ?? 0
            
            if count > 0 {
                oldPasswordTxtField.isSecureTextEntry = !oldPasswordTxtField.isSecureTextEntry
                
                let image =  oldPasswordTxtField.isSecureTextEntry ? #imageLiteral(resourceName: "login_show_password") : #imageLiteral(resourceName: "login_hide_password")
                
                sender.setImage(image, for: .normal)
            }
            
        }
        else {
            let count = newPasswordTxtField.text?.characters.count ?? 0
            
            if count > 0 {
                newPasswordTxtField.isSecureTextEntry = !newPasswordTxtField.isSecureTextEntry
                
                let image =  newPasswordTxtField.isSecureTextEntry ? #imageLiteral(resourceName: "login_show_password") : #imageLiteral(resourceName: "login_hide_password")
                
                sender.setImage(image, for: .normal)
            }
        }
    }
    
    
    
    
    //MARK:- Actions
    @IBAction func save(_ sender: UIButton) {
        
        validate()
        
        if valid {
            
            completionHandler?(["old_pass": oldPasswordTxtField.text ?? "", "new_pass": newPasswordTxtField.text ?? "", "user_id":Realm.userRealm.currentUser?.id ?? ""])
        }
        else {
            showDefaultAlert(title: StringConstant.Info, message: validationMessage)
        }
    }
    
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 230)
    }
}
