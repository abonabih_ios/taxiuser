//
//  SettingsViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol SettingsViewControllerOuput {
}


class SettingsViewController:UITableViewController, SettingsViewControllerOuput
{
    var modelView: SettingsModelView?
    var router: SettingsRouter?
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = SettingsRouter(scene: self)
        modelView = SettingsModelView(scene: self)

        setUpNavigationItems()
    }

    
    private func setUpNavigationItems() {
        self.title = StringConstant.Settings
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        backBtn.contentMode = .scaleAspectFit
       backBtn.addTarget(self, action: #selector(showSideMenu), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }

    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    
    //MARK:- UITableView delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            switch indexPath.row {
            case 4:
                router?.showChangeEmailVC()
            case 5:
                router?.showChangePasswordVC()
            case 7:
               //TODO:- Change language here
                break
            default:
                break
            }
    }
}
