//
//  SettingsModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

class SettingsModelView: ModelView
{
    
    unowned let scene: SettingsViewController
    
    
    init(scene: SettingsViewController) {
        self.scene = scene
    }

    
    func changePassword(data:[String:Any]){
        let service = ChangePasswordService()
        executeService(service: service, userData: data, tag: ChangePasswordService.className)
    }
    
    func changeEmail(data:[String:Any]){
        let service = ChangeEmailService()
        executeService(service: service, userData: data, tag: ChangeEmailService.className)
    }
    
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case ChangePasswordService.notificationName:
            scene.showDefaultAlert(title: StringConstant.Info, message: "Password Changed Sucessfully")
        case ChangeEmailService.notificationName:
            scene.showDefaultAlert(title: StringConstant.Info, message: "Email Changed Sucessfully")
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[DataReceivedWithErrorKey] as? AAError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}

