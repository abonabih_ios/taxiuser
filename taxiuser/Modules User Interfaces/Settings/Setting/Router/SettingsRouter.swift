//
//  SettingsRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import PopupController


protocol SettingsRouterInput{
    func showChangePasswordVC()
    func showChangeEmailVC()
}

class SettingsRouter: SceneRouter, SettingsRouterInput
{
    unowned let viewController: SettingsViewController
    
    private var popupVC:PopupController?

    
    init(scene: SettingsViewController) {
        self.viewController = scene
    }
    
    func showChangePasswordVC() {
        let changePasswordVC = ChangePasswordViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")  as? ChangePasswordViewController
        
        
        changePasswordVC?.completionHandler = {[weak self]  data  in
            self?.popupVC?.closePopup(nil)
            self?.viewController.showLoadingIndicator()
            self?.viewController.modelView?.changePassword(data: data as? [String:Any] ?? [:])
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(changePasswordVC!)
    }
    
    func showChangeEmailVC(){
        let changeEmailVC = ChangeEmailViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")  as? ChangeEmailViewController
        
        
        changeEmailVC?.completionHandler = {[weak self]  data  in
            
            self?.viewController.showLoadingIndicator()
            self?.viewController.modelView?.changeEmail(data: data as? [String:Any] ?? [:])

            self?.popupVC?.closePopup(nil)
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(changeEmailVC!)
    }

}
