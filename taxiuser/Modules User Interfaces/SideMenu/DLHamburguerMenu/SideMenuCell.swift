//
//  SideMenuCell.swift
//  SideMenuPOC
//
//  Created by Abdelrahman Ahmed on 9/18/16.
//  Copyright © 2016 Me. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    
    @IBOutlet var detailsLabel: UILabel!
    
    @IBOutlet weak var topSeparatorView: UIView!
    
}
