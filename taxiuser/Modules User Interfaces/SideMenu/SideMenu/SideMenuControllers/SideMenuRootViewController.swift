//
//  SideMenuRootViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class SideMenuRootViewController: DLHamburguerViewController
{
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func awakeFromNib() {
        
        let homeVC = HomeViewController.instanceFromStoryboard(storyboardName: "Main") as! HomeViewController
        
        let navVC = DLHamburguerNavigationController.instanceFromStoryboard(storyboardName: "Main") as! DLHamburguerNavigationController
        
        navVC.viewControllers = [homeVC]
        
        self.contentViewController = navVC
        
        self.menuViewController = SideMenuViewController.instanceFromStoryboard(storyboardName: "Main") as! SideMenuViewController
    }
}
