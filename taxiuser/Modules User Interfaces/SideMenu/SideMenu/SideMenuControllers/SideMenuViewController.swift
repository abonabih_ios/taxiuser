//
//  SideMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit
import RealmSwift


class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImgBtn: UIButton!
    
    
    var modelView:SideMenuModelView?
    var router:SideMenuRouter?
    
    
    
    struct SideMenuItem {
        var subItems: [SideMenuItem]?
        var type: SideMenuItemType
        var action:((Void)->Void)?
        var active:Bool { return type.isActive()}
        var collapsed: Bool = true
        var icon:UIImage? { return type.icon() }
        
        init(type: SideMenuItemType, items: [SideMenuItem]? = nil, action:((Void)->Void)? = nil) {
            self.type = type
            self.subItems = items
            self.action = action
        }
    }
    
    
    
    enum SideMenuItemType:String {
        case mainSection
        case home
        case bookedRides
        case favoriteLocations
        case settings
        case help
        case about
        case logout
        case communicateSection
        case share
        case callUs
        case emailUs
        
        var description:String {
            switch self {
            case .mainSection:
                return ""
            case .home:
                return StringConstant.Home
            case .bookedRides:
                return StringConstant.BookedRides
            case .favoriteLocations:
                return StringConstant.FavoriteLocations
            case .settings:
                return StringConstant.Settings
            case .help:
                return StringConstant.Help
            case .about:
                return StringConstant.About
            case .logout:
                return StringConstant.Logout
            case .communicateSection:
                return StringConstant.Communicate
            case .share:
                return StringConstant.Share
            case .callUs:
                return StringConstant.CallUs
            case .emailUs:
                return StringConstant.EmailUs
                
            }
            
        }
        
        func icon() -> UIImage? {
            switch self {
            case .mainSection:
                return nil
            case .home:
                return #imageLiteral(resourceName: "nav_start_trip_g")
            case .bookedRides:
                return #imageLiteral(resourceName: "nav_booked_rides_g")
            case .favoriteLocations:
                return #imageLiteral(resourceName: "nav_favorites_g")
            case .settings:
                return #imageLiteral(resourceName: "nav_settings_g")
            case .help:
                return #imageLiteral(resourceName: "nav_help_g")
            case .about:
                return #imageLiteral(resourceName: "nav_about_g")
            case .logout:
                return #imageLiteral(resourceName: "nav_logout_g")
            case .communicateSection:
                return nil
            case .share:
                return #imageLiteral(resourceName: "nav_email_g")
            case .callUs:
                return #imageLiteral(resourceName: "nav_call_g")
            case .emailUs:
                return #imageLiteral(resourceName: "nav_email_g")
            }
        }
        
        
        func isActive() -> Bool {
            switch self {
            default: return true
            }
        }
    }
    
    
    
    
    var selectedItem:SideMenuItemType = .home {didSet{tableView.reloadData()}}
    
    var sectionsMenuItems = [SideMenuItem]() {didSet{ tableView.reloadData() }}
    
    
    
    
    
    //MARK:- View lifecycle
    override func  viewDidLoad(){
        super.viewDidLoad()
        setupItems()
        updateUI()
        modelView = SideMenuModelView(scene: self)
        router = SideMenuRouter(scene: self)
        
        let userPlaceholderImage = LoggedUser.shared?.userGender == .male ? #imageLiteral(resourceName: "menu_profiles_man") : #imageLiteral(resourceName: "menu_profiles_woman")
        
        userImgBtn.setImage(userPlaceholderImage, for: .normal)
    }
    
    private func setupItems(){
        let mainSectionMenuItems = [
            SideMenuItem(type: .home){[weak self] in
                self?.selectItemWithType(itemType: .home)
            },
            SideMenuItem(type: .bookedRides){[weak self]  in
                self?.selectItemWithType(itemType: .bookedRides)
            },
            SideMenuItem(type: .favoriteLocations){[weak self]  in
                self?.selectItemWithType(itemType: .favoriteLocations)
            },
            SideMenuItem(type: .settings){[weak self]  in
                self?.selectItemWithType(itemType: .settings)
            },
            SideMenuItem(type: .help
            ){[weak self] in
                self?.selectItemWithType(itemType: .help)
            }
            ,
            SideMenuItem(type: .about
            ){[weak self] in
                self?.selectItemWithType(itemType: .about)
            }
            ,
            SideMenuItem(type: .logout
            ){[weak self] in
                self?.selectItemWithType(itemType: .logout)
            }
            ].filter{ $0.active }
        
        let communicateSectionMenuItems = [
            SideMenuItem(type: .share
            ){[weak self] in
                self?.selectItemWithType(itemType: .share)
            }
            ,
            SideMenuItem(type: .callUs
            ){[weak self] in
                self?.selectItemWithType(itemType: .callUs)
            }
            ,
            SideMenuItem(type: .emailUs
            ){[weak self] in
                self?.selectItemWithType(itemType: .emailUs)
            }
            ].filter{ $0.active }

        
        sectionsMenuItems = [
            SideMenuItem(type: .mainSection, items: mainSectionMenuItems){[weak self] in
                self?.selectItemWithType(itemType: .mainSection)
            },
            SideMenuItem(type: .communicateSection, items: communicateSectionMenuItems){[weak self]  in
                self?.selectItemWithType(itemType: .communicateSection)
            }
            ].filter{ $0.active }
        
    }
    
    
    private func updateUI(){
    }
    
    
    
    //MARK:- items actions
    private func performAction(action:@escaping (Void)->Void){
        
        if let hamburguerViewController = self.findHamburguerViewController() {
            
            hamburguerViewController.hideMenuViewControllerWithCompletion({ () -> Void in
                action()
            })
        }
    }
    
    private func selectItemWithType(itemType:SideMenuItemType) {
        switch itemType {
        case .home:
            selectedItem = .home
            router?.navigateToHome()
        case .bookedRides:
            selectedItem = .bookedRides
            router?.navigateToBookedRides()
        case .favoriteLocations:
            selectedItem = .favoriteLocations
            router?.navigateToFavorites()
        case .settings:
            selectedItem = .settings
            router?.navigateToSettings()
        case .help:
            selectedItem = .help
            router?.navigateToHelp()
        case .about:
            selectedItem = .about
            router?.navigateToAbout()
        case .logout:
            logout()
        case .share:
            share()
        case .callUs:
            callUs()
        case .emailUs:
            emailUs()
        default: return
        }
    }
    
    
    private func logout(){
        //TODO:- Transition Animation
        showTwoActionsAlert(title: StringConstant.LogoutMessage, firstActionButtontitle: StringConstant.Logout, secondActionButtontitle: StringConstant.Cancel , mesage: nil){[weak self] in
            Realm.userRealm.reset()
            self?.router?.navigateLogin()
        }
    }

    private func callUs(){
        UIApplication.shared.tryOpenUrl(urls: ["tel:0112677711"])
    }
    
    private func share(){
        let textToShare = "Look at this awesome Taxi application"
        let myWebsite = URL(string:"http://www.noonit.com/")!
        let objectsToShare:[Any] = [textToShare, myWebsite]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        let excludeActivities:[UIActivityType] = [.
            airDrop,
       .print,
       .assignToContact,
       .saveToCameraRoll,
        .addToReadingList,
       .postToFlickr,
       .postToVimeo]
        
        activityVC.excludedActivityTypes = excludeActivities
        present(activityVC, animated: true, completion: nil)
    }
    
    private func emailUs(){
        UIApplication.shared.tryOpenUrl(urls: ["mailto:amroo.mosta@gmail.com"])
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    //=========================================================================
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsMenuItems.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = sectionsMenuItems[section].subItems?.count {
            return count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch sectionsMenuItems[section].type {
        case .mainSection:
            return 0
        case .communicateSection:
            return 44
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        if let menuItem = sectionsMenuItems[indexPath.section].subItems?[indexPath.row] {
        selectedItem = menuItem.type
            print(selectedItem)
           menuItem.action?()
        }
    }

    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch sectionsMenuItems[section].type {
        case .mainSection:
            return nil
        case .communicateSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuCell.self)) as! SideMenuCell
            
            cell.titleLabel?.text = sectionsMenuItems[section].type.description
            cell.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell.titleLabel?.textColor = CustomStyle.themeColor
            
            cell.backgroundColor = UIColor.clear
            cell.topSeparatorView.isHidden = false
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            cell.addGestureRecognizer(tapRecognizer)

            return cell
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuCell.self)) as! SideMenuCell

        let menuItem = sectionsMenuItems[indexPath.section].subItems?[indexPath.row]
        
        cell.titleLabel?.text = menuItem?.type.description
        cell.itemImageView.image = menuItem?.icon
        //cell.backgroundColor = menuItem?.type == selectedItem ?  UIColor.lightGray.withAlphaComponent(0.2) :  UIColor.clear
        //cell.titleLabel?.textColor = menuItem?.type == selectedItem ?CustomStyle.themeColor : UIColor.black

        
        return cell
    }
    
    
    
    //MARK:- Buttons actions
    @IBAction func showprofile(sender: UIButton) {
        selectedItem = .settings
        router?.navigateToSettings()
    }
    
    
    // MARK: - Navigation
    //=========================================================================
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewController(withIdentifier: String(describing: DLHamburguerNavigationController.self)) as! DLHamburguerNavigationController
    }
    
}
