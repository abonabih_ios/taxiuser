//
//  ConfigurationViewDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//
import UIKit


extension BookingTripViewController:ConfigurationViewDelegate,KidsConfigurationViewDelegate
{
    
    func selectGender(_ sender:UIButton){
        bookingData["user_type"] = sender.tag
        setMarkIcon()
        
        //TODO:- show policy here
        let showPolicy =  modelView?.shouldShowPolicy() ?? false
        
        if showPolicy {
            router?.showPolicyVC(sourceView:sender)
            
            modelView?.setPolicyTipsFlag(flag: false)
        }
    }
    
    func writeNotes(){
        router?.showNotesVC()
    }
    
    func pickDropffLocation(){
        router?.showPlacePicker(pickUp: false)
    }
    
    func getFareEstimate(){
        let destLat = bookingData["dest_lat"] as? Double ?? 0
        let destLng = bookingData["dest_long"] as? Double ?? 0
        
        
        if !destLat.isZero && !destLng.isZero  {
            showLoadingIndicator()
            modelView?.getFareEstimate(type:bookingType, time:bookingTime,data:bookingData)
        }
        else {
            showDefaultAlert(title: StringConstant.Info, message: "Destination location is required to get fare estimate")
        }
    }
    
    
    //MARK:-
    func showFareEstimate(estimate:Double){
        router?.showFareEstimateVC(estimate: estimate)
    }
    
    
    func addFollowupNumbers() {
        let  followupController = FollowupNumbersViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")
        navigationController?.pushViewController(followupController!, animated: true)
    }
    
    
}
