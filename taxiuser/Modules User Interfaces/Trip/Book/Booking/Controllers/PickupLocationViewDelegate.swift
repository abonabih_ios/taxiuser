//
//  PickupLocationViewDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

extension BookingTripViewController:PickupLocationViewDelegate
{
    func pickPlace(){
        router?.showPlacePicker(pickUp:true)
    }
}
