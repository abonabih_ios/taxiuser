//
//  TripTimeViewDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/12/17.
//
//
import ActionSheetPicker_3_0
import SwiftDate


extension BookingTripViewController:TripTimeViewDelegate
{
    func showDatePicker(){
        router?.showDatePicker()
    }
    
    func showTimePicker(){
        
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: .time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            
            let time = value as? Date ?? Date()

            self.tripTimeView?.selectedTimeLbl.text = time.string(format: .iso8601(options: .withTime))
            
            for date in self.selectedDates {
                let d = date.startOfDay.add(components: [.hour : time.hour, .minute: time.minute, .second: time.second])
                
                if let index =  self.selectedDates.index(of: date) {
                    self.selectedDates.remove(at: index)
                    self.selectedDates.insert(d, at: index)
                }
                
            }
            
            //bookingData["date_time_array"] =
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60
        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
        datePicker?.show()
    }
    
    func repeated(repeat:Bool){
    }
}
