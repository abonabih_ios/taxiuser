//
//  BookingTripModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift


class BookingTripModelView: ModelView
{
    
    unowned let scene: BookingTripViewController
    
    
    init(scene: BookingTripViewController) {
        self.scene = scene
    }
    
    func addRecentPlace(placeData:[String:Any]) {
        let place = RecentPlace(value:placeData)
        RecentService.addPlaceToRecent(place: place)
    }
    
    
    func shouldShowPolicy() -> Bool{
        return Realm.appRealm.settings.policyTipsDidShowFirstTime
    }
    
    
    func setPolicyTipsFlag(flag:Bool){
         Realm.appRealm.setPolicyTipsFlag(flag:flag)
    }

    
    //MARK:- Book a trip
    func bookTrip(type:BookingType, time:BookingTime,  data:[String:Any]){
        
        var params = data
        params["user_id"] = LoggedUser.shared?.id ?? ""

        var requestTag = RideOrderService.BasicOrderNowTag
        
        if type == .basic {
            requestTag =  time == .now ? RideOrderService.BasicOrderNowTag : RideOrderService.BasicOrderLaterTag
        }
        else {
            requestTag =  time == .now ? RideOrderService.KidsNowOrderTag : RideOrderService.KidsOrderLaterTag
        }
        
        let rideOrderService = RideOrderService()
        executeService(service: rideOrderService, userData: rideOrderService.encodedParamters(parameters: params), tag: requestTag)
    }
    
    
    //MARK:- get fare estimate
    func getFareEstimate(type:BookingType, time:BookingTime, data:[String:Any]){
        
        var params = data
        params["user_id"] = LoggedUser.shared?.id ?? ""


        if type == .basic {
            params["ride_type"] =  time == .now ? "1" : "3"
        }
        else {
             params["ride_type"] =  time == .now ? "2" : "4"
        }

        
        let fareEstimateService = FareEstimateService()
        
        executeService(service: fareEstimateService, userData: params , tag: FareEstimateService.className)
    }
    
    
    //MARK:-
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case NSNotification.Name(RideOrderService.KidsOrderLaterTag), NSNotification.Name(RideOrderService.BasicOrderLaterTag):
            
            scene.showDefaultAlert(title: StringConstant.Info, message:"You trip has been booked sucessfully") {[weak self] in
                self?.scene.router?.gotoHomeScene()
            }
        case NSNotification.Name(RideOrderService.BasicOrderNowTag), NSNotification.Name(RideOrderService.KidsNowOrderTag):
            scene.router?.gotoTripScene()
        case FareEstimateService.notificationName:
            let estimate =  notification.userInfo?[DataReceivedKey] as? Double ?? 0.0
            scene.showFareEstimate(estimate: estimate)
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[DataReceivedWithErrorKey] as? AAError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }

        
}
