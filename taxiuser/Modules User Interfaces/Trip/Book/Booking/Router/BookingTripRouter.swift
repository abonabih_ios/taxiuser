//
//  BookingTripRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import PopupController


protocol BookingTripRouterInput{
    func gotoTripScene()
    func showPlacePicker(pickUp:Bool)
    func showDatePicker()
    func showFareEstimateVC(estimate:Double)
    func showPolicyVC(sourceView:UIView)
    func gotoHomeScene()
}

class BookingTripRouter: SceneRouter, BookingTripRouterInput
{
    unowned let viewController: BookingTripViewController
    
    private var popupVC:PopupController?
    
    
    init(scene: BookingTripViewController) {
        self.viewController = scene
    }
    
    
    
    
    
    func showFareEstimateVC(estimate:Double){
        guard let estimateVC = FareEstimateViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? FareEstimateViewController
            else {
                return
        }
        estimateVC.estimate = estimate
        
        estimateVC.completionHandler = {[weak self] in
            self?.popupVC?.closePopup(nil)
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.bottom),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(estimateVC)
    }
    
    func gotoHomeScene(){
        let  homeController = HomeViewController.instanceFromStoryboard(storyboardName: "Main")
        
        self.viewController.navigationController?.viewControllers = [homeController!]
    }

    func gotoTripScene(){
        let  tripViewController = TripViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")
        
        self.viewController.navigationController?.pushViewController(tripViewController!, animated: true)
    }
    
    
    
    func showPlacePicker(pickUp:Bool){
        let  placePickerViewController = PlacePickerViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as? PlacePickerViewController
        
        placePickerViewController?.tabs = [.favorite, .nearby, .recent]
        
        placePickerViewController?.didSelectPlaceHandler =  { placeData in
            
            
            if pickUp {
                self.viewController.bookingData["address"] = placeData["address"] ?? ""
                self.viewController.bookingData["pick_lat"] =  placeData["latitude"]
                self.viewController.bookingData["pick_long"] =  placeData["longitude"]
            }
            else {
                
                self.viewController.bookingData["dest_lat"] =  placeData["latitude"]
                self.viewController.bookingData["dest_long"] =  placeData["longitude"]
            }
            
        }
        
        self.viewController.navigationController?.pushViewController(placePickerViewController!, animated: true)
    }
    
    
    
    func showNotesVC(){
        let  notesController = NotesViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? NotesViewController
        
        notesController?.completionHandler = {[weak self] in
            self?.popupVC?.closePopup(nil)
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.bottom),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(notesController!)
    }

    
    func showDatePicker() {
        let  datePickerViewController = DatePickerViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? DatePickerViewController
        
        datePickerViewController?.completionHandler = { [weak self] dates in
            self?.viewController.selectedDates = dates
            
            if dates.count > 0 {
                self?.viewController.tripTimeView?.selectedDateLbl.text = dates[0].string(format: .iso8601(options: .withFullDate))
            }
            
        }
        
        self.viewController.navigationController?.pushViewController(datePickerViewController!, animated: true)
        
    }
    
    
    func showPolicyVC(sourceView:UIView){
        let  policyViewController = PolicyViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? PolicyViewController
        
        policyViewController?.modalPresentationStyle = .popover
        
        policyViewController?.popoverPresentationController?.delegate = self.viewController
        
        policyViewController?
            .preferredContentSize = CGSize(width:viewController.view.bounds.width - 32 ,height: viewController.view.bounds.height - 380)
        
        let popoverViewController = policyViewController!.popoverPresentationController
        popoverViewController?.permittedArrowDirections = .any
        popoverViewController?.delegate = self.viewController
        popoverViewController?.sourceView = sourceView
        popoverViewController?.sourceRect = sourceView.bounds
        
        popoverViewController?.presentedView?.layer.cornerRadius = 20
        
        popoverViewController?.backgroundColor = CustomStyle.themeColor
        
        
        self.viewController.present(policyViewController!, animated: true, completion: nil)
        
    }
}
