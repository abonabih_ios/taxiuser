//
//  ConfigurationView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit


protocol ConfigurationViewDelegate:class {
    func writeNotes()
    func pickDropffLocation()
    func getFareEstimate()
    func selectGender(_ sender: UIButton)

}

class ConfigurationView: UIView
{
    
    @IBOutlet weak var maleBtn: RoudedCornersButton!
    @IBOutlet weak var femaleBtn: RoudedCornersButton!

    
    weak var delegate:ConfigurationViewDelegate?
    
    
    
    var selectedGender:Int? { didSet{ setGender(gender: selectedGender ?? 0)} }
    
    //MARK:- Actions
    @IBAction func selectGender(_ sender: UIButton) {
        
        selectedGender = sender.tag
        
        delegate?.selectGender(sender)

    }
    
    
    func setGender(gender: Int) {
        let maleImg = gender == 1 ? #imageLiteral(resourceName: "map_profiles_man_r") : #imageLiteral(resourceName: "map_profiles_man_g")
        
        let femaleImg = gender == 2 ? #imageLiteral(resourceName: "map_profiles_woman_r") : #imageLiteral(resourceName: "map_profiles_woman_g")
        
        maleBtn.setImage(maleImg, for: .normal)
        femaleBtn.setImage(femaleImg, for: .normal)
    }
    
    @IBAction func setDropffLocation(_ sender: UIButton) {
        delegate?.pickDropffLocation()
    }
    
    
    @IBAction func writeNotes(_ sender: UIButton) {
        delegate?.writeNotes()
    }
 
    
    @IBAction func showFareEstimate(_ sender: UIButton) {
        delegate?.getFareEstimate()
    }
    
}
