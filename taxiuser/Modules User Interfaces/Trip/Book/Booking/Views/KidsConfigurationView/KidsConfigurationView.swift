//
//  KidsConfigurationView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit

protocol KidsConfigurationViewDelegate:ConfigurationViewDelegate {
    func addFollowupNumbers()
}


class KidsConfigurationView: UIView
{
    @IBOutlet weak var boyBtn: RoudedCornersButton!
    @IBOutlet weak var girlBtn: RoudedCornersButton!
    
    
    weak var delegate:KidsConfigurationViewDelegate?
    
    var selectedGender:Int? { didSet{ setGender(gender: selectedGender ?? 0)} }

    
    
    
    //MARK:- Actions
    @IBAction func selectGender(_ sender: UIButton) {
        
        selectedGender = sender.tag
        
        delegate?.selectGender(sender)
        
    }
    
    
    func setGender(gender: Int) {
        let boyImg = gender == 1 ? #imageLiteral(resourceName: "map_profiles_boy_r") : #imageLiteral(resourceName: "map_profiles_boy_g")
        
        let girlImg = gender == 2 ? #imageLiteral(resourceName: "map_profiles_girl_r") : #imageLiteral(resourceName: "map_profiles_girl_g")
        
        boyBtn.setImage(boyImg, for: .normal)
        girlBtn.setImage(girlImg, for: .normal)
    }
    

    
    @IBAction func setDropffLocation(_ sender: UIButton) {
        delegate?.pickDropffLocation()
    }
    
    
    @IBAction func writeNotes(_ sender: UIButton) {
        delegate?.writeNotes()
    }
    
    
    @IBAction func showFareEstimate(_ sender: UIButton) {
        delegate?.getFareEstimate()
    }
    
    
    @IBAction func followupNUmbers(_ sender: UIButton) {
        delegate?.addFollowupNumbers()
    }

}
