//
//  TripTimeView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/12/17.
//
//

import UIKit

protocol TripTimeViewDelegate:class {
    func showDatePicker()
    func showTimePicker()
    func repeated(repeat:Bool)
}

class TripTimeView: UIView
{
    
    @IBOutlet weak var selectedDateLbl: UILabel!
    @IBOutlet weak var selectedTimeLbl: UILabel!

    weak var delegate:TripTimeViewDelegate?
    
    
    @IBAction func selectDates(_ sender: UIButton) {
        delegate?.showDatePicker()
    }
    
    @IBAction func selectTime(_ sender: UIButton) {
        delegate?.showTimePicker()
    }
 
    @IBAction func repeated(_ sender: UISwitch) {
        delegate?.repeated(repeat: sender.isOn)
    }
    
}
