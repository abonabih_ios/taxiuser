//
//  EditFavoriteViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import PopupController
import SwiftyUserDefaults




class AddFollowupNumberViewController: UIViewController,PopupContentViewController
{

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    
    
    
    var completionHandler: ((Void) -> Void)?

    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    //MARK:- Actions
    @IBAction func saveNumberPlace(_ sender: UIButton) {
        
        let name = nameTxtField.text ?? ""
        let phone = phoneTxtField.text ?? ""
        
        let followup = "\(name),\(phone)"
        
        Defaults[.followupNumbers].append(followup)
        
        completionHandler?()
    }
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 230)
    }
}
