//
//  FavoritePlacesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import SwiftyUserDefaults



class FollowupNumbersViewController: UIViewController,UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView:UITableView!
    
    
    
    var followupNumbers:[String]?{ didSet{tableView.reloadData()} }
    
    var router:FollowupRouter?
    
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = FollowupRouter(scene: self)
        setUpNavigationItems()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    private func setUpNavigationItems() {
        self.title = "Follow up Numbers"
        let menuBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        menuBtn.contentMode = .center
        menuBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        menuBtn.setImage(#imageLiteral(resourceName: "back_ico"), for: .normal)
        let menuItem = UIBarButtonItem(customView: menuBtn)
        navigationItem.leftBarButtonItems = [menuItem]
        
        
        let addPlaceBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:15))
        addPlaceBtn.contentMode = .scaleAspectFill
        addPlaceBtn.addTarget(self, action: #selector(addNewPlace), for: .touchUpInside)
        addPlaceBtn.setImage(#imageLiteral(resourceName: "favorites_add"), for: .normal)
        let addPlaceMenuItem = UIBarButtonItem(customView: addPlaceBtn)
        navigationItem.rightBarButtonItems = [addPlaceMenuItem]

    }
    
    func back(){
        navigationController?.popViewControllerWithHandler(){}
    }
    
    
    
    func addNewPlace(){
        router?.showAddFollowUpVC()
    }
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = followupNumbers?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowUpViewCell") as? FollowUpViewCell
        
        let followup = followupNumbers?[indexPath.row].components(separatedBy: ",")
        
        cell?.placeNameLbl.text = followup?[0]
        cell?.addressLbl.text =  followup?[1]
        
        
        let delteAction = ABUITableViewCellRowAction(title: StringConstant.Delete) {
            
            self.followupNumbers?.remove(at: indexPath.row)
            Defaults[.followupNumbers] = self.followupNumbers!

            self.tableView.reloadData()
        }
        
        delteAction.backgroundColor  = .red


        cell?.rightActions = [delteAction]
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
