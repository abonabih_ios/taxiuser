//
//  FareEstimateViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit
import PopupController
import Cent


class NotesViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((Void)->Void)?
    
    //MARK:- 
    override func viewDidLoad() {
    }
    
    
    @IBAction func done(_ sender: UIButton) {
        completionHandler?()
    }

    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 375)
    }

}
