//
//  FareEstimateViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit


class PolicyViewController: UIViewController
{
    
    
    
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var policyTextView: UITextView!
    
    
    
    
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        view.layoutSubviews()
        policyTextView.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    
    
}
