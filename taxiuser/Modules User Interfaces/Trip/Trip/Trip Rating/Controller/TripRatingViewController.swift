//
//  TripRatingViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController
import HCSStarRatingView



class TripRatingViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((Void)->Void)?
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var notes: UITextView!
    
    
    //MARK:- Actions
    @IBAction func submit(_ sender: UIButton) {
        completionHandler?()
    }
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 390)
    }
}
