//
//  TripReportViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController


class TripReportViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((Void)->Void)?

    //MARK:- Actions
    @IBAction func done(_ sender: UIButton) {
        completionHandler?()
    }

    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 390)
    }
}
