//
//  TripViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/29/16.
//
//

import UIKit
import GoogleMaps
import RealmSwift
import NVActivityIndicatorView
import AlamofireImage


class TripViewController: UIViewController
{
    enum RideStatus:Int {
        case checkingOrderStatus = 0
        case driverOnWay = 1
        case driverArrived = 2
        case onTrip = 3
        case rating = 4
        case tripReport = 5

        init(status:Int) {
            if status >= 5 {
                self = .tripReport
            }
            else if status < 0 {
                self = .checkingOrderStatus
            }
            else {
                self = RideStatus(rawValue:status)!
            }
        }
    }
    
    
    @IBOutlet var  mapView:GMSMapView!
    
    @IBOutlet weak var stepContainerView: UIView!
    @IBOutlet weak var stepContainerViewHieghtConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var radarSearchView: NVActivityIndicatorView!
    
    
    var searchTimer:Timer?
    var rideTimer:Timer?

    
    var modelView: TripModelView?
    var router: TripRouter?
    
    var currentStatus:RideStatus = .checkingOrderStatus {didSet {didChangeTripStatus()}}
    
    
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = TripRouter(scene: self)
        setUpNavigationItems()
        configureMapView()
        
        modelView = TripModelView(scene: self)

        searchTimer =  Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(checkOrderStatus), userInfo: nil, repeats: true)
        
        searchTimer?.fire()
        // addressLbl.text = nil
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        zoomToUserLocation()
        
        radarSearchView.type = .ballScaleMultiple
        radarSearchView.frame = view.bounds
        radarSearchView.setNeedsDisplay()

    }
    
    
    //MARK:- Trip status
    private func didChangeTripStatus(){
        for view in stepContainerView.subviews {
            view.removeFromSuperview()
        }
    }
    
    
    @objc func checkOrderStatus(){
        radarSearchView.type = .ballScaleMultiple
        radarSearchView.startAnimating()
        currentStatus = .checkingOrderStatus
        modelView?.checkOrderStatus()
    }
    
    
    @objc func checkRideStatus(){
        radarSearchView.type = .ballScaleMultiple
        modelView?.checkRideStatus()
    }

    
    func driverOnWayStep(orderStateResponse:OrderStateResponse){
        
        currentStatus = .driverOnWay
        

        searchTimer?.invalidate()
        searchTimer = nil

        rideTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(checkRideStatus), userInfo: nil, repeats: true)

        
        rideTimer?.fire()
        
        
        radarSearchView.stopAnimating()
        
        let driverOnWayView =  UINib(nibName: String(describing: DriverView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverView
        
        driverOnWayView.driverImgView.af_setImage(withURL: URL(string:orderStateResponse.driverPicUrl)!)
        
        driverOnWayView.driverNameLbl.text = orderStateResponse.driverName
        driverOnWayView.carTimeLbl.text = orderStateResponse.carTime
        driverOnWayView.carDistanceLbl.text = orderStateResponse.carDistance
        driverOnWayView.carPlateLbl.text = orderStateResponse.carPlate
        driverOnWayView.carModelLbl.text = "\(orderStateResponse.carModel) \(orderStateResponse.carYear)"


        
        adjustPickerMarker(lat: orderStateResponse.carLat, lng: orderStateResponse.carLong)
        
        
        stepContainerViewHieghtConstraints.constant = 150
        stepContainerView.addSubview(driverOnWayView)
        driverOnWayView.frame = CGRect(x: 0, y: 0, width: stepContainerView.bounds.width, height: 150)
    }
    
    
    
    func driverOnWayStep(rideStateResponse:RideStateResponse){
        
        radarSearchView.stopAnimating()

        radarSearchView.stopAnimating()
        
        let driverOnWayView =  UINib(nibName: String(describing: DriverView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverView
        
        driverOnWayView.carTimeLbl.text = rideStateResponse.carTime
        driverOnWayView.carDistanceLbl.text = rideStateResponse.carDistance

        
        adjustPickerMarker(lat: rideStateResponse.carLat, lng: rideStateResponse.carLong)
        
        
        stepContainerViewHieghtConstraints.constant = 150
        stepContainerView.addSubview(driverOnWayView)
        driverOnWayView.frame = CGRect(x: 0, y: 0, width: stepContainerView.bounds.width, height: 150)
    }

    
    
    func onTripStep(){
        currentStatus = .onTrip
        let view =  UINib(nibName: "DriverOnTripView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        
        stepContainerViewHieghtConstraints.constant = view.bounds.height
        
        stepContainerView.addSubview(view)
        view.frame = stepContainerView.bounds
    }
    
    func driverArrivedStep(){
        currentStatus = .driverArrived

        let view =  UINib(nibName: "CaptainArrivedView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        
        stepContainerViewHieghtConstraints.constant = view.bounds.height
        
        stepContainerView.addSubview(view)
        view.frame = stepContainerView.bounds
    }
    
    func rateDriver(){
        currentStatus = .rating
       router?.showTripRatingVC()
    }
    
    func showTripReport(){
        currentStatus = .tripReport
        router?.showTripReportVC()
    }
    
    
    
    //MARK:-
    func configureMapView(){
        //Set MapView delegate
        //mapView.delegate = self
        
        //Show user location on mapview
        mapView.isMyLocationEnabled = true
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.TripTitle
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .scaleAspectFit
        backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "back_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        navigationController?.popViewControllerWithHandler {}
    }
    
    
    func adjustPickerMarker(lat:Double, lng:Double) {
        mapView.clear()
        
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let pickupLocationMarker = GMSMarker(position: position)
        pickupLocationMarker.icon = #imageLiteral(resourceName: "map_red_car")
        
        pickupLocationMarker.map = mapView
    }
    
    
    func zoomToUserLocation() {
        //Zoom to user location
        if LocationService.sharedInstance.canGetLocation() {
            LocationService.sharedInstance.getCurrentLocation(completion: {[weak self] (coordinate) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.mapView.animate(with: GMSCameraUpdate.setTarget(coordinate, zoom: 16.0))
                }
                
            }){ [weak self] error in
                self?.showDefaultAlert(title: "Info", message: error.localizedDescription)
            }
        }
        else {
            //TODO:- Ask user to enable location service
            
        }
    }
    
    
    
    //MARK:- GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //Get place address of camera target coordintes
        GMSGeocoder().reverseGeocodeCoordinate(position.target) {  (response, error) in
            if let _ = error {
                print(error.debugDescription)
            }
            else {
                if  let count = response?.results()?.count {
                    if count > 0 {
                        //                        self?.addressLbl.text = response!.results()![0].lines!.joined(separator: ",")
                    }
                }
            }
        }
    }
    
    
    //MARK:- Actions
    @IBAction func zoomToUserLocation(_ sender: UIButton) {
        zoomToUserLocation()
    }
    
    @IBAction func toggleMapType(_ sender: UIButton) {
        if mapView.mapType == kGMSTypeNormal {
            mapView.mapType = kGMSTypeHybrid
        }
        else {
            mapView.mapType = kGMSTypeNormal
        }
    }
}
