//
//  TripModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift



class TripModelView: ModelView
{
    
    unowned let scene: TripViewController
    
    
    init(scene: TripViewController) {
        self.scene = scene
    }
    
    func addRecentPlace(placeData:[String:Any]) {
        let place = RecentPlace(value:placeData)
        RecentService.addPlaceToRecent(place: place)
    }
    
    
    //MARK:- Check order status
    func checkOrderStatus(){
        let orderId = Realm.tripRealm.currentOrder?.orderId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let params:[String : Any] = ["order_id":orderId, "user_id":userId, "pick_lat":0, "pick_long":0]
        
        let service = OrderStateService()
        executeService(service: service, userData: params, tag: OrderStateService.className)
    }
    
    func cancelOrder(){
        
        let orderId = Realm.tripRealm.currentOrder?.orderId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        let params:[String : Any] = ["order_id":orderId, "user_id":userId]
        
        let service = CancelOrderService()
        
        executeService(service: service, userData: params, tag: CancelOrderService.className)
    }
    
    
    func checkRideStatus(){
        let rideId = Realm.tripRealm.currentOrder?.rideId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let params:[String : Any] = ["ride_id":rideId,"user_id":userId]
        
        let service = RideStateService()
        executeService(service: service, userData: params, tag: RideStateService.className)
    }
    
    func rateRide(){
    }
    
    func cancelRide(){
    }
    
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case OrderStateService.notificationName:
            
            if let response =  notification.userInfo?[DataReceivedKey] as? OrderStateResponse {
                
                print(response.orderState)
                switch response.orderState {
                case .assigned:
                    scene.driverOnWayStep(orderStateResponse: response)
                case .noDrivers:
                    scene.radarSearchView.stopAnimating()
                    scene.showDefaultAlert(title: StringConstant.Info, message: "There are no nearby drivers") {[weak self] in
                        self?.scene.router?.navigateToHomeScene()
                    }
                case .searching:
                    scene.radarSearchView.startAnimating()
                }
            }
        case CancelOrderService.notificationName:
            break
        case RideStateService.notificationName:
            if let response =  notification.userInfo?[DataReceivedKey] as? RideStateResponse {
                switch response.rideState {
                case .booked:
                    break
                case .onWay:
                    scene.driverOnWayStep(rideStateResponse: response)
                case .arrived:
                    scene.driverArrivedStep()
                case .onTrip:
                    scene.onTripStep()
                case .finished:
                    scene.rideTimer?.invalidate()
                    scene.rideTimer = nil
                    scene.rateDriver()
                case .payed:
                    break
                }
            }
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
//        
//        let error =  notification.userInfo?[DataReceivedWithErrorKey] as? AAError
//        
//        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}
