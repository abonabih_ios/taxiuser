//
//  BookingTripRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import PopupController



protocol TripRouterInput{
    func showTripRatingVC()
    func showTripReportVC()
    func navigateToHomeScene()
}

class TripRouter: SceneRouter, TripRouterInput
{
    unowned let viewController: TripViewController
    
    private var popupVC:PopupController?

    
    
    init(scene: TripViewController) {
        self.viewController = scene
    }
    
    
    func navigateToHomeScene() {
        self.viewController.present(SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main")!, animated: true, completion: nil)
    }

    func showTripRatingVC(){
        let ratingVC = TripRatingViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? TripRatingViewController
        
        
        ratingVC?.completionHandler = {[weak self] in
            self?.popupVC?.closePopup(nil)
            self?.showTripReport()
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.bottom),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(ratingVC!)
    }
    
    func showTripReportVC(){
        showTripReport()

//        if popupVC != nil {
//            popupVC?.closePopup(){
//                self.showTripReport()
//            }
//        }
//        else {
//        }
    }
    
    private func showTripReport(){
        let reportVC = TripReportViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")  as? TripReportViewController
        
        
        reportVC?.completionHandler = {[weak self] in
            self?.popupVC?.closePopup(nil)
            self?.navigateToHomeScene()
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.bottom),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(reportVC!)
    }
}
