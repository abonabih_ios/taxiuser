//
//  CaptainArrivedView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/24/17.
//
//

import UIKit

class CaptainArrivedView: UIView
{
    //Actions
    @IBAction func callDriver(_ sender: UIButton) {
        UIApplication.shared.tryOpenUrl(urls: ["tel:0112677711"])
    }
}
