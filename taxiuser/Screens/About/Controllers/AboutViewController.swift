//
//  AboutViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit


protocol AboutViewControllerOuput {
}


class AboutViewController: UIViewController, AboutViewControllerOuput
{
    
    var router: AboutRouter?
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = AboutRouter(scene: self)
        setUpNavigationItems()
    }
    
    private func setUpNavigationItems() {
        self.title = StringConstant.About
        let menuBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        menuBtn.contentMode = .scaleAspectFit
        menuBtn.addTarget(self, action: #selector(showSideMenu), for: .touchUpInside)
        menuBtn.setImage(#imageLiteral(resourceName: "main_menu_w"), for: .normal)
        let menuItem = UIBarButtonItem(customView: menuBtn)
        navigationItem.leftBarButtonItems = [menuItem]
    }
    
    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
}
