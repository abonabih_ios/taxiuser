//
//  AboutRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit


class AboutRouter
{
    unowned let viewController: AboutViewController
    
    init(scene: AboutViewController) {
        self.viewController = scene
    }
}

