//
//  AuthViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/26/17.
//
//

import UIKit
import Cent
import PhoneNumberKit
import SwiftyUserDefaults





class  AuthViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var policyView: UIView!
    @IBOutlet weak var readPolicyLbl: UILabel!

    
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    
    @IBOutlet weak var firstActionImgView: UIImageView!
    @IBOutlet weak var firstActionLbl: UILabel!
    
    @IBOutlet weak var secondActionImgView: UIImageView!
    @IBOutlet weak var secondActionLbl: UILabel!
    
    
    @IBOutlet weak var carLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var carImgView: UIImageView!{
        didSet {
            self.carImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "intro_car") : #imageLiteral(resourceName: "intro_car").imageFlippedForRightToLeftLayoutDirection()
        }
    }
    
    
    
    //Cells
    var loginCell:LoginCell?
    var registrationCell:RegistrationCell?
    var activationCell:ActivationCell?
    
    
    enum AuthStep:Int {
        case none
        case login
        case register
        case activation
        
        init(step:Int) {
            if step <  AuthStep.none.rawValue {
                self = .none
            }
            else if step >  AuthStep.activation.rawValue  {
                self = .activation
            }
            else {
                self = AuthStep(rawValue:step)!
            }
        }
    }
    
    
    var currentStep:AuthStep = .none {didSet { setActionsTitlesAndImages() }}
    
    
    
    
    var modelView:AuthModelView?
    var router:AuthRouter?
    
    
    
    
    //MARK:- Lifecycle
    override func viewDidLoad(){
        super.viewDidLoad()
        modelView = AuthModelView(scene: self)
        router = AuthRouter(scene: self)
        
        let readPolicyString = NSMutableAttributedString(string: StringConstant.ReadPolicy)
        let range = NSString(string:StringConstant.ReadPolicy).range(of: StringConstant.UsagePolicy)
        readPolicyString.setAttributes([NSUnderlineStyleAttributeName: 1], range:range)
        readPolicyLbl.attributedText = readPolicyString
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if currentStep == .none {
            
            self.currentStep = LoggedUser.shared != nil && LoggedUser.shared?.userVerified == false ? .activation : .login
            
            moveCarToCenter()
            
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
                
            }){ completed in
                
                self.collectionView.scrollToItem(at: IndexPath(item:self.currentStep.rawValue, section:0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    
    
    func didLoginOrActivatedASucessfully(){
        moveCarToEnd()
        
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            
        }){ completed in
            self.router?.navigateToHomeScene()
        }
    }
    
    
    func showActivation(){
        currentStep = .activation
        router?.navigateToActivation()
    }
    
    
    func showLogin(){
        currentStep = .login
        router?.navigateToLogin()
    }
    
    
    private func moveCarToCenter(){
        carLeadingConstraint.constant = (view.bounds.width - carImgView.bounds.width)  / 2
    }
    
    
    private func moveCarToEnd(){
        carLeadingConstraint.constant = view.bounds.width
    }
    
    
    
    
    //MARK:- UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCell", for: indexPath)
            return cell
        }
        else if indexPath.item == 1 {
            loginCell = loginCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LoginCell.self), for: indexPath) as? LoginCell
            
            return loginCell!
        }
        else if indexPath.item == 2 {
            registrationCell = registrationCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RegistrationCell.self), for: indexPath) as? RegistrationCell
            return registrationCell!
        }
        else {
            activationCell = activationCell ?? collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ActivationCell.self), for: indexPath) as? ActivationCell
            
            activationCell?.delegate = self
            return activationCell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    
    
    
    //MARK:- Actions title & images
    func setActionsTitlesAndImages(){
        switch currentStep {
        case .login:
            forgotPasswordBtn.isHidden = false
            policyView.isHidden = true
            firstActionImgView.image = #imageLiteral(resourceName: "login_register")
            secondActionImgView.image =  UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "login_login") : #imageLiteral(resourceName: "login_login").imageFlippedForRightToLeftLayoutDirection()
            
            firstActionLbl.text = StringConstant.Register
            secondActionLbl.text = StringConstant.SignIn
        case .register:
            policyView.isHidden = false
            forgotPasswordBtn.isHidden = true
            firstActionImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "reg_back") : #imageLiteral(resourceName: "reg_back").imageFlippedForRightToLeftLayoutDirection()
            secondActionImgView.image =  #imageLiteral(resourceName: "reg_register")
            
            firstActionLbl.text = StringConstant.Back
            secondActionLbl.text = StringConstant.Register
        case .activation:
            policyView.isHidden = true
            forgotPasswordBtn.isHidden = true
            firstActionImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "ver_back") : #imageLiteral(resourceName: "ver_back").imageFlippedForRightToLeftLayoutDirection()
            secondActionImgView.image =  #imageLiteral(resourceName: "ver_confirm")
            
            firstActionLbl.text = StringConstant.Cancel
            secondActionLbl.text =  StringConstant.Confirm
        default:
            break
        }
    }
    
    //MARK:- Actions
    @IBAction func forgotPassword(_ sender: UIButton) {
        showTwoActionsAlert(title: StringConstant.CallUs, firstActionButtontitle: StringConstant.CallUs, secondActionButtontitle: StringConstant.Cancel, mesage: StringConstant.MsgForgotPassCallSupport) {
            UIApplication.shared.tryOpenUrl(urls: ["tel:\(StringConstant.SupportMobile)"])
        }
    }
    
    @IBAction func firstBtnDidTapped(_ sender: UIButton) {
        switch currentStep {
        case .login:
            currentStep  = .register
            router?.navigateToRegistration()
        case .register:
            currentStep  = .login
            router?.navigateToLogin()
        case .activation:
            showLoadingIndicator()
            modelView?.cancelVerification(data: ["user_id": LoggedUser.shared?.id ?? ""])
        default:
            break
        }
    }
    
    @IBAction func secondBtnDidTapped(_ sender: UIButton) {
        switch currentStep {
        case .login:
            loginCell?.validate()
            if loginCell?.valid == true {
                showLoadingIndicator()
                
                Defaults[.userMobileNumber] =  loginCell?.mobileTxtField.text ?? ""

                modelView?.login(data: ["mob": loginCell?.mobileTxtField.text ?? "",
                                        "pass":loginCell?.passwordTxtField.text ?? "",
                                        "device_id": UIDevice.uuidString,
                                        "sim_ser": "4444",
                                        "os_type": "2",
                                        "os_ver": UIDevice.osVersion])
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: loginCell?.validationMessage)
            }
        case .register:
            registrationCell?.validate()
            if registrationCell?.valid == true {
                showLoadingIndicator()
                
                var data = ["name":registrationCell?.nameTxtField.text ?? ""]
                data["mob"] =  registrationCell?.mobileTxtField.text ?? ""
                data["pass"] = registrationCell?.passwordTxtField.text ?? ""
                data["gender"] =  registrationCell?.gender ?? "1"
                data["email"] =  registrationCell?.emailTxtField.text ?? ""
                data["country_code"] =  AppDelegate.CountryCode
                data["device_id"] =  UIDevice.uuidString
                data["sim_ser"] = "4444"
                data["os_type"] =  "2"
                data["os_ver"] =  UIDevice.osVersion
                Defaults[.userMobileNumber] = registrationCell?.mobileTxtField.text ?? ""
                modelView?.register(data:data)
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: registrationCell?.validationMessage)
            }
        case .activation:
            activationCell?.validate()
            if activationCell?.valid == true {
                showLoadingIndicator()
                modelView?.activate(data:["user_id": LoggedUser.shared?.id ?? "",
                                          "ver_code": activationCell?.verificationCodeTxtField.text ?? ""])
            }
            else {
                showDefaultAlert(title: StringConstant.Info, message: activationCell?.validationMessage)
            }
        default:
            break
        }
    }
    
    
    @IBAction func showPolicy(_ sender: UIButton) {
       // router?.showPolicyVC()
    }
    
}
