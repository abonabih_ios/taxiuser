//
//  PolicyViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 5/7/17.
//
//

import UIKit
import PopupController

class ReadPolicyViewController: UIViewController,PopupContentViewController
{
    @IBOutlet weak var webView: UIWebView!
    
    
    var completionHandler: ((Void) -> Void)?

    
    
    override func viewDidLoad() {
        
        let currentLanguage = (UserDefaults.standard.array(forKey: "AppleLanguages") as? [String])?.first() ?? "en"

        let fileName = "legal_\(currentLanguage)"
        
        let url = Bundle.main.url(forResource: fileName, withExtension: "html")
        let myRequest = URLRequest(url: url!)
        webView.loadRequest(myRequest)

    }
    
    
    @IBAction func dismiss(_ sender: UIButton) {
        completionHandler?()
    }
    
    
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height)
    }
}
