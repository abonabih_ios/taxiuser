//
//  AuthRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import UIKit
import PopupController


class AuthRouter
{
    unowned let viewController: AuthViewController
    
    var policyPopupVC:PopupController?

    
    
    init(scene: AuthViewController) {
        self.viewController = scene
    }
    
    func navigateToLogin() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.login.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }
    
    func navigateToRegistration() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.register.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }

    func navigateToActivation() {
        viewController.collectionView.scrollToItem(at: IndexPath(item:AuthViewController.AuthStep.activation.rawValue, section:0), at: .centeredHorizontally, animated: true)
    }

    func navigateToHomeScene() {
        
        let sideMenuRootVC = SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main") as! SideMenuRootViewController
        
        let direction = UIApplication.shared.userInterfaceLayoutDirection
        
        sideMenuRootVC.menuDirection = direction == .leftToRight ? .left : .right

        self.viewController.present(sideMenuRootVC, animated: true, completion: nil)
    }
    
    
    func showPolicyVC(){
            let policyVC = ReadPolicyViewController.instanceFromStoryboard(storyboardName: "Authorization_Storyboard") as? ReadPolicyViewController
            
            policyVC?.completionHandler = {[weak self] in
                self?.policyPopupVC?.dismiss()
            }
        
            policyPopupVC = PopupController
                .create(viewController)
                .customize(
                    [
                        .animation(.fadeIn),
                        .layout(.center),
                        .backgroundStyle(.blackFilter(alpha: 0.4))
                    ]
                )
                .show(policyVC!)
    }
}
