//
//  ActivationCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit


protocol ActivationCellDelegate:class {
    func resendCode(code:String)
}


class ActivationCell: UICollectionViewCell
{
    
    @IBOutlet weak var verificationCodeTxtField: UITextField!
    
    @IBOutlet weak var carImgView: UIImageView!{
        didSet {
            self.carImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "intro_car") : #imageLiteral(resourceName: "intro_car").imageFlippedForRightToLeftLayoutDirection()
        }
    }

    weak var delegate:ActivationCellDelegate?
    
    
    var valid:Bool = false
    var validationMessage:String = ""

    
    
    @IBAction func resendCode(_ sender: Any) {
        delegate?.resendCode(code: verificationCodeTxtField.text ?? "")
    }
    
    
    
    //MARK:- Validate
    func validate() {
        
        let length = verificationCodeTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).length ?? 0
        
        if length == 0 {
            valid = false
            validationMessage = StringConstant.MsgEnterVerCode
        }
        else {
            valid = true
            validationMessage = ""
        }
    }
    
}
