//
//  LoginCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit
import PhoneNumberKit



class LoginCell: UICollectionViewCell
{
    @IBOutlet weak var mobileTxtField: UITextField!
    
    @IBOutlet weak var passwordTxtField: UITextField!
    
    @IBOutlet weak var passwordImgView: UIImageView!

    
    var valid:Bool = false
    var validationMessage:String = ""
    
    
    
    //MARK:- Validate
    func validate() {
        
        
        do {
            let phoneNumberKit = PhoneNumberKit()
            
            let mobile = mobileTxtField.text ?? ""
            
            _ = try phoneNumberKit.parse("\(AppDelegate.CountryCode)\(mobile)")
            
            if passwordTxtField.text?.validateWith(pattern: "[0-9a-zA-Z]{6,}") != true {
                valid = false
                validationMessage = StringConstant.MsgValidatePassLen
            }
            else {
                valid = true
                validationMessage = ""
            }
        }
        catch {
            valid = false
            validationMessage = StringConstant.MsgEnterMobNum
        }

        
    }

    
    
    
    //MARK:- Actions
    @IBAction func showHidePassword(_ sender: UIButton) {
        let count = passwordTxtField.text?.characters.count ?? 0
        
        if count > 0 {
            passwordTxtField.isSecureTextEntry = !passwordTxtField.isSecureTextEntry
            
           passwordImgView.image =  passwordTxtField.isSecureTextEntry ? #imageLiteral(resourceName: "login_show_password") : #imageLiteral(resourceName: "login_hide_password")
        }
    }
}
