//
//  RegistrationCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import UIKit
import PhoneNumberKit




class RegistrationCell: UICollectionViewCell
{
    @IBOutlet weak var maleImgView: UIImageView!
    @IBOutlet weak var femaleImgView: UIImageView!
    @IBOutlet weak var passwordImgView: UIImageView!

    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!

    @IBOutlet weak var carImgView: UIImageView! {
        didSet {
            self.carImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "intro_car") : #imageLiteral(resourceName: "intro_car").imageFlippedForRightToLeftLayoutDirection()
        }
    }

    
    

    
    var valid:Bool = false
    var validationMessage:String = ""
    
    var gender:String = "1"

    
    
    @IBAction func selectGender(_ sender: UIButton) {
        gender = "\(sender.tag)"
        
        if sender.tag == 1 {
            maleImgView.backgroundColor = CustomStyle.themeColor
            femaleImgView.backgroundColor = .clear
        }
        else {
            maleImgView.backgroundColor = .clear
            femaleImgView.backgroundColor = CustomStyle.themeColor
        }
    }
    
    
    @IBAction func showHidePassword(_ sender: UIButton) {
        let count = passwordTxtField.text?.characters.count ?? 0
        
        if count > 0 {
            passwordTxtField.isSecureTextEntry = !passwordTxtField.isSecureTextEntry
            
            passwordImgView.image =  passwordTxtField.isSecureTextEntry ? #imageLiteral(resourceName: "login_show_password") : #imageLiteral(resourceName: "login_hide_password")
        }
    }
    
    
    
    //MARK:- Validate
    func validate() {
        
        if (nameTxtField.text?.length ?? 0) < 1 {
            valid = false
            validationMessage = StringConstant.MsgEnterValidFullName
        }
        else {
            do {
                let phoneNumberKit = PhoneNumberKit()
                
                let mobile = mobileTxtField.text ?? ""
                
                _ = try phoneNumberKit.parse("\(AppDelegate.CountryCode)\(mobile)")
                
                let emailLength = emailTxtField.text?.length ?? 0
                
                if emailTxtField.text?.validateWith(pattern: "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$") != true && emailLength > 0 {
                    valid = false
                    validationMessage = StringConstant.MsgEnterValidEmail
                }
                else if passwordTxtField.text?.validateWith(pattern: "[0-9a-zA-Z]{6,}") != true {
                    valid = false
                    validationMessage = StringConstant.MsgValidatePassLen
                }
                else {
                    valid = true
                    validationMessage = ""
                }
            }
            catch {
                valid = false
                validationMessage = StringConstant.MsgEnterMobNum
            }
        }
        
    }

}
