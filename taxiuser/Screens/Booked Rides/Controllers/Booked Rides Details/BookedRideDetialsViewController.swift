//
//  BookedRideDetialsViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import Cent
import SwiftDate
import CoreLocation
import PopupController


class BookedRideDetialsViewController:UITableViewController
{
    
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var pickupLocLbl: UILabel!
    @IBOutlet weak var dropOffLbl: UILabel!
    @IBOutlet weak var carLbl: UILabel!
    @IBOutlet weak var notesLbl: UILabel!
    @IBOutlet weak var followupLbl: UILabel!

    @IBOutlet weak var tripImgView: UIImageView!

    @IBOutlet weak var tripCarTypeImgView: UIImageView!

    
    
    var modelView: BookedRideDetailsModelView?
    var router: BookedRideDetailsRouter?
    
    var ride:BookedRide?
    
    private var popupVC:PopupController?

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItems()
        router = BookedRideDetailsRouter(scene: self)
        modelView = BookedRideDetailsModelView(scene: self)
        typeLbl.text = ride?.rideTtype == .basic ?  StringConstant.BasicRide : StringConstant.KidsRide
        
        if ride?.dateTime.contains("0000-00-00 00:00:00") == false {
            let date =  Date.parse(dateStr: ride?.dateTime ?? "", format: StringConstant.DateFormate)
            
            dateLbl.text = "\(date.year.format(f: "04"))-\(date.month.format(f: "02"))-\(date.day.format(f: "02"))"
            timeLbl.text =  date.string(format: .strict("hh:mm a"))
        }
        
        
        pickupLocLbl.text = ride?.addressPick
        dropOffLbl.text = (ride?.addresDrop.isEmpty == true) ? StringConstant.MsgWillTellTheDriver : ride?.addresDrop
        carLbl.text = ride?.carType == .economy ? StringConstant.Economy : StringConstant.Business
        
        notesLbl.text = ride?.notes
        
        followupLbl.text = ride?.followUpNumbers.map{$0["tel"]!}.joined(separator: " - ")
        
        followupLbl.superview?.isHidden = ride?.rideTtype != .kids
        
        tripCarTypeImgView.image = ride?.rideTtype == .basic ? #imageLiteral(resourceName: "bok_list_basic") : #imageLiteral(resourceName: "bok_list_kids")

        if ride?.rideTtype == .basic {
            tripImgView.image = ride?.gender == .male ? #imageLiteral(resourceName: "bok_trip_profiles_man") : #imageLiteral(resourceName: "bok_trip_profiles_woman")
            
        }
        else {
            tripImgView.image = ride?.gender == .boy ? #imageLiteral(resourceName: "bok_trip_profiles_boy") : #imageLiteral(resourceName: "bok_trip_profiles_girl")
        }
    }

    
    private func setUpNavigationItems() {
        self.title = StringConstant.RideDetails
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        backBtn.contentMode = .center
       backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()
        
        backBtn.setImage(img, for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
        
        
        let cancelBtn = UIButton(frame: CGRect(x:0,y:0,width:80,height:30))
        cancelBtn.setTitle(StringConstant.Cancel, for: .normal)
        cancelBtn.contentMode = .center
        cancelBtn.addTarget(self, action: #selector(cancel), for: UIControlEvents.touchUpInside)
        let cancelItem = UIBarButtonItem(customView: cancelBtn)
        navigationItem.rightBarButtonItems = [cancelItem]
    }


    @IBAction func back(_ sender: UIButton){
        router?.backToRidesList()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        showLoadingIndicator()
        modelView?.cancelOrder()
    }
    
    @IBAction func showPickupLocation(_ sender: UIButton) {
        guard let lat = ride?.pickupLat, let lng = ride?.pickupLng  else { return }
        
        MapsUtils.openAppleMapsWithLocation(location: CLLocationCoordinate2D(latitude: lat,longitude: lng), placeName: ride?.addressPick)
    }

    
    @IBAction func showDropOffLocation(_ sender: UIButton) {
        guard let lat = ride?.destinationLat, let lng = ride?.destinationLng  else { return }
        
        MapsUtils.openAppleMapsWithLocation(location: CLLocationCoordinate2D(latitude: lat,longitude: lng), placeName: ride?.addresDrop)
    }
    
    @IBAction func showNotes(_ sender: UIButton) {
            let  notesController = NotesViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? NotesViewController
    
        notesController?.notes = ride?.notes
        
        notesController?.isEditable = false
        notesController?.completionHandler = {[weak self] in
                self?.popupVC?.dismiss()
            }
            
            
            popupVC = PopupController
                .create(self)
                .customize(
                    [
                        .animation(.fadeIn),
                        
                        .layout(.center),
                        .backgroundStyle(.blackFilter(alpha: 0.4))
                    ]
                )
                .show(notesController!)
    }

    
    @IBAction func showFollowupNumbers(_ sender: UIButton) {
        let  followupController = FollowupNumbersViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? FollowupNumbersViewController
        followupController?.editable = false
        followupController?.followupNumbers = ride?.followUpNumbers.map{ "\($0["name"]!)@\($0["tel"]!)"}
        navigationController?.pushViewController(followupController!, animated: true)
    }
    
    
    
    
}
