//
//  BookedRideDetailsModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import Realm



class BookedRideDetailsModelView: BaseModelView
{
    unowned let scene: BookedRideDetialsViewController
    
    
    init(scene: BookedRideDetialsViewController) {
        self.scene = scene
    }

    
    //Fetch list of booked rides
    func cancelOrder(){
        let rideId = scene.ride?.rideId ?? ""
        let userId = LoggedUser.shared?.id ?? ""
        let params:[String : Any] = ["user_id":userId, "ride_id":"\(rideId)"]
        
        let service = CancelBookedRideService()
        executeService(service: service, userData: params,headers: [:], tag: CancelBookedRideService.className)
    }
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case CancelBookedRideService.notificationName:
            scene.hideLoadingIndicator()
            scene.showDefaultAlert(title: StringConstant.Info, message: StringConstant.RideCancelledScucessfully) {[weak self] in
                self?.scene.router?.backToRidesList()
            }
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}
