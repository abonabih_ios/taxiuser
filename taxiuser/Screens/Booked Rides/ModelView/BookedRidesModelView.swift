//
//  BookedRidesModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift


class BookedRidesModelView: BaseModelView
{
    unowned let scene:BookedRidesViewController
    
    
    init(scene: BookedRidesViewController) {
        self.scene = scene
    }

    
    //Fetch list of booked rides
    func fetchBookedRidesList(){
        let userId = Realm.userRealm.currentUser?.id ?? ""
        let params:[String : Any] = ["user_id":userId]
        
        let service = BookedService()
        executeService(service: service, userData: params,headers: [:], tag: BookedService.className)
    }
    
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case BookedService.notificationName:
            
            if let response =  notification.userInfo?[BaseService.DataReceivedKey] as? [BookedRide] {
                
                scene.bookKedRides = response
                print(response)
                
            }
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}
