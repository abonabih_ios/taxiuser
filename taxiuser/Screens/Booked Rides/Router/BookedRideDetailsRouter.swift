//
// BookedRideDetailsParentRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit


class BookedRideDetailsRouter
{
    unowned let viewController: BookedRideDetialsViewController
    
    init(scene: BookedRideDetialsViewController) {
        self.viewController = scene
    }
    
    func backToRidesList() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
}
