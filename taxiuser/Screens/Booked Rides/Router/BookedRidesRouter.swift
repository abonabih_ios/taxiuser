//
//  BookedRidesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit


class BookedRidesRouter
{
    unowned let viewController: BookedRidesViewController
    
    init(scene: BookedRidesViewController) {
        self.viewController = scene
    }
    
    func openRideDetails(ride:BookedRide) {
        let bookedRidesVC = BookedRideDetialsViewController.instanceFromStoryboard(storyboardName: "BookedRides_Storyboard") as? BookedRideDetialsViewController
        
        bookedRidesVC?.ride = ride
        viewController.navigationController?.pushViewController(bookedRidesVC!, animated: true)
    }
}
