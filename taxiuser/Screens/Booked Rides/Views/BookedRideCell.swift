//
//  BookedRideCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/9/16.
//
//

import UIKit

class BookedRideCell: UITableViewCell
{
    
    @IBOutlet weak var tripTypeImgView: UIImageView!
    @IBOutlet weak var tripTypeLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var pickupLocLbl: UILabel!
    @IBOutlet weak var dropOffLocLbl: UILabel!
    @IBOutlet weak var tripImgView: RoundedCornerImageView!
}
