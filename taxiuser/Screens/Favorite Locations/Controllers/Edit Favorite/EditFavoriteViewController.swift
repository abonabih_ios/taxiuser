//
//  EditFavoriteViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import PopupController



protocol EditFavoriteViewControllerDelegate:class {
    func didEndEditFavoritePlace(place:Place)
    func pickFavoritePlace()
}


class EditFavoriteViewController: UIViewController,PopupContentViewController
{
    
    var place:Place?
    
    
    @IBOutlet weak var placeNameTxtField: UITextField!
    
    @IBOutlet weak var addressLbl: UILabel!

    weak var delegate:EditFavoriteViewControllerDelegate?
    
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        placeNameTxtField.text = place?.name
        addressLbl.text = place?.address
        

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // self.view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
    }
    
    override var prefersStatusBarHidden: Bool {return true}
    
    //MARK:- Actions
    @IBAction func pickPlace(_ sender: UIButton) {
       // self.dismiss(animated: false)
        delegate?.pickFavoritePlace()
    }
    
    @IBAction func savePickedPlace(_ sender: UIButton) {
        guard let place = self.place else {
            showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgPickLoc)
            return
        }
        
        place.name = placeNameTxtField.text ?? place.name 
        delegate?.didEndEditFavoritePlace(place: place)
    }
    
    
    @IBAction func dismissViewTapGesture(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 230)
    }
}
