//
//  FavoritePlacesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit

class FavoritePlacesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView:UITableView!
    
    
    
    var favoritePlaces:[Place]?{ didSet{tableView.reloadData()} }
    
    var model:FavoriteModelView?
    
    var router:FavoriteRouter?
    
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        model = FavoriteModelView(scene: self)
        router = FavoriteRouter(scene: self)
        setUpNavigationItems()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        model?.loadFavoritePlaces()
        tableView.reloadData()
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.FavoriteLocations
        let menuBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        menuBtn.contentMode = .center
        menuBtn.addTarget(self, action: #selector(showSideMenu), for: .touchUpInside)
        menuBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let menuItem = UIBarButtonItem(customView: menuBtn)
        navigationItem.leftBarButtonItems = [menuItem]
        
        
        let addPlaceBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        addPlaceBtn.contentMode = .scaleAspectFill
        addPlaceBtn.addTarget(self, action: #selector(addNewPlace), for: .touchUpInside)
        addPlaceBtn.setImage(#imageLiteral(resourceName: "favorites_add"), for: .normal)
        let addPlaceMenuItem = UIBarButtonItem(customView: addPlaceBtn)
        navigationItem.rightBarButtonItems = [addPlaceMenuItem]

    }
    
    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    func addNewPlace(){
        router?.showEditFavorite(place: nil)
    }
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = favoritePlaces?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritePlaceCell") as? FavoritePlaceCell
        
        let place = favoritePlaces?[indexPath.row]
        
        cell?.placeNameLbl.text = place?.name
        cell?.addressLbl.text = place?.address
        
        
        let delteAction = ABUITableViewCellRowAction(title: StringConstant.Delete) {
            self.model?.removePlace(place: place!)
            self.favoritePlaces?.remove(value: place!)
            self.tableView.reloadData()
        }
        
        delteAction.backgroundColor  = .red


        cell?.rightActions = [delteAction]
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
