//
//  FavoriteRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import PopupController





class FavoriteRouter
{
    unowned let viewController: FavoritePlacesViewController
    
    //var editFavoritePopupVC:PopupController?
    var editFavoritePopupVC:EditFavoriteViewController?

    init(scene: FavoritePlacesViewController) {
        self.viewController = scene
    }
    
    
    
    //MARK:- FavoriteRouterInput
    func showPlacePicker() {
        let  placePickerViewController = PlacePickerViewController.instanceFromStoryboard(storyboardName: "PlacePicker_Storyboard") as? PlacePickerViewController
        
        placePickerViewController?.tabs = [.map, .nearby, .recent]
        
        placePickerViewController?.didSelectPlaceHandler = { placeData in
            
            let place = Place(value:placeData)
            
            self.showEditFavorite(place: place)
        }
        
        self.viewController.navigationController?.pushViewController(placePickerViewController!, animated: true)
    }
    
    func showEditFavorite(place:Place? = nil){
        editFavoritePopupVC = EditFavoriteViewController.instanceFromStoryboard(storyboardName: "Favorites_Storyboard") as? EditFavoriteViewController
        
        editFavoritePopupVC?.place = place
       editFavoritePopupVC?.delegate = viewController
        
        
        self.viewController.present(editFavoritePopupVC!, animated: true, completion: nil)
        
//        editFavoritePopupVC = PopupController
//            .create(viewController)
//            .customize(
//                [
//                    .animation(.fadeIn),
//                    .layout(.center),
//                    .backgroundStyle(.blackFilter(alpha: 0.4))
//                ]
//            )
//            .show(editFavoriteVC!)
    }
    
    func dismissEditPopupVC(){
        editFavoritePopupVC?.dismiss(animated: false)
    }
    
    
    func didEndEditFavorite(){
        editFavoritePopupVC?.dismiss(animated: false)
    }

}
