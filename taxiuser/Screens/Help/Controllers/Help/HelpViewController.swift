//
//  HelpViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

protocol HelpViewControllerOuput {
}


class HelpViewController:UITableViewController, HelpViewControllerOuput
{
    var modelView: HelpModelView?
    var router: HelpRouter?
    
    @IBOutlet weak var balanceLbl: UILabel!

    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = HelpRouter(scene: self)
        modelView = HelpModelView(scene: self)

        setUpNavigationItems()
        
        
        balanceLbl.text = "\(LoggedUser.shared?.balance ?? 0) \(StringConstant.Qar)"
        
        modelView?.getUserBalance()
    }
    
    private func setUpNavigationItems() {
        self.title = StringConstant.Help
        let menuBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        menuBtn.contentMode = .scaleAspectFit
        menuBtn.addTarget(self, action: #selector(showSideMenu), for: .touchUpInside)
        menuBtn.setImage(#imageLiteral(resourceName: "main_menu_w"), for: .normal)
        let menuItem = UIBarButtonItem(customView: menuBtn)
        navigationItem.leftBarButtonItems = [menuItem]
    }

    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    
    //MARK:- 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            router?.navigateLegalNotes()
        }
    }
}
