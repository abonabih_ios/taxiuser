//
//  LegalNotesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import UIKit

class LegalNotesViewController: UIViewController
{
    
    @IBOutlet weak var webView: UIWebView!

    var router: LegalNotesRouter?

    
    
    
    
    
    

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let currentLanguage = (UserDefaults.standard.array(forKey: "AppleLanguages") as? [String])?.first() ?? "en"
        
        let fileName = "legal_\(currentLanguage)"
        
        let url = Bundle.main.url(forResource: fileName, withExtension: "html")
        let myRequest = URLRequest(url: url!)
        webView.loadRequest(myRequest)

        
        setUpNavigationItems()
        
        router = LegalNotesRouter(scene: self)

    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.TitleLegalNotes
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)

        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()
        backBtn.setImage(img, for: .normal)

        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        router?.back()
    }
    
    
    
}
