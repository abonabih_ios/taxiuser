//
//  RatesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import DropDown


class RatesViewController:UITableViewController
{

    @IBOutlet weak var selctedTypeItemView: RateMenuCell!
    @IBOutlet weak var selctedCostTypeItemView: RateMenuCell!

    
    @IBOutlet weak var tripTypeDropDownAnchor: UIView!
    @IBOutlet weak var tripCostDropDownAnchor: UIView!
    
    
    @IBOutlet weak var startingLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var penalityLbl: UILabel!

    @IBOutlet weak var laterStartingLbl: UILabel!
    @IBOutlet weak var laterMinLbl: UILabel!
    @IBOutlet weak var laterPenalityLbl: UILabel!
    
    @IBOutlet weak var movingLbl: UILabel!

    @IBOutlet weak var currencyUnitLbl: UILabel!
    @IBOutlet weak var startingNoeCurrencyUnitLbl: UILabel!
    @IBOutlet weak var waitingUnitLabel: UILabel!

    
    
    

    let tripTypeDropDown = DropDown()
    let tripCostDropDown = DropDown()

    
    var modelView: RatesModelView?
    var router: RatesRouter?

    var basicEconomyRate:Rate?
    var basicBusinessRate:Rate?
    var kidsEconomyRate:Rate?
    var kidsBusinessRate:Rate?

    
    private var tripType:BookingType = .basic
    private var carType:CarType = .economy

    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = RatesRouter(scene: self)
        setUpNavigationItems()

        
        selctedTypeItemView.imgView.image = UIImage(named:"rates_business")
        selctedTypeItemView.optionLabel.text = StringConstant.BasicRide

        selctedCostTypeItemView.imgView.image = UIImage(named:"rates_business")
        selctedCostTypeItemView.optionLabel.text = StringConstant.Economy

        modelView = RatesModelView(scene: self)
        
        updateUI()
        modelView?.getRates()
    }
    
    func updateUI() {
    
        var rate:Rate?
        
        printLog(tag: .info, data: "\(tripType),\(carType)")

        switch (tripType, carType) {

        case (.basic, .economy):
            rate = basicEconomyRate
        case (.basic, .busniess):
            rate = basicBusinessRate
        case (.kids, .economy):
            rate = kidsEconomyRate
        case (.kids, .busniess):
            rate = kidsBusinessRate
        default:
            break
        }
        
        currencyUnitLbl.text = StringConstant.Qar
        startingNoeCurrencyUnitLbl.text = StringConstant.Qar
       startingLbl.text = rate?.nowStart
       minLbl.text = rate?.nowMin
       penalityLbl.text = rate?.nowFee
        
       laterStartingLbl.text = rate?.laterStart
       laterMinLbl.text = rate?.laterMin
       laterPenalityLbl.text = rate?.laterFee
        
       movingLbl.text = "\(rate?.moving ?? "") \(StringConstant.Qar)"
        waitingUnitLabel.text = "\(rate?.waiting ?? "") \(StringConstant.Qar)"

    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.Rates
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()
        backBtn.setImage(img, for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        tripTypeDropDown.hide()
        tripCostDropDown.hide()
        router?.back()
        
    }
    
    // MARK: - Actions
    @IBAction func selectRideType(_ sender: UIButton) {
        tripCostDropDown.hide()
        
        
        tripTypeDropDown.dataSource = [
            StringConstant.BasicRide,
            StringConstant.KidsRide ]
        
        tripTypeDropDown.cellNib = UINib(nibName: String(describing: RateMenuCell.self), bundle: nil)
        
        tripTypeDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? RateMenuCell else { return }
            
            // Setup your custom UI components
            cell.imgView.image = [UIImage(named:"rates_business"),UIImage(named:"rates_kids")][index]
            cell.optionLabel.text = item
        }
        
        
        tripTypeDropDown.selectionAction = { [unowned self] (index, item) in
            self.selctedTypeItemView.optionLabel.text = item
            self.selctedTypeItemView.imgView.image = [UIImage(named:"rates_business"),UIImage(named:"rates_kids")][index]
            
            self.tripType = index == 0 ? .basic : .kids
            self.updateUI()

        }
        
        tripTypeDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)

        tripTypeDropDown.anchorView = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? tripTypeDropDownAnchor :  tripCostDropDownAnchor
        
        tripTypeDropDown.bottomOffset = CGPoint(x: 0, y: tripTypeDropDown.anchorView?.plainView.bounds.height ?? sender.bounds.height)

        tripTypeDropDown.show()
        
    }
    
    @IBAction func selectLevel(_ sender: UIButton) {
        tripTypeDropDown.hide()
    
        tripCostDropDown.dataSource = [
            StringConstant.Economy,
            StringConstant.Business ]
        
        tripCostDropDown.cellNib = UINib(nibName: String(describing: RateMenuCell.self), bundle: nil)
        
        tripCostDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? RateMenuCell else { return }
            
            // Setup your custom UI components
            cell.imgView.image = [UIImage(named:"rates_econmy"),UIImage(named:"rates_business")][index]
            cell.optionLabel.text = item
        }

        

        
        tripCostDropDown.selectionAction = { [unowned self] (index, item) in
            self.selctedCostTypeItemView.imgView.image = [UIImage(named:"rates_econmy"), UIImage(named:"rates_business")][index]
            self.selctedCostTypeItemView.optionLabel.text = item
            
            self.carType = index == 0 ? .economy : .busniess
            self.updateUI()
        }
        
        tripCostDropDown.anchorView = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? tripCostDropDownAnchor : tripTypeDropDownAnchor
        
        
        tripCostDropDown.bottomOffset = CGPoint(x: 0, y: tripCostDropDown.anchorView?.plainView.bounds.height ?? sender.bounds.height)

        tripCostDropDown.show()

    }
    
}
