//
//  SettingsModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift


class HelpModelView: BaseModelView
{
    unowned let scene: HelpViewController

    
    init(scene: HelpViewController) {
        self.scene = scene
    }

    
    
    //MARK:- User Balance
    func getUserBalance(){
        scene.showLoadingIndicator()
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let service = GetUserBalanceService()
        executeService(service: service, userData: ["user_id":userId],headers:[:], tag: GetUserBalanceService.className)

    }
    
    
    //MARK:-
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case GetUserBalanceService.notificationName:
            scene.balanceLbl.text = "\(LoggedUser.shared?.balance ?? 0) \(StringConstant.Qar)"
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }

}
