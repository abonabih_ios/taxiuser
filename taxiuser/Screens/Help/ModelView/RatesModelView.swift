//
//  RatesModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift



class RatesModelView: BaseModelView
{
    
    unowned let scene: RatesViewController
    
    
    init(scene: RatesViewController) {
        self.scene = scene
    }

    
    func getRates(){
        
        scene.showLoadingIndicator()
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let service = RatesService()
        executeService(service: service, userData: ["user_id":userId],headers:[:], tag: RatesService.className)
    }
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case RatesService.notificationName:
            if let response = notification.userInfo?[BaseService.DataReceivedKey] as? RatesResponse  {
                scene.basicBusinessRate = response.basicBusinessRates.first()
                scene.basicEconomyRate = response.basicEconomyRates.first()
                scene.kidsEconomyRate = response.kidsEconomyRates.first()
                scene.kidsBusinessRate = response.kidsBusinessRates.first()
                scene.tableView.isHidden = false
                
                scene.updateUI()
            }
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}
