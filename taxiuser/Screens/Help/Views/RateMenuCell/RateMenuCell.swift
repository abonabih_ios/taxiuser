//
//  RateMenuCell.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/13/17.
//
//

import UIKit
import DropDown

class RateMenuCell: DropDownCell
{
    
    @IBOutlet weak var imgView: UIImageView!
    
     @IBOutlet weak override var optionLabel: UILabel! {
        didSet {
            optionLabel.textAlignment = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? .left : .right
        }
    }
    
    
}
