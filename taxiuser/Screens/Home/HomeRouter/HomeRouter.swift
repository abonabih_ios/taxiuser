//
//  HomeRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/18/16.
//
//

import UIKit


class HomeRouter
{
    unowned let viewController: HomeViewController
    
    
    init(scene: HomeViewController) {
        self.viewController = scene
    }

    func navigateToBasicTrip() {
        let tripVc = BookingTripViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? BookingTripViewController

        tripVc?.bookingType = .basic
        viewController.navigationController?.pushViewController(tripVc!, animated: true)
    }
    
    func navigateToKidsTrip() {
        let tripVc = BookingTripViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")  as? BookingTripViewController
        
        tripVc?.bookingType = .kids
        
        viewController.navigationController?.pushViewController(tripVc!, animated: true)
    }
}
