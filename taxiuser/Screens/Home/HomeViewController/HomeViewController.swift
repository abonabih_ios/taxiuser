//
//  HomeViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/18/16.
//
//

import UIKit
import SwiftyUserDefaults

protocol HomeViewControllerOuput {
    func startBasicTrip(_ sender: UIButton)
    func startKidsTrip(_ sender: UIButton)
}

class HomeViewController: UIViewController, HomeViewControllerOuput
{
    var router:HomeRouter?
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.systemFont(ofSize: 17.0)], for: .normal)
        //:@{NSForegroundColorAttributeName:UIColor.whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
        
        
        
        router = HomeRouter(scene: self)
        setUpNavigationItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Defaults[.followupNumbers] = []
        
        UIView.animate(withDuration: 0.5) { 
            self.navigationController?.navigationBar.isHidden = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.5) {
            self.navigationController?.navigationBar.isHidden = false
        }
        
    }
    
    private func setUpNavigationItems() {
        self.title = StringConstant.HomeTitle
        let menuBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        menuBtn.addTarget(self, action: #selector(showSideMenu), for: UIControlEvents.touchUpInside)
        menuBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let menuItem = UIBarButtonItem(customView: menuBtn)
        navigationItem.leftBarButtonItems = [menuItem]
    }
    
    
    //MARK:- Actions
    @IBAction func startBasicTrip(_ sender: UIButton) {
        router?.navigateToBasicTrip()
    }
    
    @IBAction func startKidsTrip(_ sender: UIButton) {
        router?.navigateToKidsTrip()
    }
    
    @IBAction func showSideMenu(_ sender: UIButton){
        findHamburguerViewController()?.showMenuViewController()
    }

}
