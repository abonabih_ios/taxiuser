//
//  AppModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/27/17.
//
//

import UIKit

class AppModelView: BaseModelView
{
    weak var window: UIWindow?
    
    //================================================================================================================

    init(window: UIWindow?)
    {
        self.window = window
    }
    
    //================================================================================================================

    //Fetch list of booked rides
    func requestAppEntryDate()
    {
        let userId = LoggedUser.shared?.id ?? ""
        let params:[String : Any] = ["user_id":userId, "device_manufact":"Apple", "device_brand":"iPhone", "device_model":"iPhone 7"]

        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.showLoadingIndicator()

        let service = AppEntryService()
        executeService(service: service, userData: params,headers: [:], tag: AppEntryService.className)
    }
    
    //================================================================================================================

    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification)
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.hideLoadingIndicator()
        
        switch notification.name
        {
            case AppEntryService.notificationName:
                appDelegate?.hideLoadingIndicator()
                if let response = notification.userInfo?[BaseService.DataReceivedKey] as? AppEntryResponse {
                    appDelegate?.router?.navigateAccordingToAppEntry(appEntryData: response)
                }
            default:
                break
        }
        super.dataDidReceived(notification: notification)
    }
    
    //================================================================================================================

    override func errorDidReceived(notification: NSNotification)
    {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate

        appDelegate?.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError

        appDelegate?.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}
