//
//  AppRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/29/17.
//
//

import UIKit
import RealmSwift



class AppRouter
{
    weak var mainWindow: UIWindow?
    
    init(window: UIWindow?) {
        self.mainWindow = window
    }
    
    func navigateAccordingToAppEntry(appEntryData:AppEntryResponse) {
        
        if appEntryData.disabled == 1 {
            showBlockDialog()
        }
        else {
            checkIfNeedsUpdateAndnavigate(appEntryData: appEntryData)
        }
    }
    
    private func checkIfNeedsUpdateAndnavigate(appEntryData:AppEntryResponse){
        switch appEntryData.update {
        case 1:
            showForceUpdateDialog(updateLink: appEntryData.updateUrl)
        case 2:
            showOptionalUpdateDilaog(updateLink: appEntryData.updateUrl)
        default:
            navigateToNextScreen()
        }
    }
    
    private func  navigateToNextScreen(){
        if !userDidLoggedInAndVerified()  {
            navigateToAuthScreen()
        } else if isThereCurrentTrip() {
            navigateToCurrentTripScreen()
        }
        else {
            navigateToHomeScreen()
        }
    }
    
    
    private func userDidLoggedInAndVerified()->Bool {
        let isUserLoggedIn = LoggedUser.shared != nil
        
        let userVerified =  LoggedUser.shared?.userVerified ?? false
        
        if AppConfigurations.appMode == .debug {
            print("LOGGED IN: \(isUserLoggedIn)")
            print("VERIFIED: \(userVerified)")
        }
        
        return isUserLoggedIn &&  userVerified
    }
    
    private func navigateToAuthScreen(){
        mainWindow?.rootViewController = AuthViewController.instanceFromStoryboard(storyboardName: "Authorization_Storyboard")
    }
    
    private func navigateToHomeScreen(){
        let sideMenuVC = SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main") as? SideMenuRootViewController
        
        let direction = UIApplication.shared.userInterfaceLayoutDirection
        
        sideMenuVC?.menuDirection = direction == .leftToRight ? .left : .right
        
        mainWindow?.rootViewController = sideMenuVC
    }
    
    private func isThereCurrentTrip()->Bool {
        let noCurrentRides = Realm.tripRealm.currentOrder?.rideId?.isEmpty ?? true
        
        return !noCurrentRides
    }
    
    private func navigateToCurrentTripScreen(){
        let  tripViewController = TripViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")
        
        let sideMenuVC = SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main") as? SideMenuRootViewController
        
        let direction = UIApplication.shared.userInterfaceLayoutDirection
        
        sideMenuVC?.menuDirection = direction == .leftToRight ? .left : .right
        
        let navVC = sideMenuVC?.contentViewController as? UINavigationController
        
        navVC?.viewControllers = [tripViewController!]
        mainWindow?.rootViewController = sideMenuVC
    }
    
    
    private func showForceUpdateDialog(updateLink:String){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        appDelegate?.showDefaultAlert(title: nil, message: StringConstant.MessageForceUpdate) {
            UIApplication.shared.tryOpenUrl(urls: [updateLink])
        }
    }
    
    private func showOptionalUpdateDilaog(updateLink:String){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        
        appDelegate?.showTwoActionsAlert(title: nil, firstActionButtontitle: StringConstant.Update, secondActionButtontitle: StringConstant.Later, mesage: StringConstant.MessageOptionalUpdate, onCancel: {[weak self] in
            self?.navigateToNextScreen()
        }) {
            UIApplication.shared.tryOpenUrl(urls: [updateLink])
        }
    }
    
    
    private func showBlockDialog(){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        appDelegate?.showTwoActionsAlert(title: nil, firstActionButtontitle: StringConstant.Call, secondActionButtontitle: StringConstant.Later, mesage: StringConstant.MsgAppBlockedCallSupport) {
            UIApplication.shared.tryOpenUrl(urls: ["tel:\(StringConstant.SupportMobile)"])
        }
    }
}
