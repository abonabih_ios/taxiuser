//
//  PickerMapViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import GoogleMaps
import XLPagerTabStrip



protocol PickerMapViewControllerOuput {
    func pickLocation(_ sender: UIButton)
}


class PickerMapViewController:UIViewController, PickerMapViewControllerOuput, GMSMapViewDelegate, IndicatorInfoProvider
{
    
    
    @IBOutlet var  mapView:GMSMapView!
    @IBOutlet var  addressLbl:UILabel!

    
    var  pickerMarkerImageView:UIImageView!
    
    
    var modelView: PickerMapModelView?
    var router: MapPickerRouter?
    
    
    var itemInfo = IndicatorInfo(title: "-")

    
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = MapPickerRouter(scene: self)
        setUpNavigationItems()
        configureMapView()
         addressLbl.text = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        zoomToUserLocation()
        
        //Add picker pin on camera target
        adjustPickerMarker()
    }
    
    
    
    //MARK:-
    func configureMapView(){
        
        //Set MapView delegate
        mapView.delegate = self
        
        //Show user location on mapview
        mapView.isMyLocationEnabled = true
        
        //Show user location on mapview
        mapView.isTrafficEnabled = true
        
        
        pickerMarkerImageView = UIImageView(image:#imageLiteral(resourceName: "map_locator_pickup"))
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.TripTitle
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .scaleAspectFit
        backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()

        backBtn.setImage(img, for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        navigationController?.popViewControllerWithHandler {}
    }
    
    
    func adjustPickerMarker() {
        DispatchQueue.main.async {
            
            //Convert target coordinates to point on map view
            let p = self.mapView.projection.point(for: self.mapView.camera.target)
            
            //Convert target coordinates to point on mapview's superview
            let cameraCenterPointInMapView = self.mapView.convert(p, to: self.mapView.superview)
            
            //Add marker on camer target
            if self.pickerMarkerImageView.superview == nil  {
                self.view.addSubview(self.pickerMarkerImageView)
            }
            
            self.pickerMarkerImageView.center = CGPoint(x: cameraCenterPointInMapView.x, y: cameraCenterPointInMapView.y - self.pickerMarkerImageView.bounds.size.height/2)
        }
    }
    
    
    func zoomToUserLocation() {
        //Zoom to user location
        if LocationService.sharedInstance.canGetLocation() {
            LocationService.sharedInstance.getCurrentLocation(completion: {[weak self] (coordinate) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.mapView.animate(with: GMSCameraUpdate.setTarget(coordinate, zoom: 16.0))
                }
                
            }){ [weak self] error in
                self?.showDefaultAlert(title: StringConstant.Info, message: error.localizedDescription)
            }
        }
        else {
           askUserToEnableLocationService()
        }
    }
    
    
    
    
    //MARK:- GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //Get place address of camera target coordintes
        GMSGeocoder().reverseGeocodeCoordinate(position.target) {  (response, error) in
            if let _ = error {
                print(error.debugDescription)
            }
            else {
                if  let count = response?.results()?.count {
                    if count > 0 {
                        self.addressLbl.text = response!.results()![0].lines!.joined(separator: ",")
                    }
                }
            }
        }
    }
    
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    
    
    
    //MARK:- Actions
    @IBAction func zoomToUserLocation(_ sender: UIButton) {
        zoomToUserLocation()
    }
    
    @IBAction func toggleMapType(_ sender: UIButton) {
        if mapView.mapType == .normal {
            mapView.mapType = .hybrid
        }
        else {
            mapView.mapType = .normal
        }
    }
    
    
    @IBAction func pickLocation(_ sender: UIButton) {
        let parentVC = parent as? PlacePickerViewController
        
        
        let data:[String:Any] = ["latitude": mapView.camera.target.latitude, "longitude": mapView.camera.target.longitude, "name": addressLbl.text?.components(separatedBy: ",").first() ?? "", "address":addressLbl.text ?? ""]
        
        navigationController?.popViewControllerWithHandler(){
            parentVC?.didSelectPlaceHandler?(data)
        }
    }
    
}
