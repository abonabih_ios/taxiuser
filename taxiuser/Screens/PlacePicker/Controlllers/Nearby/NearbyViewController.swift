//
//  NearbyViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import XLPagerTabStrip




class NearbyViewController:UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableView:UITableView!
    
    
    var itemInfo = IndicatorInfo(title: "-")

    
    var modelView: PlaceNearbySearchModelView?

    
     var results:[PlaceItem]?

    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        modelView = PlaceNearbySearchModelView(scene: self)
        
        //TODO:- Test on simulator
        //modelView?.searchNearby(data: ["location":"30,31","radius": AppDelegate.NearbyRadius,"key":AppDelegate.GooglePlacesAPIKey])



        
        LocationService.sharedInstance.getCurrentLocation(completion: { location in
            
            self.showLoadingIndicator(false)
            

            
            self.modelView?.searchNearby(data: ["location":"\(location.latitude),\(location.longitude)","radius": AppDelegate.NearbyRadius,"key":AppDelegate.GooglePlacesAPIKey
                
                ])

            
            }) {
                [weak self] error in
                self?.showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgEnableLocServices)
                var m = 0
                m = m*2
        }
        
    }
    

    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = results?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell") as? PlaceCell
        
        cell?.placeNameLbl.text = results?[indexPath.row].name
        cell?.addressLbl.text = results?[indexPath.row].address
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let place = results?[indexPath.row]
            else { return }
        let parentVC = parent as? PlacePickerViewController
        

        let data:[String:Any] = ["latitude": place.lat, "longitude": place.lng, "name": place.name, "address": place.address]
        
        navigationController?.popViewControllerWithHandler(){
            parentVC?.didSelectPlaceHandler?(data)
        }
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
    
    //MARK:- Search by text
    
}
