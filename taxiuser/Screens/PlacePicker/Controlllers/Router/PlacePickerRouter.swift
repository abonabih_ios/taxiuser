//
//  RatesRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit


class PlacePickerRouter
{
    unowned let viewController: PlacePickerViewController
    
    init(scene: PlacePickerViewController) {
        self.viewController = scene
    }
    
    func back() {
        viewController.navigationController?.popViewControllerWithHandler {}
    }
    
    
    func showSearchResults(results:[PlaceItem]) {
        viewController.searchVCContainerView.isHidden = false
        
        let searchResultsVC = viewController.childViewControllers.first() as? SearchResultViewController
        
        searchResultsVC?.results = results
    }
}
