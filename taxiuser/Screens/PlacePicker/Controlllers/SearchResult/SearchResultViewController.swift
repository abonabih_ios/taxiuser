//
//  SearchResultViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 4/25/17.
//
//

import UIKit

class SearchResultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet weak var tableView:UITableView!
    
    
    var results:[PlaceItem]? {didSet{tableView.reloadData()}}
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = results?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell") as? PlaceCell
        
        cell?.placeNameLbl.text = results?[indexPath.row].name
        cell?.addressLbl.text = results?[indexPath.row].address
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let place = results?[indexPath.row]
            else { return }
        let parentVC = parent as? PlacePickerViewController
        
        
        let data:[String:Any] = ["latitude": place.lat, "longitude": place.lng, "name": place.name, "address": place.address]
        
        navigationController?.popViewControllerWithHandler(){
            parentVC?.didSelectPlaceHandler?(data)
        }
    }
}
