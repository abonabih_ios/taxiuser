//
//  RatesModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

class PlacePickerModelView: BaseModelView
{
    unowned let scene: PlacePickerViewController
    
    
    init(scene: PlacePickerViewController) {
        self.scene = scene
    }

    
    func searchNearby(data:[String:Any]){
        executeService(service: PlaceSearchService(), userData: data,headers: [:], tag: PlaceSearchService.className)
    }
    
    
    //MARK:-
    override func dataDidReceived(notification: NSNotification) {
        switch notification.name {
        case PlaceSearchService.notificationName:
            if let results = notification.userInfo?[BaseService.DataReceivedKey] as? [PlaceItem] {
                if results.count > 0 {
                    scene.router?.showSearchResults(results:results)
                }
            }
            scene.hideLoadingIndicator()
            break
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        switch notification.name {
        case PlaceSearchService.notificationName:
            scene.hideLoadingIndicator()
            break
        default:
            break
        }
        super.errorDidReceived(notification: notification)
    }
}
