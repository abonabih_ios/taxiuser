//
//  RecentPlacesModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/19/17.
//
//

import Foundation

class RecentPlacesModelView: BaseModelView
{
    unowned let scene: RecentPlacesViewController
    
    
    init(scene: RecentPlacesViewController) {
        self.scene = scene
    }
    
    
    
    //MARK:- Load Recent Places
    func loadRecentPlaces(){
        scene.recentPlaces = RecentService.recentPlaces()
    }
    
    
    func removeRecentPlace(place:RecentPlace){
        RecentService.removePlaceFromRecent(place: place)
    }
}
