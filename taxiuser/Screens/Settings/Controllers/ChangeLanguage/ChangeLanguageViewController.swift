//
//  ChangePasswordViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController

class ChangeLanguageViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((Void)->Void)?

    private let  langugaes = ["en", "ar", "ur"]

    
    
    
    
    //MARK:- Validate
    func changeLanguage(index:Int) {
        
        showTwoActionsAlert(title: StringConstant.Info, firstActionButtontitle: StringConstant.Restart, secondActionButtontitle: StringConstant.Cancel, mesage: StringConstant.MsgChangeLanguage, onCancel: {
            
        }) {
            UserDefaults.standard.set([self.langugaes[index]], forKey: "AppleLanguages")
            
            UserDefaults.standard.synchronize()
            exit(0)
        }
    }
    
    
    
    
    
    
    //MARK:- Actions
    @IBAction func apply(_ sender: UIButton) {
        
        let currentLanguage = (UserDefaults.standard.array(forKey: "AppleLanguages") as? [String])?.first() ?? "en"
        
        if currentLanguage != langugaes[sender.tag] {
            changeLanguage(
                index: sender.tag)
        }
        else {
            completionHandler?()
        }
    }
    
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 240)
    }
}
