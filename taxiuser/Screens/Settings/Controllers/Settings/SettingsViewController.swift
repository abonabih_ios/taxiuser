//
//  SettingsViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import SwiftyUserDefaults


class SettingsViewController:UITableViewController
{
    

    var modelView: SettingsModelView?
    var router: SettingsRouter?
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!

    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = SettingsRouter(scene: self)
        modelView = SettingsModelView(scene: self)

        setUpNavigationItems()
        refreshUI()
    }

    func refreshUI(){
        nameLbl.text = LoggedUser.shared?.name
        mobileLbl.text = Defaults[.userMobileNumber]
        genderLbl.text = LoggedUser.shared?.gender == "1" ? StringConstant.Male : StringConstant.Female
        
        emailLbl.text = LoggedUser.shared?.email
        
        let currentLanguage = (UserDefaults.standard.array(forKey: "AppleLanguages") as? [String])?.first() ?? "en"
        
        languageLbl.text = I18n.localizedString(currentLanguage)

    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.Settings
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        backBtn.contentMode = .scaleAspectFit
       backBtn.addTarget(self, action: #selector(showSideMenu), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }

    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    
    //MARK:- UITableView delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            switch indexPath.row {
            case 4:
                router?.showChangeEmailVC()
            case 5:
                router?.showChangePasswordVC()
            case 7:
                router?.showChangeLanguageVC()
            default:
                break
            }
    }
}
