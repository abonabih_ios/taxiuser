//
//  SettingsModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit

class SettingsModelView: BaseModelView
{
    
    unowned let scene: SettingsViewController
    
    
    init(scene: SettingsViewController) {
        self.scene = scene
    }

    
    func changePassword(data:[String:Any]){
        let service = ChangePasswordService()
        executeService(service: service, userData: data,headers:[:], tag: ChangePasswordService.className)
    }
    
    func changeEmail(data:[String:Any]){
        let service = ChangeEmailService()
        executeService(service: service, userData: data,headers:[:], tag: ChangeEmailService.className)
    }
    
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case ChangePasswordService.notificationName:
            scene.showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgPassChanged)
        case ChangeEmailService.notificationName:
            scene.refreshUI()
            scene.showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgEmailChanged)
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
        
        let error =  notification.userInfo?[BaseService.DataReceivedWithErrorKey] as? QLError
        
        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        super.errorDidReceived(notification: notification)
    }
}

