//
//  SettingsRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import PopupController



class SettingsRouter
{
    unowned let viewController: SettingsViewController
    
    private var popupVC:PopupController?

    
    init(scene: SettingsViewController) {
        self.viewController = scene
    }
    
    func showChangePasswordVC() {
        let changePasswordVC = ChangePasswordViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")  as? ChangePasswordViewController
        
        
        changePasswordVC?.completionHandler = {[weak self]  data  in
            self?.popupVC?.dismiss()
            self?.viewController.showLoadingIndicator()
            self?.viewController.modelView?.changePassword(data: data as? [String:Any] ?? [:])
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(changePasswordVC!)
    }
    
    func showChangeEmailVC(){
        let changeEmailVC = ChangeEmailViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")  as? ChangeEmailViewController
        
        
        changeEmailVC?.completionHandler = {[weak self]  data  in
            
            self?.viewController.showLoadingIndicator()
            self?.viewController.modelView?.changeEmail(data: data as? [String:Any] ?? [:])

            self?.popupVC?.dismiss()
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(changeEmailVC!)
    }
    
    
    func showChangeLanguageVC(){
        let changeLanguageVC = ChangeLanguageViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")  as? ChangeLanguageViewController
        
        
        changeLanguageVC?.completionHandler = {[weak self]  data  in
            
//            self?.viewController.showLoadingIndicator()
//            self?.viewController.modelView?.changeEmail(data: data as? [String:Any] ?? [:])
            
            self?.popupVC?.dismiss()
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(changeLanguageVC!)
    }

}
