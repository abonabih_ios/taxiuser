//
//  SideMenuModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/24/16.
//
//

import UIKit

class SideMenuModelView: BaseModelView
{
    unowned let scene: SideMenuViewController
    
    
    init(scene: SideMenuViewController) {
        self.scene = scene
    }
}
