//
//  SideMenuRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/24/16.
//
//

import UIKit


class SideMenuRouter
{
    unowned let viewController: SideMenuViewController
    
    init(scene: SideMenuViewController) {
        self.viewController = scene
    }

    func openVC(destinationViewController:UIViewController){
        let nvc = viewController.mainNavigationController()
        
        if let hamburguerViewController = viewController.findHamburguerViewController() {
            
            hamburguerViewController.hideMenuViewControllerWithCompletion{
                nvc.viewControllers = [destinationViewController]
                hamburguerViewController.contentViewController = nvc
            }
        }
    }
    
    func navigateToHome() {
        let homeVC = HomeViewController.instanceFromStoryboard(storyboardName: "Main")
       openVC(destinationViewController: homeVC!)
    }
    
    func navigateLogin(){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let loginVc = AuthViewController.instanceFromStoryboard(storyboardName: "Authorization_Storyboard")
        appDelegate?.window?.rootViewController = loginVc
    }
    
    func navigateToSettings() {
        let settingsVC = SettingsViewController.instanceFromStoryboard(storyboardName: "Settings_Storyboard")
        openVC(destinationViewController: settingsVC!)
    }
    
    func navigateToBookedRides() {
        let bookedRidesVC = BookedRidesViewController.instanceFromStoryboard(storyboardName: "BookedRides_Storyboard")
        openVC(destinationViewController: bookedRidesVC!)
    }
    
    func navigateToFavorites() {
        let favoritesVC = FavoritePlacesViewController.instanceFromStoryboard(storyboardName: "Favorites_Storyboard")
        openVC(destinationViewController: favoritesVC!)
    }

    
    func navigateToHelp() {
        let helpVC = HelpViewController.instanceFromStoryboard(storyboardName: "Help_Storyboard")
        openVC(destinationViewController: helpVC!)
    }
    
    func navigateToAbout() {
        let aboutVC = AboutViewController.instanceFromStoryboard(storyboardName: "About_Storyboard")
        openVC(destinationViewController: aboutVC!)
    }

}
