//
//  BookingTripViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import GoogleMaps
import RealmSwift
import SwiftDate
import SwiftyUserDefaults



//MARK:- BookingType
enum BookingType:Int {
    case basic = 1
    case kids = 2
    case local = 3
    case global = 4
}



//MARK:- BookingTime
enum BookingTime:Int {
    case now = 1
    case later = 2
}






//MARK:- BookingTripViewController
class BookingTripViewController:UIViewController, GMSMapViewDelegate
{
    
    
    enum RideStep:Int {
        case pickupLocation = 0
        case dateAndTimeSelection = 1
        case configuration = 2
        
        init(step:Int) {
            if step > 2 {
                self = .configuration
            }
            else if step < 0 {
                self = .pickupLocation
            }
            else {
                self = RideStep(rawValue:step)!
            }
        }
    }
    
    
    
    
    
    
    @IBOutlet var  mapView:GMSMapView!
    
    @IBOutlet  var economyView:RoundedCornersView!
    @IBOutlet var  businessView:RoundedCornersView!
    
    @IBOutlet  var economyImgView:UIImageView!
    @IBOutlet var  businessImgView:UIImageView!
    
    @IBOutlet  var economyLbl:UILabel!
    @IBOutlet var  businessLbl:UILabel!
    
    @IBOutlet weak var stepContainerView: UIView!
    @IBOutlet weak var stepContainerViewHieghtConstraints: NSLayoutConstraint!
    
    //Back and next buttons
    @IBOutlet weak var previousImgView: UIImageView!
    @IBOutlet weak var previousLbl: UILabel!
    
    @IBOutlet weak var nextImgView: UIImageView!
    @IBOutlet weak var nextLbl: UILabel!
    
    
    
    
    var  pickerMarkerImageView:UIImageView!
    
    var pickupLocationMarker:GMSMarker?
    
    
    var pickupLocationView:PickupLocationView?
    var tripTimeView:TripTimeView?
    
    
    var modelView: BookingTripModelView?
    var router: BookingTripRouter?
    
    var currentStep:RideStep = .pickupLocation {didSet {perfromCurrentStep()}}
    
    
    var bookingData:[String:Any] = [:]
    
    
    var bookingType:BookingType = .basic
    var bookingTime:BookingTime = .now
    
    var selectedDates:[Date] = []
    
    
    var configurationView:ConfigurationView?
    var kidsConfigurationView:KidsConfigurationView?

    var pickupLocation:CLLocationCoordinate2D?
    
    var dropOffPlaceName:String?
    
    var pickupPlaceName:String?

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        zoomToUserLocation()
        
        router = BookingTripRouter(scene: self)
        modelView = BookingTripModelView(scene: self)
        setUpNavigationItems()
        configureMapView()
        
        
        bookingData = [
            "pick_lat": mapView.camera.target.latitude,
            "pick_long": mapView.camera.target.longitude,
            "car_type":"1",
            "user_type":"1"
        ]
        
        Defaults[.notes] = ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.pickupLocationView?.addressLbl.text = nil
        self.configurationView?.destinationPlaceNameLbl.text = dropOffPlaceName
        self.kidsConfigurationView?.destinationPlaceNameLbl.text = dropOffPlaceName

        if let coordinates = pickupLocation {
           zoomToLocation(location2d: coordinates)
        }
        
        //Add picker pin on camera target
        adjustPickerMarker()

        perfromCurrentStep()
    }
    
    
    //MARK:- Trip steps
    private func perfromCurrentStep(){
        for view in stepContainerView.subviews {
            view.removeFromSuperview()
        }
        
//        pickerMarkerImageView.isHidden = currentStep != .pickupLocation
        mapView.isUserInteractionEnabled =  currentStep == .pickupLocation
        economyView.superview?.isHidden =  currentStep != .pickupLocation
        
        if currentStep == .pickupLocation {
            mapView.clear()
        }
        else {
            let position = mapView.camera.target
            
            if pickupLocationMarker == nil {
                pickupLocationMarker = GMSMarker(position: position)
                setMarkIcon()
                
                pickupLocationMarker?.map = mapView
            }
            
        }
        
        switch currentStep {
        case .pickupLocation:
            performPickupLocationStep()
        case .dateAndTimeSelection:
            performDateAndTimeStep()
        case .configuration:
            performConfiguration()
        }
    }
    
    private func performPickupLocationStep(){
        
        
        
        pickupLocationView = pickupLocationView ??  UINib(nibName:String(describing: PickupLocationView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PickupLocationView
        
        pickupLocationView?.delegate = self
        
        stepContainerViewHieghtConstraints.constant = pickupLocationView!.bounds.height
        
        stepContainerView.addSubview(pickupLocationView!)
        pickupLocationView?.frame = stepContainerView.bounds
        
        previousImgView.image = #imageLiteral(resourceName: "map_now")
        nextImgView.image = #imageLiteral(resourceName: "map_later")
        
        previousLbl.text = StringConstant.Now
        nextLbl.text = StringConstant.Later
        
        pickupLocationView?.addressLbl.text =  pickupPlaceName
        
    }
    
    private func performDateAndTimeStep(){
        tripTimeView = tripTimeView ??   UINib(nibName: String(describing: TripTimeView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TripTimeView
        
        tripTimeView?.delegate = self
        stepContainerViewHieghtConstraints.constant = tripTimeView!.bounds.height
        
        stepContainerView.addSubview(tripTimeView!)
        tripTimeView?.frame = stepContainerView.bounds
        
        tripTimeView?.selectedTimeLbl.text = tripTimeView?.selectedTimeLbl.text ?? Date().string(format: .strict("hh:mm a"))
        
        
        tripTimeView?.selectedDateLbl.text = tripTimeView?.selectedDateLbl.text ?? Date().string(format: .strict("yyyy-mm-dd"))


        
        previousImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ?  #imageLiteral(resourceName: "map_back") :  #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()
        nextImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ?  #imageLiteral(resourceName: "map_next") :  #imageLiteral(resourceName: "map_next").imageFlippedForRightToLeftLayoutDirection()
        
        
        previousLbl.text = StringConstant.Back
        nextLbl.text = StringConstant.Next
        
    }
    
    private func performConfiguration(){
        
        if bookingType == .basic {
            
            configurationView =  UINib(nibName: String(describing: ConfigurationView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ConfigurationView
            
            
            
            configurationView?.delegate = self
            
            configurationView?.selectedGender = bookingData["user_type"] as? Int
            
            stepContainerViewHieghtConstraints.constant = 200
            
            stepContainerView?.addSubview(configurationView!)
            configurationView?.frame = CGRect(x: 0, y: 0, width: stepContainerView!.bounds.width, height: 200)
            
        }
        else if bookingType == .kids  {
            
           kidsConfigurationView =  UINib(nibName: String(describing: KidsConfigurationView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? KidsConfigurationView
            
            
            
            kidsConfigurationView?.selectedGender = bookingData["user_type"] as? Int
            
            kidsConfigurationView?.delegate = self
            
            stepContainerViewHieghtConstraints.constant = 228
            
            stepContainerView.addSubview(kidsConfigurationView!)
            kidsConfigurationView?.frame = CGRect(x: 0, y: 0, width: stepContainerView.bounds.width, height: 228)
        }
        
        
        if dropOffPlaceName != nil {
            self.configurationView?.destinationPlaceNameLbl.text = dropOffPlaceName
            self.kidsConfigurationView?.destinationPlaceNameLbl.text = dropOffPlaceName
            
            configurationView?.removeBtn.isHidden = false
            kidsConfigurationView?.removeBtn.isHidden = false
        }
        
        previousImgView.image = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()
        nextImgView.image = #imageLiteral(resourceName: "map_go")
        
        previousLbl.text = StringConstant.Back
        nextLbl.text = StringConstant.Start
        
    }
    
    
    
    //MARK-
    func setMarkIcon() {
        
        pickupLocationMarker?.icon = #imageLiteral(resourceName: "map_locator_pickup")
//        _ = bookingData["user_type"] as? Int ?? 1
       
        if bookingType == .basic {
            
//            pickupLocationMarker?.icon =  gender == 1 ? #imageLiteral(resourceName: "map_locator_man") :  #imageLiteral(resourceName: "map_locator_woman")
        }
        else {
            //pickupLocationMarker?.icon =  gender == 1 ? #imageLiteral(resourceName: "map_locator_boy") :  #imageLiteral(resourceName: "map_locator_girl")
        }
    }
    
    
    //MARK:-
    func configureMapView(){
        //Adjust map padding
        let mapInsets = UIEdgeInsetsMake(0.0, 0.0, 60, 0.0)
        mapView.padding = mapInsets
        
        //Set MapView delegate
        mapView.delegate = self
        
        //Show user location on mapview
        mapView.isMyLocationEnabled = true
        
        //Show user location on mapview
        mapView.isTrafficEnabled = false
        
        
        pickerMarkerImageView = UIImageView(image:#imageLiteral(resourceName: "map_locator_pickup"))
        
        if pickerMarkerImageView.superview == nil {
            self.view.addSubview(self.pickerMarkerImageView)
        }
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.TripTitle

        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .scaleAspectFit
        backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()

        backBtn.setImage(img, for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        
        navigationItem.leftBarButtonItems = [backItem]
    }
    
    func back(){
        navigationController?.popViewControllerWithHandler {}
    }
    
    
    func adjustPickerMarker() {
        DispatchQueue.main.async {
            
            //Convert target coordinates to point on map view
            let p = self.mapView.projection.point(for: self.mapView.camera.target)
            
            
            //Convert target coordinates to point on mapview's superview
            let cameraCenterPointInMapView = self.mapView.convert(p, to: self.mapView.superview)
            
            //Add marker on camer target

            self.pickerMarkerImageView.center = CGPoint(x: cameraCenterPointInMapView.x, y: cameraCenterPointInMapView.y - self.pickerMarkerImageView.bounds.size.height/2)
        }
    }
    
    func zoomToLocation(location2d:CLLocationCoordinate2D) {
        DispatchQueue.main.async {
            self.mapView.animate(with: GMSCameraUpdate.setTarget(location2d, zoom: 16.0))
        }
    }
    
    func zoomToUserLocation() {
        //Zoom to user location
        if LocationService.sharedInstance.isLocationServicesEnabled() {
            LocationService.sharedInstance.getCurrentLocation(completion: { (coordinate) in
                
                DispatchQueue.main.async {
                    self.mapView.animate(with: GMSCameraUpdate.setTarget(coordinate, zoom: 16.0))
                    
                    self.bookingData["pick_lat"] = coordinate.latitude
                    self.bookingData["pick_long"] = coordinate.longitude
                    
                    self.bookingData["req_lat"] =  coordinate.latitude
                    self.bookingData["req_long"] =  coordinate.latitude
                    
                    self.pickupLocation = coordinate
                    
                    //self.booking = Booking(value: self.bookingData)
                    
                }
                
            }){ error in
                self.showDefaultAlert(title: StringConstant.Info, message: error.localizedDescription)
            }
        }
        else {
            askUserToEnableLocationService()
        }
    }
    
    @IBAction func setTripType(_ sender: UIButton) {
        if sender.tag == 1  //Economy button tapped
        {
            economyView.backgroundColor = .white
            economyImgView.image = #imageLiteral(resourceName: "map_car_economy_gray")
            economyLbl.textColor = .black
            
            businessView.backgroundColor = CustomStyle.themeColor
            businessImgView.image = #imageLiteral(resourceName: "map_car_business_white")
            businessLbl.textColor = .white
            
            
        }
        else
        {
            economyView.backgroundColor = CustomStyle.themeColor
            economyImgView.image = #imageLiteral(resourceName: "map_car_economy_white")
            economyLbl.textColor = .white
            
            businessView.backgroundColor = .white
            businessImgView.image = #imageLiteral(resourceName: "map_car_business_gray")
            businessLbl.textColor = .black
            
            
        }
    }
    
    
    
    
    //MARK:- GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //Get place address of camera target coordintes

        pickupLocation = nil

        let location = pickupLocation ?? position.target
        
        pickupLocation = location
        
        GMSGeocoder().reverseGeocodeCoordinate(location) {  (response, error) in
            if let _ = error {
                print(error.debugDescription)
            }
            else {
                if  let count = response?.results()?.count {
                    if count > 0 {
                        
                        let address = response?.results()?[0].lines?.joined(separator: ",") ?? ""
                        
                        self.bookingData["pick_address"] = address
                        self.pickupPlaceName = address
                        self.pickupLocationView?.addressLbl.text =  self.pickupPlaceName
                    }
                }
            }
        }
        
    }
    
    
    
    //MARK:- Actions
    @IBAction func zoomToUserLocation(_ sender: UIButton) {
        zoomToUserLocation()
    }
    
    @IBAction func toggleMapType(_ sender: UIButton) {
        if mapView.mapType == .normal {
            mapView.mapType = .hybrid
        }
        else {
            mapView.mapType = .normal
        }
    }
    
    
    @IBAction func selectCarType(_ sender: UIButton) {
        bookingData["car_type"] = "\(sender.tag)"
    }
    
    @IBAction func stepBack(_ sender: UIButton) {
        //Check status type go to configuration/or date selections
        
        
        if currentStep == .pickupLocation {
            currentStep = .configuration
            bookingTime = .now
        }
        else if  currentStep == .configuration && bookingTime == .now  {
            currentStep = .pickupLocation
        }
        else {
            let step =  RideStep(step:currentStep.rawValue - 1)
            
            if step != currentStep {
                currentStep = step
            }
        }
    }
    
    @IBAction func stepNext(_ sender: UIButton) {
        //Check status type go to configuration/or date selections
        
        if currentStep == .configuration {
            
           
            
            if configurationView?.selectedGender == nil  && bookingType == .basic {
                showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgSelectGender)
            }
            else if kidsConfigurationView?.selectedGender == nil  && bookingType == .kids {
                showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgSelectGender)
            }
            else {
                
                bookingData["notes"] = Defaults[.notes]

                bookingData["ride_type"] = "\(bookingTime.rawValue)"
                bookingData["pick_lat"] = mapView.camera.target.latitude
                bookingData["pick_long"] = mapView.camera.target.longitude
                
                let datArrayString = selectedDates
                    .map {
                        $0.inGMTRegion().absoluteDate.string(format: .iso8601(options: [.withFullDate, .withFullTime , .withSpaceBetweenDateAndTime])).substring(to: StringConstant.DateFormate.endIndex)}
                    .joined(separator: ",")
                
                bookingData["date_time_array"] = "\(datArrayString)"
                
                if self.bookingType == .kids {

                    let followupNumbers = Defaults[.followupNumbers]
                    
                    if followupNumbers.count > 0 {
                        bookingData["follow_up_tel"] = "[\(followupNumbers.joined(separator: ","))]"
                    }
                }
                
                bookingData["repeat"] = selectedDates.count >= 1 ?  1 : 0
                
                let placeData:[String:Any] = ["latitude":mapView.camera.target.latitude,
                                              "longitude": mapView.camera.target.longitude,
                                              "name": pickupLocationView?.addressLbl.text?.components(separatedBy: ",").first() ?? " ",
                                              "address":  pickupLocationView?.addressLbl.text ?? "" ,
                                              "date": Date().inGMTRegion().absoluteDate]
                
                
                
                
                self.modelView?.addRecentPlace(placeData: placeData)
                
                
                LocationService.sharedInstance.getCurrentLocation(completion: { location in
                    
                    self.modelView?.addRecentPlace(placeData: placeData)
                    
                    self.bookingData["req_lat"] = location.latitude
                    self.bookingData["req_long"] = location.longitude
                    
                    
                    self.showLoadingIndicator()
                    self.modelView?.bookTrip(type: self.bookingType, time:self.bookingTime, data:self.bookingData)
                    
                }, failure: { error in
                    self.showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgEnableLocServices)
                })
                
                return
            }
        }
        else if currentStep == .dateAndTimeSelection && bookingTime == .later &&  selectedDates.isEmpty {
            showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgSelectDate)
        }
        else if currentStep == .dateAndTimeSelection && bookingTime == .later &&  tripTimeView?.selectedTimeLbl.text?.isEmpty == true {
            showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgSelectTime)
        }
        else {
            if currentStep == .pickupLocation {
                bookingTime = .later
            }
            
            let step =  RideStep(step:currentStep.rawValue + 1)
            if step != currentStep {
                currentStep = step
            }
        }
    }
}
