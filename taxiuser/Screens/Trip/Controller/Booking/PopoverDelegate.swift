//
//  PopoverDelegate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/5/17.
//
//

import UIKit


extension BookingTripViewController:UIPopoverPresentationControllerDelegate
{
    // popover settings, adaptive for horizontal compact trait
    // #pragma mark - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
}
