//
//  DatePickerViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/12/17.
//
//

import UIKit
import FSCalendar


class DatePickerViewController: UIViewController
{
    
    @IBOutlet weak var calenderView: FSCalendar!
    @IBOutlet weak var backImgView:UIImageView!
    
    
    var completionHandler: (([Date]) -> Void)!

    
    var selectedDates:[Date] = []
    
    
    
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItems()
        
        
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()

        backImgView.image = img

    }

    
    private func setUpNavigationItems() {
        self.title = StringConstant.DatePickerTitle
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        backBtn.contentMode = .scaleAspectFit
        backBtn.addTarget(self, action: #selector(showSideMenu), for: UIControlEvents.touchUpInside)
        backBtn.setImage(#imageLiteral(resourceName: "menu_ico"), for: .normal)
        let backItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [backItem]
    }

    func showSideMenu(){
        findHamburguerViewController()?.showMenuViewController()
    }
    
    //MARK:- Actions
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewControllerWithHandler {}
    }
    
    
    @IBAction func confirm(_ sender: UIButton) {
        selectedDates = calenderView.selectedDates as [Date]
        completionHandler(selectedDates)
        back(sender)
    }
}
