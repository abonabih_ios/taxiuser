//
//  FareEstimateViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit
import PopupController
import Cent


class FareEstimateViewController: UIViewController, PopupContentViewController
{
    
    @IBOutlet weak var estimateLbl: UILabel!
    
    var completionHandler:((Void)->Void)?
    
    
    var estimate:Double = 0.0
    
    
    
    
    //MARK:- 
    override func viewDidLoad() {
        
        let estimateString:NSString = NSString(string:"\(estimate) \(StringConstant.Qar)")
       
        let estimateAttributedString = NSMutableAttributedString(string: estimateString as String, attributes: [:])
        
            estimateAttributedString.addAttributes([NSForegroundColorAttributeName: CustomStyle.themeColor], range: estimateString.range(of: StringConstant.Qar))

        
        estimateLbl.attributedText = estimateAttributedString
    }
    
    
    @IBAction func done(_ sender: UIButton) {
        completionHandler?()
    }

    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 228)
    }

}
