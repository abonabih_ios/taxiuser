//
//  FavoritePlacesViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import SwiftyUserDefaults



class FollowupNumbersViewController: UIViewController,UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView:UITableView!
    
    
    
    var followupNumbers:[String]?
    
    var router:FollowupRouter?
    
    var editable:Bool = true
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router = FollowupRouter(scene: self)
        setUpNavigationItems()
        if editable {
            followupNumbers =  Defaults[.followupNumbers]
        }
        
        tableView.reloadData()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.FollowUpNumbers
        let backBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
        backBtn.contentMode = .center
        backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        let img = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight ? #imageLiteral(resourceName: "map_back") : #imageLiteral(resourceName: "map_back").imageFlippedForRightToLeftLayoutDirection()

        backBtn.setImage(img, for: .normal)
        let menuItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItems = [menuItem]
        
        
        if editable {
            let addNumberBtn = UIButton(frame: CGRect(x:0,y:0,width:30,height:30))
            addNumberBtn.contentMode = .scaleAspectFill
            addNumberBtn.addTarget(self, action: #selector(addNewFollowupNumber), for: .touchUpInside)
            addNumberBtn.setImage(#imageLiteral(resourceName: "favorites_add"), for: .normal)
            let addNumberMenuItem = UIBarButtonItem(customView: addNumberBtn)
            navigationItem.rightBarButtonItems = [addNumberMenuItem]
        }

    }
    
    func back(){
        navigationController?.popViewControllerWithHandler(){}
    }
    
    
    
    func addNewFollowupNumber(){
        router?.showAddFollowUpVC()
    }
    
    
    //MARK:- UITableView datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = followupNumbers?.count else {
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowUpViewCell") as? FollowUpViewCell
        
        let followup = followupNumbers?[indexPath.row].components(separatedBy: "@")
        
        cell?.placeNameLbl.text = followup?[0]
        cell?.addressLbl.text =  followup?[1]
        
        
        if editable {
            
            let delteAction = ABUITableViewCellRowAction(title: StringConstant.Delete) {
                
                self.followupNumbers?.remove(at: indexPath.row)
                Defaults[.followupNumbers] = self.followupNumbers!
                
                self.tableView.reloadData()
            }
            
            delteAction.backgroundColor  = .red
            
            
            cell?.rightActions = [delteAction]
        }
        
        return cell!
    }
    
    //MARK:- UITableView delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
