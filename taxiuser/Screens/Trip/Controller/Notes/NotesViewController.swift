//
//  FareEstimateViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/20/17.
//
//

import UIKit
import PopupController
import Cent
import SwiftyUserDefaults

class NotesViewController: UIViewController, PopupContentViewController
{
    
    @IBOutlet weak var notesTextView: UITextView!
    
    var notes:String?
    
    
    var completionHandler:((Void)->Void)?
    
    var isEditable = true
    
    //MARK:- 
    override func viewDidLoad() {
        
        if isEditable {
            notesTextView.text = Defaults[.notes]
        }
        else {
             notesTextView.text = notes
        }
        
        notesTextView.isEditable = isEditable
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    @IBAction func dismissViewTapGesture(_ sender: UITapGestureRecognizer) {
        completionHandler?()
    }
    
    
    @IBAction func submit(_ sender: UIButton) {
        if isEditable {
//            if notesTextView.text.isEmpty{
//                self.showDefaultAlert(title: "<#T##String?#>", message: <#T##String?#>)
//            }else{
//                Defaults[.notes] = notesTextView.text ?? ""
//            }
            Defaults[.notes] = notesTextView.text ?? ""
        }
        completionHandler?()
    }

    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 300)
    }
}
