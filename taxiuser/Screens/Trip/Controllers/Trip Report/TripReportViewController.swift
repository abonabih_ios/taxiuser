//
//  TripReportViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController


class TripReportViewController: UIViewController, PopupContentViewController
{
    
    
    @IBOutlet weak var costLbl: UILabel!
    @IBOutlet weak var costUnitLbl: UILabel!
    @IBOutlet weak var distLbl: UILabel!
    @IBOutlet weak var timebl: UILabel!
    @IBOutlet weak var waitingLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!

    
    var rideStateResponse:RideStateResponse?
    
    
    var completionHandler:((Void)->Void)?

    //MARK:- Lifecycle
    override func viewDidLoad() {
        costLbl.text = rideStateResponse?.cost
        costUnitLbl.text = StringConstant.Qar
        distLbl.text = "\((rideStateResponse?.distance ?? 0).roundTo(places:2)) \(StringConstant.KM)"

        timebl.text = "\(rideStateResponse?.time ?? "-") \(StringConstant.Minute)"
        waitingLbl.text = "\(rideStateResponse?.waiting ?? "-") \(StringConstant.Minute)"
        paymentLbl.text = "\(rideStateResponse?.paymentAmount ?? "-") \(StringConstant.Qar)"
    }
    
    
    
    
    //MARK:- Actions
    @IBAction func done(_ sender: UIButton) {
        completionHandler?()
    }

    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 390)
    }
}
