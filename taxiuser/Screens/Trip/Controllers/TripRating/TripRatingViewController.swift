//
//  TripRatingViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/10/17.
//
//

import UIKit
import PopupController
import HCSStarRatingView
import SwiftyUserDefaults
import RealmSwift


class TripRatingViewController: UIViewController, PopupContentViewController
{
    
    var completionHandler:((Void)->Void)?
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var userNameLbl: UILabel!
    

    @IBOutlet weak var driverImageView: RoundedCornerImageView!
    
    
    
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        userNameLbl.text = LoggedUser.shared?.name
        
        driverImageView.af_setImage(withURL: URL(string: Defaults[.driverImgUrl])!)
    }
    
    
    //MARK:- Actions
    @IBAction func submit(_ sender: UIButton) {
        completionHandler?()
        Realm.tripRealm.clearCurrentTrip()
    }
    
    
    //MARK:- PopupContentViewController
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 16, height: 390)
    }
}
