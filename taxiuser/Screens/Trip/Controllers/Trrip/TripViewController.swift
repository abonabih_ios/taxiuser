//
//  TripViewController.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/29/16.
//
//

import UIKit
import GoogleMaps
import RealmSwift
import NVActivityIndicatorView
import AlamofireImage
import SwiftyUserDefaults

class TripViewController: UIViewController
{
    enum RideStatus:Int {
        case checkingOrderStatus = 0
        case driverOnWay = 1
        case driverArrived = 2
        case onTrip = 3
        case rating = 4
        case tripReport = 5

        init(status:Int) {
            if status >= 5 {
                self = .tripReport
            }
            else if status < 0 {
                self = .checkingOrderStatus
            }
            else {
                self = RideStatus(rawValue:status)!
            }
        }
    }
    
    
    @IBOutlet var  mapView:GMSMapView!
    
    @IBOutlet weak var stepContainerView: UIView!
    @IBOutlet weak var stepContainerViewHieghtConstraints: NSLayoutConstraint!
    
    var radarSearchView: NVActivityIndicatorView!
    
    
    var searchTimer:Timer?
    var rideTimer:Timer?

    
    var modelView: TripModelView?
    var router: TripRouter?
    
    var currentStatus:RideStatus = .checkingOrderStatus {didSet {didChangeTripStatus()}}
    
    
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        radarSearchView = NVActivityIndicatorView(frame:view.bounds)

        radarSearchView.color = UIColor(hex6:0xD14041)
        view.addSubview(radarSearchView)
        
        router = TripRouter(scene: self)
        setUpNavigationItems()
        configureMapView()
        
        modelView = TripModelView(scene: self)

        searchTimer =  Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(checkOrderStatus), userInfo: nil, repeats: true)
        
        searchTimer?.fire()
        // addressLbl.text = nil
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        radarSearchView.frame = (UIApplication.shared.delegate as? AppDelegate)?.window?.bounds ?? .zero

        radarSearchView.center = view.center

        radarSearchView.type = .ballScaleMultiple
        
        
        radarSearchView.padding = 0
        radarSearchView.setNeedsDisplay()
        radarSearchView.layoutSubviews()
        
        zoomToUserLocation()


    }
    
    
    //MARK:- Trip status
    private func didChangeTripStatus(){
        for view in stepContainerView.subviews {
            view.removeFromSuperview()
        }
    }
    
    
    @objc func checkOrderStatus(){
        radarSearchView.type = .ballScaleMultiple
        radarSearchView.startAnimating()
        currentStatus = .checkingOrderStatus
        modelView?.checkOrderStatus()
    }
    
    
    @objc func checkRideStatus(){
        radarSearchView.type = .ballScaleMultiple
        modelView?.checkRideStatus()
    }

    
    func driverOnWayStep(orderStateResponse:OrderStateResponse){
        
        currentStatus = .driverOnWay
        

        searchTimer?.invalidate()
        searchTimer = nil

        rideTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(checkRideStatus), userInfo: nil, repeats: true)

        
        rideTimer?.fire()
        
        
        radarSearchView.stopAnimating()
        
        let driverOnWayView =  UINib(nibName: String(describing: DriverView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverView
        
        driverOnWayView.driverImgView.af_setImage(withURL: URL(string:orderStateResponse.driverPicUrl)!)
        
        driverOnWayView.driverNameLbl.text = orderStateResponse.driverName
        driverOnWayView.carTimeLbl.text = orderStateResponse.carTime
        driverOnWayView.carDistanceLbl.text = "\((orderStateResponse.carDistance/1000).roundTo(places:2)) \(StringConstant.KM)"
        driverOnWayView.carPlateLbl.text = Defaults[.carModel]
        driverOnWayView.carModelLbl.text = "\(orderStateResponse.carBrand) \(orderStateResponse.carModel) \(orderStateResponse.carYear)"


        
        adjustPickerMarker(lat: orderStateResponse.carLat, lng: orderStateResponse.carLong)
        
        
        stepContainerViewHieghtConstraints.constant = 150
        stepContainerView.addSubview(driverOnWayView)
        driverOnWayView.frame = CGRect(x: 0, y: 0, width: stepContainerView.bounds.width, height: 150)
    }
    
    
    
    func driverOnWayStep(rideStateResponse:RideStateResponse){
        
        radarSearchView.stopAnimating()

        radarSearchView.stopAnimating()
        
        let driverOnWayView =  UINib(nibName: String(describing: DriverView.self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverView
        
        driverOnWayView.driverImgView.af_setImage(withURL: URL(string:Defaults[.driverImgUrl])!)
        
        driverOnWayView.driverNameLbl.text = Defaults[.driverName]
        driverOnWayView.carPlateLbl.text = Defaults[.carPlate]
        driverOnWayView.carModelLbl.text = "\(Defaults[.carBrand]) \(Defaults[.carModel])"
        


        driverOnWayView.carTimeLbl.text = "\(rideStateResponse.carTime) \(StringConstant.Minute)"
        
        driverOnWayView.carDistanceLbl.text =  "\((rideStateResponse.carDistance/1000).roundTo(places:2)) \(StringConstant.KM)"
        

        
        adjustPickerMarker(lat: rideStateResponse.carLat, lng: rideStateResponse.carLong)
        
        
        stepContainerViewHieghtConstraints.constant = 150
        stepContainerView.addSubview(driverOnWayView)
        driverOnWayView.frame = CGRect(x: 0, y: 0, width: stepContainerView.bounds.width, height: 150)
    }

    
    
    func onTripStep(rideStateResponse:RideStateResponse){
        currentStatus = .onTrip
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.hidesBackButton = true
        
        let driverOnTripViewew =  UINib(nibName: "DriverOnTripView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DriverOnTripView
        
        driverOnTripViewew.driverImgView.af_setImage(withURL: URL(string:Defaults[.driverImgUrl])!)
        
        driverOnTripViewew.driverNameLbl.text = Defaults[.driverName]
        driverOnTripViewew.carPlateLbl.text = Defaults[.carPlate]
        driverOnTripViewew.carModelLbl.text = Defaults[.carModel]

        stepContainerViewHieghtConstraints.constant = driverOnTripViewew.bounds.height
        
        adjustPickerMarker(lat: rideStateResponse.carLat, lng: rideStateResponse.carLong)

        
        stepContainerView.addSubview(driverOnTripViewew)
        driverOnTripViewew.frame = stepContainerView.bounds
    }
    
    func driverArrivedStep(rideStateResponse:RideStateResponse){
        currentStatus = .driverArrived

        let captainArrivedView =  UINib(nibName: "CaptainArrivedView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CaptainArrivedView
        
        
        captainArrivedView.driverImgView.af_setImage(withURL: URL(string:Defaults[.driverImgUrl])!)
        
        captainArrivedView.driverNameLbl.text = Defaults[.driverName]
        captainArrivedView.carPlateLbl.text = Defaults[.carPlate]
        captainArrivedView.carModelLbl.text = Defaults[.carModel]

        stepContainerViewHieghtConstraints.constant = captainArrivedView.bounds.height
        
        stepContainerView.addSubview(captainArrivedView)
        captainArrivedView.frame = stepContainerView.bounds
    }
    
    func rateDriver(data:RideStateResponse){
        currentStatus = .rating
       router?.showTripRatingVC()
    }
    
    
    
    
    //MARK:-
    func configureMapView(){
        //Set MapView delegate
        //mapView.delegate = self
        
        //Show user location on mapview
        mapView.isMyLocationEnabled = true
    }
    
    
    private func setUpNavigationItems() {
        self.title = StringConstant.TripTitle
        let cancelBtn = UIButton(frame: CGRect(x:0,y:0,width:15,height:20))
        cancelBtn.contentMode = .scaleAspectFit
        cancelBtn.addTarget(self, action: #selector(cancel), for: UIControlEvents.touchUpInside)
        
        let img = #imageLiteral(resourceName: "map_policy_close")

        cancelBtn.setImage(img, for: .normal)
        let cancelItem = UIBarButtonItem(customView: cancelBtn)
        navigationItem.leftBarButtonItems = [cancelItem]
    }
    
    func cancel(){
        
        showTwoActionsAlert(title: StringConstant.TitleCancelRide, firstActionButtontitle: StringConstant.Yes, secondActionButtontitle: StringConstant.No, mesage: StringConstant.MsgCancelRide ) {
            self.showLoadingIndicator()
            self.modelView?.cancelRide()
            self.rideTimer?.invalidate()
            self.searchTimer?.invalidate()
        }        
    }
    
    
    func adjustPickerMarker(lat:Double, lng:Double) {
        mapView.clear()
        
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let pickupLocationMarker = GMSMarker(position: position)
        pickupLocationMarker.icon = #imageLiteral(resourceName: "map_red_car")
        
        pickupLocationMarker.map = mapView
    }
    
    
    func zoomToUserLocation() {
        //Zoom to user location
        if LocationService.sharedInstance.canGetLocation() {
            LocationService.sharedInstance.getCurrentLocation(completion: {[weak self] (coordinate) in
                
                DispatchQueue.main.async { [weak self] in
                    self?.mapView.animate(with: GMSCameraUpdate.setTarget(coordinate, zoom: 16.0))
                }
                
            }){ [weak self] error in
                self?.showDefaultAlert(title: "Info", message: error.localizedDescription)
            }
        }
        else {
            askUserToEnableLocationService()
        }
    }
    
    
    
    //MARK:- GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //Get place address of camera target coordintes
        GMSGeocoder().reverseGeocodeCoordinate(position.target) {  (response, error) in
            if let _ = error {
                print(error.debugDescription)
            }
            else {
                if  let count = response?.results()?.count {
                    if count > 0 {
                        //                        self?.addressLbl.text = response!.results()![0].lines!.joined(separator: ",")
                    }
                }
            }
        }
    }
    
    
    //MARK:- Actions
    @IBAction func zoomToUserLocation(_ sender: UIButton) {
        zoomToUserLocation()
    }
    
    @IBAction func toggleMapType(_ sender: UIButton) {
        if mapView.mapType == .normal {
            mapView.mapType = .hybrid
        }
        else {
            mapView.mapType = .normal
        }
    }
}
