//
//  TripModelView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import RealmSwift



class TripModelView: BaseModelView
{
    
    unowned let scene: TripViewController
    
    
    init(scene: TripViewController) {
        self.scene = scene
    }
    
    func addRecentPlace(placeData:[String:Any]) {
        let place = RecentPlace(value:placeData)
        RecentService.addPlaceToRecent(place: place)
    }
    
    private var rideStatusResponse:RideStateResponse?
    
    
    //MARK:- Check order status
    func checkOrderStatus(){
        let orderId = Realm.tripRealm.currentOrder?.orderId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let params:[String : Any] = ["order_id":orderId, "user_id":userId, "pick_lat":0, "pick_long":0]
        
        let service = OrderStateService()
        executeService(service: service, userData: params,headers:[:], tag: OrderStateService.className)
    }
    
    func cancelOrder(){
        
        let orderId = Realm.tripRealm.currentOrder?.orderId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        let params:[String : Any] = ["order_id":orderId, "user_id":userId]
        
        let service = CancelOrderService()
        
        executeService(service: service, userData: params,headers:[:], tag: CancelOrderService.className)
    }
    
    
    func checkRideStatus(){
        let rideId = Realm.tripRealm.currentOrder?.rideId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        
        let params:[String : Any] = ["ride_id":rideId,"user_id":userId]
        
        let service = RideStateService()
        executeService(service: service, userData: params,headers:[:], tag: RideStateService.className)
    }
    
    func rateRide(evalStars:Int, comments:String?){
        scene.showLoadingIndicator()
        let rideId = Realm.tripRealm.currentOrder?.rideId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        let params:[String : Any] = ["ride_id":rideId, "user_id":userId,"eval_stars":evalStars, "comments":comments ?? ""]
        
        let service = EvaluationService()
        
        executeService(service: service, userData: params,headers:[:], tag: EvaluationService.className)
    }
    
    func cancelRide(){
        let orderId = Realm.tripRealm.currentOrder?.rideId ?? ""
        let userId = Realm.userRealm.currentUser?.id ?? ""
        let params:[String : Any] = ["ride_id":orderId, "user_id":userId]
        
        let service = CancelRideService()
        
        executeService(service: service, userData: params,headers:[:], tag: CancelRideService.className)
    }
    
    
    
    //MARK:- Did receive result
    override func dataDidReceived(notification: NSNotification) {
        scene.hideLoadingIndicator()
        switch notification.name {
        case CancelOrderService.notificationName:
            scene.rideTimer?.invalidate()
            scene.searchTimer?.invalidate()
            
            scene.rideTimer = nil
            scene.searchTimer = nil
            
            Realm.tripRealm.clearCurrentTrip()
        case CancelRideService.notificationName:

            scene.rideTimer?.invalidate()
            scene.searchTimer?.invalidate()
            
            scene.rideTimer = nil
            scene.searchTimer = nil

            scene.showDefaultAlert(title: StringConstant.RideCancelledScucessfully, message: nil){[weak self] in
                Realm.tripRealm.clearCurrentTrip()
                self?.scene.router?.navigateToHomeScene()
            }
        case OrderStateService.notificationName:
            
            if let response =  notification.userInfo?[BaseService.DataReceivedKey] as? OrderStateResponse {
                
                print(response.orderState)
                switch response.orderState {
                case OrderStateResponse.assigned:
                    scene.driverOnWayStep(orderStateResponse: response)
                case OrderStateResponse.noDrivers:
                    self.removeAll()
                    scene.rideTimer?.invalidate()
                    scene.searchTimer?.invalidate()
                    
                    scene.rideTimer = nil
                    scene.searchTimer = nil

                    scene.radarSearchView.stopAnimating()
                    scene.showDefaultAlert(title: StringConstant.Info, message: StringConstant.MsgNoNearbyDrivers) {[weak self] in
                        Realm.tripRealm.clearCurrentTrip()
                        self?.scene.router?.navigateToHomeScene()
                    }
                case OrderStateResponse.searching:
                    scene.radarSearchView.startAnimating()
                default:
                    scene.rideTimer?.invalidate()
                    scene.searchTimer?.invalidate()
                    
                    scene.rideTimer = nil
                    scene.searchTimer = nil

                    scene.router?.navigateToHomeScene()
                }
            }
        case RideStateService.notificationName:
            if let response =  notification.userInfo?[BaseService.DataReceivedKey] as? RideStateResponse {
                self.rideStatusResponse = response
                switch response.rideState {
                case RideStateResponse.booked:
                    break
                case RideStateResponse.onWay:
                    scene.driverOnWayStep(rideStateResponse: response)
                case RideStateResponse.arrived:
                    scene.driverArrivedStep(rideStateResponse: response)
                case RideStateResponse.onTrip:
                    scene.onTripStep(rideStateResponse: response)
                case RideStateResponse.finished:
                    scene.onTripStep(rideStateResponse: response)
                case RideStateResponse.payed:
                    scene.rideTimer?.invalidate()
                    scene.rideTimer = nil
                    scene.rateDriver(data:response)
                case RideStateResponse.userCancelled:
                    scene.showDefaultAlert(title: nil, message: StringConstant.UserCancelledRide) {[weak self] in
                        self?.scene.router?.navigateToHomeScene()
                    }
                case RideStateResponse.driverCancelled:
                    scene.showDefaultAlert(title: nil, message: StringConstant.DriverCancelledRide){[weak self] in
                        self?.scene.router?.navigateToHomeScene()
                    }
                default:
                    break
                }
            }
        case EvaluationService.notificationName:
            scene.currentStatus = .tripReport
            scene.router?.showTripReport(data: rideStatusResponse)
        default:
            break
        }
        super.dataDidReceived(notification: notification)
    }
    
    override func errorDidReceived(notification: NSNotification){
        scene.hideLoadingIndicator()
//        
//        let error =  notification.userInfo?[DataReceivedWithErrorKey] as? QLError
//        
//        scene.showDefaultAlert(title: StringConstant.Info, message: error?.message)
        switch notification.name {
        case CancelOrderService.notificationName:
            Realm.tripRealm.clearCurrentTrip()
            scene.router?.navigateToHomeScene()
        case CancelRideService.notificationName:
            Realm.tripRealm.clearCurrentTrip()
            scene.router?.navigateToHomeScene()
        case EvaluationService.notificationName:
            scene.currentStatus = .tripReport
            scene.router?.showTripReport(data: rideStatusResponse)
        default:
            break
        }
    }
}
