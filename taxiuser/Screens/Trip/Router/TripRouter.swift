//
//  BookingTripRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/25/16.
//
//

import UIKit
import PopupController



class TripRouter
{
    unowned let viewController: TripViewController
    
    private var popupVC:PopupController?

    
    
    init(scene: TripViewController) {
        self.viewController = scene
    }
    
    
    func navigateToHomeScene() {

        self.viewController.present(SideMenuRootViewController.instanceFromStoryboard(storyboardName: "Main")!, animated: true, completion: nil)
    }

    func showTripRatingVC(){
        let ratingVC = TripRatingViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? TripRatingViewController
        
        ratingVC?.completionHandler = {[weak self] in
            self?.popupVC?.dismiss()
            self?.viewController.modelView?.rateRide(evalStars: Int(ratingVC!.ratingView.value), comments: ratingVC?.notes.text)
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.top),
                    .backgroundStyle(.blackFilter(alpha: 0.4)),
                    .dismissWhenTaps(false),
                    .movesAlongWithKeyboard(false)
                ]
            )
            .show(ratingVC!)
    }
    
    
    func showTripReport(data:RideStateResponse?){
        let reportVC = TripReportViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard")  as? TripReportViewController
        
        reportVC?.rideStateResponse = data
        
        reportVC?.completionHandler = {[weak self] in
            self?.popupVC?.dismiss()
            self?.navigateToHomeScene()
        }
        
        popupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4)),
                    .dismissWhenTaps(false)
                ]
            )
            .show(reportVC!)
    }
}
