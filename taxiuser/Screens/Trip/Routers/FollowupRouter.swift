//
//  FavoriteRouter.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit
import PopupController
import SwiftyUserDefaults



class FollowupRouter
{
    unowned let viewController: FollowupNumbersViewController
    
    var addFollowupPopupVC:PopupController?
    

    init(scene: FollowupNumbersViewController) {
        self.viewController = scene
    }
    
    
    
    //MARK:- FavoriteRouterInput
    func showAddFollowUpVC(){
        let addFollowupVC = AddFollowupNumberViewController.instanceFromStoryboard(storyboardName: "Trips_Storyboard") as? AddFollowupNumberViewController
        
        addFollowupVC?.completionHandler = {[weak self] in
            self?.dismissEditPopupVC()
            self?.viewController.followupNumbers = Defaults[.followupNumbers]
             self?.viewController.tableView.reloadData()
        }
        
        addFollowupPopupVC = PopupController
            .create(viewController)
            .customize(
                [
                    .animation(.fadeIn),
                    .layout(.center),
                    .backgroundStyle(.blackFilter(alpha: 0.4))
                ]
            )
            .show(addFollowupVC!)
    }
    
    func dismissEditPopupVC(){
        addFollowupPopupVC?.dismiss()
    }
}
