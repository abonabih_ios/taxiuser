//
//  DriverOnTripView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 5/3/17.
//
//

import UIKit

class DriverOnTripView: UIView
{
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
        
    @IBOutlet weak var carModelLbl: UILabel!
    
    @IBOutlet weak var carPlateLbl: UILabel!

}
