//
//  DriverView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/17/17.
//
//

import UIKit
import SwiftyUserDefaults


class DriverView: UIView
{
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    
    @IBOutlet weak var carDistanceLbl: UILabel!

    @IBOutlet weak var carTimeLbl: UILabel!

    @IBOutlet weak var carModelLbl: UILabel!

    @IBOutlet weak var carPlateLbl: UILabel!

    
    
    
    
    //Actions
    @IBAction func callDriver(_ sender: UIButton) {
        UIApplication.shared.tryOpenUrl(urls: ["tel:\(Defaults[.driverMobile])"])
    }
}
