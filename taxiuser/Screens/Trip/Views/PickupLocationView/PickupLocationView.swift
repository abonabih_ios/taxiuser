//
//  PickupLocationView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//

import UIKit

protocol PickupLocationViewDelegate:class {
    func pickPlace(pickup:Bool)
}

class PickupLocationView: UIView
{
    
    @IBOutlet weak var addressLbl: UILabel!
    
    weak var delegate:PickupLocationViewDelegate?
    
    @IBAction func pickPlace(_ sender: UIButton) {
        delegate?.pickPlace(
        pickup: sender.tag == 0)
    }
    
    func showAdress(adress:String?){
        addressLbl.text = adress
    }
}
