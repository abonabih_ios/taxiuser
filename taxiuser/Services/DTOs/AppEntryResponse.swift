//
//  AppEntryResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/27/17.
//
//

import ObjectMapper

class AppEntryResponse: Mappable
{
    /*
     (0) : UNEXPECTED ERROR : NOT "POST"
     (1) : SUCCESS
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) : ERROR
     (4) : Driver doesn't exist
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknownError
        case driverDoesnotexist
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknownError
        }
    }

    
    var code:ErrorCode = .unknownError
    var name:String = ""
    var gender:String = ""
    var email:String = ""
    var balance:Double?
    var disabled:Int = 0
    var bookedCount:Int = 0
    var update:Int = 0
    var updateUrl:String = ""
    var base:String = ""
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        code <- (map["code"],EnumTransform<ErrorCode>())
        name <- map["name"]
        gender <- map["gender"]
        email <- map["email"]
        balance <- map["balance"]
        disabled <- map["disabled"]
        bookedCount <- map["booked_count"]
        update <- map["update"]
        updateUrl <- map["update_url"]
        base <- map["base"]
    }


}
