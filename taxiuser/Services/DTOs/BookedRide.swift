//
//  BookedRide.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import ObjectMapper
import SwiftDate


enum CarType:String {
    case economy = "1"
    case busniess = "2"
}

class BookedRide: Mappable
{
    
    
    
    var rideId:String = ""
    var rideTtype:BookingType = .basic
    var carType:CarType = .economy
    var gender:Gender = .male
    var dateTime:String = ""
    var pickLong:String = "0"
    var pickLat:String = "0"
    
    var addressPick:String?
    
    var destLong:String?
    var destLat:String?
    var addresDrop:String = StringConstant.MsgWillTellTheDriver
    var notes:String?

    var dateString:String? {
        
        if dateTime.contains("0000-00-00 00:00:00") {
            return nil
        }
        
        return Date.parse(dateStr: dateTime, format: "yyyy-MM-dd hh:mm:ss").string(format: .strict("YYYY-MM-dd hh:mm a"))
    }
    
    
    var pickupLat:Double {return Double(pickLat) ?? 0 }
    var pickupLng:Double {return Double(pickLong) ?? 0 }

    var destinationLat:Double? {return Double(destLat ?? "")}
    var destinationLng:Double? {return Double(destLong ?? "")}

    
    var followUpNumbers:[[String:String]] = []

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        rideId <- map["ride_id"]
        rideTtype <- (map["ride_type"],EnumTransform<BookingType>())
        carType <- (map["car_type"],EnumTransform<CarType>())
        gender <- (map["gender"],EnumTransform<Gender>())
        dateTime <- map["date_time"]
        
        addressPick <- map["address_pick"]
        
        addresDrop <- map["address_drop"]
        notes <- map["notes"]
        
        destLong <- map["dest_long"]
        destLat <- map["dest_lat"]
        
        pickLong <- map["pick_long"]
        pickLat <- map["pick_lat"]

        followUpNumbers <- map["follow_tel"]

    }
    
}
