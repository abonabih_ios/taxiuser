//
//  BookedRidesResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import ObjectMapper

class BookedRidesResponse: Mappable
{
    
    /*
     (0) : UNEXPECTED ERROR : NOT "POST"
     (1) : SUCCESS
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) :  ERROR
     (4) : NO RIDES BOOKED FOR THE USER
     */
    
    
    
    var code:ErrorCode = .unknownError
    var results:[BookedRide] = []
    
    

    enum ErrorCode:Int {
        case unexpectedError
        case sucess
        case queryParamsMissed
        case unknownError
        case noRidesBooked
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknownError
        }
    }

    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        code <- (map["res_code"],EnumTransform<ErrorCode>())
        results <- map["booked_list"]
    }
}
