//
//  PlaceSearchResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/8/17.
//
//
import ObjectMapper


class PlaceSearchResponse:Mappable
{
    var status:Status = .zeroResults
    var results:[PlaceItem] = []
    
    
    enum Status:String {
        case ok =  "OK"
        case zeroResults =  "ZERO_RESULTS"
        case overQuota =  "OVER_QUERY_LIMIT"
        case requestDenied =  "REQUEST_DENIED"
        case invalidRequest =  "INVALID_REQUEST"
    }

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        status <- (map["status"],EnumTransform<Status>())
        results <- map["results"]
    }
    
}


