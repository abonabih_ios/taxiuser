//
//  Rate.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 4/26/17.
//
//

import ObjectMapper


class Rate: Mappable
{
    
    var rideTypeId = ""
    var nowStart = ""
    var nowMin = ""
    var nowFee  = ""
    var laterStart = ""
    var laterMin = ""
    var laterFee = ""
    var moving  = ""
    var waiting  = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        rideTypeId <- map["ride_type_id"]
        nowStart <- map["now_start"]
        nowMin <- map["now_min"]
        nowFee <- map["now_fee"]
        laterStart <- map["later_start"]
        laterMin <- map["later_min"]
        laterFee <- map["later_fee"]
        moving <- map["moving"]
        waiting <- map["waiting"]
    }
}
