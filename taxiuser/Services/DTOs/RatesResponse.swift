//
//  RatesResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 4/26/17.
//
//
import ObjectMapper


class RatesResponse: Mappable
{
    var code:ErrorCode = .unexpectedError
    var basicEconomyRates:[Rate] = []
    var basicBusinessRates:[Rate] = []
    var kidsEconomyRates:[Rate] = []
    var kidsBusinessRates:[Rate] = []

    enum ErrorCode:Int {
        case unexpectedError
        case sucess
        case queryParamsMissed
        case unexpectedError1
        case unexpectedError2
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unexpectedError
        }
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        code <- (map["res_code"],EnumTransform<ErrorCode>())
        basicEconomyRates <- map["basic_economy_rates"]
        basicBusinessRates <- map["basic_business_rates"]
        kidsEconomyRates <- map["kids_economy_rates"]
        kidsBusinessRates <- map["kids_business_rates"]
    }
}
