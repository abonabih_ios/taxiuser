//
//  RideStateResponse.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/17/17.
//
//

import Foundation
import ObjectMapper

class RideStateResponse:Mappable
{
    
    static let  booked = "0"
    static let onWay = "1"
    static let arrived = "2"
    static let onTrip = "3"
    static let finished = "4"
    static let payed = "5"
    static let userCancelled = "6"
    static let driverCancelled = "7"


    
    var rideState:String = "0"

    
    var carDistance:Double = 0
    var carTime:Int = -1
    
    var carLong:Double = 0
    var carLat:Double = 0

    var cost:String = ""
    var distance:Double = 0
    var time:String = "0"
    var waiting:String = "0"
    var paymentAmount:String = ""
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        carDistance <- map["car_distance"]
        carTime <- map["car_time"]
        carLong <- map["car_long"]
        carLat <- map["car_lat"]
        
        cost <- map["cost"]
        distance <- map["distance"]
        
        time <- map["time"]
        waiting <- map["waiting"]
        paymentAmount <- map["payment_amount"]
        
        rideState <- map["ride_state"]
    }
}
