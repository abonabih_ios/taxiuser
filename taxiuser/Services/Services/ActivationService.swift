//
//  ActivationService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import Foundation
import  RealmSwift


class ActivationService: BaseService
{
    
    
    
    
    /*
     (0) : NO ERRORS
     (1) : UNEXPECTED ERROR : NOT "POST"
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) : ERR
     (4) : ERR
     (5) : Invalid code
     (6) : User not found
     */
    
    enum ActivationError:Int {
        case no
        case unexpected
        case missedParams
        case unknown1
        case unknown2
        case invalidCode
        case userNotFound
        
        init(code:Int) {
            self = ActivationError(rawValue:code) ?? .unknown1
        }
    }
    

    
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let valid = response["valid"] as? Bool ?? false
            let erroCode = response["err_code"] as? Int ?? ActivationError.unknown1.rawValue
            
            let error = ActivationError(code:erroCode)
            
            switch error {
            case .no:
                if let user = LoggedUser.shared {
                    
                    try? Realm.userRealm.write {
                        user.verified = valid ? "1" : "0"
                    }
                }
                super.requestDidSucess(responseData: valid, requestId: requestId)
            case .invalidCode:
                requestDidFail(error: QLError(tag:requestId, code:ActivationError.invalidCode.rawValue , message:StringConstant.MsgInvalidCode), tag: requestId)
            case .userNotFound:
                requestDidFail(error: QLError(tag:requestId, code:ActivationError.userNotFound.rawValue , message:StringConstant.MsgUserNotFound), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ActivationError.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ActivationError.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
        
}
