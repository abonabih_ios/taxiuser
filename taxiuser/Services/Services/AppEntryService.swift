//
//  AppEntryService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/27/17.
//
//

import ObjectMapper
import RealmSwift
import SwiftyUserDefaults

class AppEntryService: BaseService
{
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String){
        if responseData is [String:Any] {
            
            guard let response = Mapper<AppEntryResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
                return
            }
            
            switch response.code {
            case .sucess:
                
                try!  Realm.userRealm.write() {
                    
                    Realm.userRealm.currentUser?.balance = response.balance ?? Realm.userRealm.currentUser?.balance ?? 0
                    
                    Realm.userRealm.currentUser?.name = response.name
                    Realm.userRealm.currentUser?.gender = response.gender
                    
                    Realm.userRealm.currentUser?.email = response.email
                    Realm.userRealm.currentUser?.bookedCount = response.bookedCount

                    Defaults[.baseUrl] = response.base

                }

                super.requestDidSucess(responseData: response, requestId: requestId)
            case .driverDoesnotexist:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgUserDoesNotExist), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
