//
//  BookedService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import ObjectMapper



class BookedService: BaseService
{
    
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String){
        if responseData is [String:Any] {
            
            guard let response = Mapper<BookedRidesResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
                return
            }
            
            switch response.code {
            case .sucess:
                super.requestDidSucess(responseData: response.results, requestId: requestId)
            case .noRidesBooked:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.NoBookedRidesForUser), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
