//
//  CancelBookedRideService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 5/27/17.
//
//

import UIKit

class CancelBookedRideService: BaseService
{
    /*
     *(0): UNEXPECTED ERROR : NOT "POST"
     *(1): SUCCESS
     *(2): SOME QUERY PAPRAMS ARE MISSED
     *(3): ERR DB CONNECTION
     *(4): RIDE DOES NOT EXIST
     */
    
    enum ErrorCode:Int {
        case unexpetected
        case sucess
        case queryParams
        case errorDBConnection
        case rideNotExists
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unexpetected
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unexpetected.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                super.requestDidSucess(responseData: StringConstant.RideCancelledScucessfully, requestId: requestId)
            case .rideNotExists:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgRideNotExists), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
