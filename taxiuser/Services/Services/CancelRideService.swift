//
//  CancelRideService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/2/17.
//
//

import Foundation

class CancelRideService: BaseService
{
    
    
    

    /*
     * ( 0 ): UNEXPECTED ERROR : NOT "POST"
     * ( 1 ): SUCCESS
     * ( 2 ): SOME QUERY PAPRAMS ARE MISSED
     * ( 3 ): ERROR
     * ( 4 ): RIDE DOES NOT EXIST
     * ( 5 ): ERROR
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknown
        case rideNotExists
        case unknown2

        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unknown.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                super.requestDidSucess(responseData: StringConstant.RideCancelledScucessfully, requestId: requestId)
            case .rideNotExists:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown.rawValue , message:StringConstant.MsgRideNotExists), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }

}
