//
//  CancelVerificationService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import Foundation
import RealmSwift

class CancelVerificationService: BaseService
{
    
    
    
    
    /*
     (0) : NO ERRORS
     (1) : UNEXPECTED ERROR : NOT "POST"
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) : ERR
     (4) : USER DOES NOT EXIST
     (5) : CAN NOT DELETE VERIFIED CUSTOMER ### MUST CONTACT CALL CENTER
     (6) : ERR
     */
    
    enum ErrorCode:Int {
        case no
        case unexpected
        case missedParams
        case unknown1
        case userNotExists
        case cannotDeleteVerfiedCustomer
        case unknown2
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown1
        }
    }
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let valid = response["valid"] as? Bool ?? false
            let erroCode = response["err_code"] as? Int ?? ErrorCode.unknown1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .no:
                Realm.userRealm.reset()
                super.requestDidSucess(responseData: valid, requestId: requestId)
            case .userNotExists:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.userNotExists.rawValue , message: StringConstant.MsgUserDoesNotExist), tag: requestId)
            case .cannotDeleteVerfiedCustomer:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.cannotDeleteVerfiedCustomer.rawValue , message:StringConstant.MsgCannotDeleteVerifiedUser), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
