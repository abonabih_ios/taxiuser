//
//  ChangeEmailService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/21/17.
//
//

import Foundation
import RealmSwift
import SwiftyUserDefaults


class ChangeEmailService: BaseService
{
    
    
    
    /*
     *          ( 0 )  :  UNEXPECTED ERROR : NOT "POST"
     *          ( 1 )   :   SUCCESS
     *          ( 2 )   :   SOME QUERY PAPRAMS ARE MISSED
     *          ( 3 )   :   ERROR
     *          ( 4 )   :   user not found
     *          ( 5 )   :   unkown error
     */
    
    
    enum ErrorCode:Int {
        case unexpectedError
        case sucess
        case queryParamsMissed
        case unknowError
        case userNotFound
        case unknowError2
        
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknowError
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:Any] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unknowError.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                Realm.userRealm.updateUserEmai(email: Defaults[.userNewEmail])
                Defaults[.userNewEmail] = ""
                super.requestDidSucess(responseData: true, requestId: requestId)
            case .userNotFound:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.userNotFound.rawValue , message:StringConstant.MsgUserNotFound), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknowError.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknowError.rawValue , message: StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
