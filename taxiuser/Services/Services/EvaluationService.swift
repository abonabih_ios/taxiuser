//
//  EvaluationService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/17/17.
//
//

import Foundation

class EvaluationService: BaseService
{
    /*
    (0) : UNEXPECTED ERROR : NOT "POST"
    (1) : SUCCESS
    (2) : SOME QUERY PAPRAMS ARE MISSED
    (3) : ERROR
    (4) : ERR EVAL STARS
    (5) : RIDE DOES NOT EXIST
    (6) :  ERROR
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case error1
        case errorEvalStars
        case rideDoesnotexist
        case error2

        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .error1
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.error1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                super.requestDidSucess(responseData: "Sucess", requestId: requestId)
            case .rideDoesnotexist:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.error1.rawValue , message:StringConstant.MsgRideNotExists), tag: requestId)
            case .errorEvalStars:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.error1.rawValue , message:StringConstant.MsgSpecifyRideEvaluationStars), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.error1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.error1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
