//
//  FavoriteService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/7/17.
//
//
import ObjectMapper
import RealmSwift

class FavoriteService: BaseService
{
    class func favoritePlaces()->[Place]{
        let places = Array(Realm.favoritePlacesRealm.objects(Place.self))
        return places
    }
    
    class func addPlaceToFavorites(place:Place){
        let realm = Realm.favoritePlacesRealm
        realm.addPlace(place: place)
        realm.refresh()
    }
    
    class func removePlaceFromFavorites(place:Place){
        let realm = Realm.favoritePlacesRealm
        realm.removePlace(place: place)
        realm.refresh()
    }
    
    class func updatePlace(placeData:[String:String]) {
        
    }
}
