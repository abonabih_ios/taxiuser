//
//  GetUserBalanceService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 6/17/17.
//
//

import Foundation
import RealmSwift


class GetUserBalanceService: BaseService
{
    
    
    /*
     *(0): err Req type
     *(1): SUCCESS
     *(2): SOME QUERY PAPRAMS ARE MISSED
     *(3): ERR DB CONNECTION
     *(4): USER DOES NOT EXIST
     */
    
    enum ErrorCode:Int {
        case unexpetected
        case sucess
        case queryParams
        case errorDBConnection
        case userDoesNotExist
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unexpetected
        }
    }
    
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unexpetected.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                 let balance = response["user_balance"] as? Double ??   Realm.userRealm.currentUser?.balance ?? 0
                 
                 Realm.userRealm.updateUserBalance(balance:balance)
                 
                super.requestDidSucess(responseData:balance, requestId: requestId)
            case .userDoesNotExist:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgUserDoesNotExist), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unexpetected.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
