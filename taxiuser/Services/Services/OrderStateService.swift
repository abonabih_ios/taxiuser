//
//  OrderStateService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 2/2/17.
//
//

import Foundation
import RealmSwift
import SwiftyUserDefaults



class OrderStateService: BaseService
{
    
    
    
    /*
     (0) err Req type
     (1) Success
     (2) err query param
     (3) ERROR
     (4) RIDE DOES NOT EXIST
     (5) ERROR
     */
    enum ErrorCode:Int {
        case requestType
        case sucess
        case queryParams
        case unknown1
        case rideDoesnotExist
        case unknown3
        
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown1
        }
    }
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let erroCode = response["res_code"] as? Int ?? ErrorCode.unknown1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .sucess:
                //TODO:- Persiste data if needed
                if let orderStateResponse = OrderStateResponse(JSON: response) {
                    
                    Realm.tripRealm.saveCurrentRide(rideId: orderStateResponse.rideId)
                    Defaults[.driverName] = orderStateResponse.driverName
                    Defaults[.driverImgUrl] = orderStateResponse.driverPicUrl
                    Defaults[.driverMobile] = orderStateResponse.driverMobile
                    
                    Defaults[.driverMobile] = orderStateResponse.driverMobile

                    Defaults[.carPlate] = orderStateResponse.carPlate
                  
                    Defaults[.carBrand] = orderStateResponse.carBrand
                    
                    Defaults[.carModel] = "\(orderStateResponse.carBrand) \(orderStateResponse.carModel) \(orderStateResponse.carYear)"
                    
                    super.requestDidSucess(responseData: orderStateResponse, requestId: requestId)
                }
                else {
                    requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
                }
            case .rideDoesnotExist:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.rideDoesnotExist.rawValue , message:StringConstant.MsgRideNotExists), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
