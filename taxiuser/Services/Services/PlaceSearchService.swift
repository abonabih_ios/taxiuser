//
//  PlaceSearchService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/8/17.
//
//

import Foundation
import ObjectMapper


class PlaceSearchService: BaseService
{
    
    

    
    //MARK:- Execute
    override func execute(data: [String: Any]?, headers:[String: String], tag:String) {
        
        var request:Request!
        
        
        if let _ =  data?["keyword"]  {
            request = Request(tag: "PlaceTextSearch")
        }
        else  {
            request = Request(tag: "PlaceNearbySearch")
        }
        
        request.tag = String(describing: PlaceSearchService.self)
        request.url = request.url + (encodedParamters(parameters: data!))
        
        startRequest(request: request)
        

    }

    
    func encodedParamters(parameters:[String:Any])->String {
        
        guard let keyword =  parameters["keyword"] as? String else {
        
            return "?location=\(parameters["location"]!)&radius=\(parameters["radius"]!)&key=\(parameters["key"]!)"

        }
        
        
        return "?query=\(keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)&key=\(parameters["key"]!)"

        
//        return "?location=\(parameters["location"]!)&radius=\(parameters["radius"]!)&keyword=\(keyword)&key=\(parameters["key"]!)"
        
    }
    
    
    //MARK:- Sucess
    override func processReceivedData(responseData: Any, requestId: String){
        if responseData is [String:Any] {
            
            guard let response = Mapper<PlaceSearchResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
                return
            }
            
            switch response.status {
            case .ok:
                super.requestDidSucess(responseData: response.results, requestId: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
