//
//  RatesService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 4/26/17.
//
//

import Foundation
import ObjectMapper


class RatesService: BaseService
{
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        if responseData is [String:Any] {
            
            guard let response = Mapper<RatesResponse>().map(JSON: responseData as! [String : Any]) else {
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
                return
            }
            
            switch response.code {
            case .sucess:
                super.requestDidSucess(responseData:response, requestId: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:0 , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
}
