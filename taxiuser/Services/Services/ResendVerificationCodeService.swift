//
//  ResendVerificationCodeService.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/27/17.
//
//

import Foundation

class ResendVerificationCodeService: BaseService
{
    
    
    
    
    /*
     (0) : NO ERRORS
     (1) : UNEXPECTED ERROR : NOT "POST"
     (2) : SOME QUERY PAPRAMS ARE MISSED
     (3) : ERR
     (4) : USER DOES NOT EXIST
     (5) : USER ALREADY VERIFIED
     (6) : ERR
     */
    
    enum ErrorCode:Int {
        case no
        case unexpected
        case missedParams
        case unknown1
        case userNotExists
        case alreadyVerified
        case unknown2
        
        init(code:Int) {
            self = ErrorCode(rawValue:code) ?? .unknown1
        }
    }
    
    
    
    //MARK:-
    override func processReceivedData(responseData: Any, requestId: String) {
        
        if let response =  responseData as? [String:AnyObject] {
            
            
            let valid = response["valid"] as? Bool ?? false
            let erroCode = response["err_code"] as? Int ?? ErrorCode.unknown1.rawValue
            
            let error = ErrorCode(code:erroCode)
            
            switch error {
            case .no:
                super.requestDidSucess(responseData: valid, requestId: requestId)
            case .userNotExists:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.userNotExists.rawValue , message:StringConstant.MsgUserDoesNotExist), tag: requestId)
            case .alreadyVerified:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.alreadyVerified.rawValue , message:StringConstant.MsgUserAlreadyVerified), tag: requestId)
            default:
                requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
            }
            
        }
        else {
            requestDidFail(error: QLError(tag:requestId, code:ErrorCode.unknown1.rawValue , message:StringConstant.MsgSomethingWentWrong), tag: requestId)
        }
    }
    
}
