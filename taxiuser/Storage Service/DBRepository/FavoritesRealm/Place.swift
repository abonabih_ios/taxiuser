//
//  Place.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/30/16.
//
//
import RealmSwift
import ObjectMapper

class Place: Object, Mappable
{
    
    dynamic var name: String = ""
   
    dynamic var address:String  = ""
    
    dynamic var latitude:Double  = 0
    dynamic var longitude:Double  = 0

    
    required convenience init?(map: Map) {
        self.init()
    }

    
    func mapping(map: Map) {
        name <- map["name"]
        address <- map["address"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
