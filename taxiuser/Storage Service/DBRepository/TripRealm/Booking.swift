//
//  Booking.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/30/16.
//
//
import RealmSwift
import ObjectMapper
import SwiftDate


class Booking: Object,Mappable
{
    
    
    class var  currentBookingID:String {return "CURRENT_BOOKING" }
    
    
    
    dynamic var id:String  = Booking.currentBookingID
    dynamic var orderId:String?
    dynamic var rideId:String?

    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    
    func mapping(map: Map) {
        id <- map["id"]
        orderId <- map["orderId"]
        rideId <- map["rideId"]
    }

    override class func primaryKey() -> String? {
        return "id"
    }

}
