//
//  TripRealm.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 12/30/16.
//
//

import RealmSwift

extension Realm
{
    private class var tripConfig:Configuration {
        var config = Realm.Configuration()
        config.fileURL = config.fileURL!.deletingLastPathComponent()
            .appendingPathComponent("\(Bundle.main.targetName!)_Trips.realm")
        return config
    }
    
    class var tripRealm: Realm {
        return try! Realm(configuration: Realm.tripConfig)
    }
    
    
    var currentOrder:Booking? {
        return Realm.tripRealm.objects(Booking.self).first
    }
    
    func saveCurrentOrder(orderId:String) {
        try!  Realm.tripRealm.write() {
            
            Realm.tripRealm.create(Booking.self, value: ["id":Booking.currentBookingID, "orderId":orderId], update: true)
        }
    }
    
    
    func saveCurrentRide(rideId:String) {
        try!  Realm.tripRealm.write() {
            
            Realm.tripRealm.create(Booking.self, value: ["id":Booking.currentBookingID, "rideId":rideId], update: true)
        }
    }
    
    func clearCurrentTrip(){
        try!  Realm.tripRealm.write() {
            Realm.tripRealm.deleteAll()
        }
    }
    
}
