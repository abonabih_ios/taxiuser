//
//  CoreDataStack.swift
//  Medical Reminder
//
//  Created by Abdelrahman Ahmed on 10/10/15.
//

import Foundation
import CoreData

class CoreDataStack
{
    
    
    private static let modelName = "Studio"
    private static let seedName = "Studio_Database"
    
    let context: NSManagedObjectContext
    
    
    
    // MARK: - Core Data stack
    init() {
        let coordinator = CoreDataStack.persistentStoreCoordinator
        context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
    }
    
    
    private static var applicationDocumentsDirectory: NSURL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }
    
    private static var managedObjectModel: NSManagedObjectModel {
        let modelURL = Bundle.main.url(forResource: CoreDataStack.modelName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }
    
    private static var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = CoreDataStack.applicationDocumentsDirectory.appendingPathComponent(CoreDataStack.seedName + ".sqlite")
        let failureReason = "There was an error creating or loading the application's saved data."
        do {
            
            // Declare Options
            let options = [ NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true ]
            
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }
    
    
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
                
            } catch let error as NSError {
                NSLog("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
        }
    }
    
    
    //MARK:- Reset all
    func resetAll(){
        
        let url = CoreDataStack.applicationDocumentsDirectory.appendingPathComponent(CoreDataStack.seedName + ".sqlite")
        
        do {
            try FileManager.default.removeItem(at: url!)
        }
        catch let error as NSError {
            NSLog("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        for ct in context.registeredObjects {
            context.delete(ct)
        }
        
        do {
            try CoreDataStack.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        }
        catch let error as NSError {
            NSLog("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
    }
    
    
    func deleteObject(object:NSManagedObject?){
        if let _ = object {
            context.delete(object!)
        }
    }
}
