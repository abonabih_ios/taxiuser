//
//  Defaults.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//


import SwiftyUserDefaults



extension DefaultsKeys
{
    //MARK:- Application related keys
    static let followupNumbers = DefaultsKey<[String]>("follow_up_numbers")
    
    static let userMobileNumber = DefaultsKey<String>("userMobileNumber")
    static let userNewEmail = DefaultsKey<String>("userNewEmail")

    static let driverName = DefaultsKey<String>("driverName")
    static let driverImgUrl = DefaultsKey<String>("driverImgUrl")
    static let driverMobile = DefaultsKey<String>("driverMobile")
    static let carPlate = DefaultsKey<String>("carPlate")
    static let carBrand = DefaultsKey<String>("carBrand")
    static let carModel = DefaultsKey<String>("carModel")

    static let notes = DefaultsKey<String>("notes")

    static let baseUrl = DefaultsKey<String>("baseUrl")

}
