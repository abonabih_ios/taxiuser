//
//  ABSwipeableCellAnimation.swift
//
//  Created by abdelrahman shaheen on 7/18/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

extension ABSwipeableTableViewCell
{
    
    //MARK:- Animation methods
    func onReleaseSwipingAnimation() {
        if isDraggedToFarRight() {
            moveRightAnimation()
        } else if isDraggedToFarLeft() {
            moveLeftAnimation()
        } else {
            moveToOriginalFrameAnimation()
        }
    }
    
    func moveRightAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
            self.contentView.frame = self.mostRightFrame()
            self.leftActionsHidden = false

            }) { (Bool) -> Void in
        }
    }
    
    func moveLeftAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
            self.contentView.frame = self.mostLeftFrame()
            self.rightActionsHidden = false

            }) { (Bool) -> Void in
        }
    }
    
    func moveToOriginalFrameAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            self.contentView.frame = self.originalFrame()
            
            }) { (Bool) -> Void in
                self.leftActionsHidden = true
                self.rightActionsHidden = true
        }
    }
    
    
    
    //MARK:- Swipeable cell original , right most, and left most frames
    private func originalFrame() -> CGRect {
        return CGRect(x: 0, y: contentView.frame.origin.y,
            width: contentView.bounds.width, height: contentView.bounds.height)
    }
    
    private func mostLeftFrame() -> CGRect {
        var x: CGFloat = 0
        if let cellRightActions = rightActions {
            x =  -CGFloat(cellRightActions.count) * bounds.height
        }
        return CGRect(x: x, y: contentView.frame.origin.y,
            width: contentView.bounds.width, height: contentView.bounds.height)
    }
    
    private func mostRightFrame() -> CGRect {
        var x: CGFloat =  0
        if let cellLeftActions = leftActions {
            x = CGFloat(cellLeftActions.count) * bounds.height
        }
        return CGRect(x: x, y: contentView.frame.origin.y,
            width: contentView.bounds.width, height: contentView.bounds.height)
    }
    
    
}
