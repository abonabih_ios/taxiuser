//
//  ABUITableViewCellRowAction.swift
//  CustomRowActionCell
//
//  Created by abdelrahman shaheen on 7/16/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

class ABUITableViewCellRowAction: NSObject
{
    typealias  ABUITableViewCellRowActionHandler = () -> Void
    
    var image: UIImage?
    var backgroundColor: UIColor?
    var title: String?
    var handler: ABUITableViewCellRowActionHandler
    
    
    init(title: String?, handler: @escaping ABUITableViewCellRowActionHandler) {
        self.handler = handler
        self.title = title
        super.init()
    }
    
    func executeAction() {
        handler()
    }
}
