

//
//  RowActionButton.swift
//
//  Created by abdelrahman shaheen on 7/18/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

extension UIButton
{
    convenience init(rowAction: ABUITableViewCellRowAction) {
        self.init()
        setTitle(rowAction.title, for: .normal)
        backgroundColor = rowAction.backgroundColor
        setImage(rowAction.image, for: .normal)
        addTarget(rowAction, action: Selector(("executeAction")), for: UIControlEvents.touchUpInside)
    }
}
