//
//  UIViewUtility.swift
//  Medical Reminder
//
//  Created by abdelrahman shaheen on 7/20/15.
//  Copyright (c) 2015 NTG Clarity. All rights reserved.
//

import UIKit

extension UIView
{
    func touchedViewInside(point:CGPoint, withEvent event:UIEvent?) -> UIView {
        for subview in subviews {
            let convertedPoint = subview.convert(point, from:self)
            let hitTestView = subview.hitTest(convertedPoint, with: event)
            if (hitTestView != nil) {
                return hitTestView!
            }
        }
        return self
    }
    
}
