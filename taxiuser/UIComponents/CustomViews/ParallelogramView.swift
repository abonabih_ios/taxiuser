//
//  ParallelogramView.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 10/18/16.
//   All rights reserved.
//

import UIKit

@IBDesignable
class ParallelogramView: UIView
{
    @IBInspectable var startX:CGFloat = 0.0
    
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateLayer()
    }
    
    func updateLayer() {
        let parallelogramPath = UIBezierPath()
        parallelogramPath.move(to: CGPoint(x: startX, y: 0))
        parallelogramPath.addLine(to: CGPoint(x: bounds.width, y: 0))
        parallelogramPath.addLine(to: CGPoint(x: bounds.width - startX, y: bounds.height))
        parallelogramPath.addLine(to: CGPoint(x: 0, y: bounds.height))
        parallelogramPath.close()
        parallelogramPath.fill()
        
        layer.shadowPath = parallelogramPath.cgPath
        layer.shadowOpacity = 3.0

        layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        layer.shadowColor = UIColor.red.cgColor
        layer.shadowRadius = 5
        layer.masksToBounds = false
    }
}
