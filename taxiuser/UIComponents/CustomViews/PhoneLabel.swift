//
//  PhoneLabel.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 1/14/16.
//   All rights reserved.
//

import UIKit
import Cent


class PhoneLabel: UnderlinedLabel
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK:- setup label
    func setup(){
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        addGestureRecognizer(tapRecognizer)
        isUserInteractionEnabled = true
    }
    
    
    func tap(_ recognizer:UITapGestureRecognizer){
        callNumber(text!)
    }
    
    
    fileprivate func callNumber(_ phoneNumber:String) {
        if  phoneNumber =~ "" {
            let phone = "tel://\(phoneNumber)"
            let url:URL = URL(string:phone)!
            UIApplication.shared.openURL(url)
        }
    }
}
