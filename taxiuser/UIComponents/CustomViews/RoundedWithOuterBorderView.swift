//
//  RoundedImageWithOuterBorder.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 1/19/16.
//   All rights reserved.
//

import UIKit

@IBDesignable
class RoundedWithOuterBorderView: UIView
{

    @IBInspectable var masksToBounds: Bool    = false { didSet{ updateButtonLayer() } }
    @IBInspectable var cornerRadius : CGFloat = 0 { didSet{ updateButtonLayer() } }
    @IBInspectable var shadowRadius : CGFloat = 0 { didSet{ updateButtonLayer() } }
    @IBInspectable var borderWidth : CGFloat = 0 { didSet{ updateButtonLayer() } }
    @IBInspectable var borderColor : UIColor = UIColor.black { didSet{ updateButtonLayer() } }
    @IBInspectable var color: UIColor = UIColor.clear   { didSet{ updateButtonLayer() } }
    @IBInspectable var outlineOffset: CGFloat = 0
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateButtonLayer()
        clipsToBounds = true
        
        let rect = CGRect(x: bounds.origin.x + outlineOffset , y: bounds.origin.y + outlineOffset , width: bounds.width - 4 * outlineOffset , height: bounds.height - 4 * outlineOffset)
        
        let shape = CAShapeLayer()
        shape.frame = rect
        shape.path = UIBezierPath(roundedRect: rect, cornerRadius: layer.cornerRadius - outlineOffset).cgPath;
        
        shape.fillColor = color.cgColor
        layer.addSublayer(shape)
    }
    
    func updateButtonLayer() {
        layer.masksToBounds = masksToBounds
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 0
        layer.borderColor = borderColor.cgColor
    }
}
