//
//  UITextField.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/9/17.
//
//
import UIKit

@IBDesignable
extension UITextField{
    
    class func connectFields(fields:[UITextField], lastKeyType:UIReturnKeyType, lastAction:Selector) {
        guard let last = fields.last else {
            return
        }
        
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        
        last.returnKeyType = lastKeyType
        
        
        
        last.addTarget(last.delegate, action: lastAction , for: .editingDidEndOnExit)
        
    }

    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}
