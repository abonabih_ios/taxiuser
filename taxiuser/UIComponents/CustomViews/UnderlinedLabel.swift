//
//  UnderlinedLabel.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 7/3/16.
//   All rights reserved.
//

import UIKit

enum Alighment {
    case ltr
    case rtl
}


@IBDesignable
class UnderlinedLabel: UILabel
{
    @IBInspectable var undelined:Bool = true {didSet{underLine(undelined)}}
    
    override var text: String? {didSet{underLine(undelined)}}
    
    
    override init(frame:CGRect){
        super.init(frame: frame)
        underLine(undelined)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        underLine(undelined)
    }
    
    
    fileprivate func underLine(_ underLinded:Bool){
        
        var underlineAttribute:[String:Int] = [:]
        
        if underLinded {
            underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        }
        else {
            underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleNone.rawValue]
        }
        
        if let _  = text {
            let underlineAttributedString = NSAttributedString(string: text!, attributes: underlineAttribute)
            attributedText = underlineAttributedString
        }
    }
}
