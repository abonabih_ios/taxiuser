//
//  UIBlocker.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/12/17.
//
//

import UIKit
import NVActivityIndicatorView


extension UIViewController
{
    func blockUI(){
        let activityData = ActivityData()
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func unblockUI(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    
}
