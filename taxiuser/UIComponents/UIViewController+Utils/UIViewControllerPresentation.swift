//
//  UIViewControllerPresentation.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 2/27/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import UIKit


extension UIViewController
{
    func present(vc:UIViewController, style:String) {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.window?.layer.add(transition, forKey: kCATransition)
        present(vc, animated: false, completion: nil)
    }
    

}
