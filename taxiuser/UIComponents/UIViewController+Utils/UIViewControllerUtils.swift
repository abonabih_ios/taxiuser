//
//  UIViewControllerUtils.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 11/1/16.
//   All rights reserved.
//

import UIKit
import CoreLocation
import SVProgressHUD


extension UIViewController
{
    //MARK:- Factory method
    class func instanceFromStoryboard<T:UIViewController>(storyboardName:String)->T? {
        let vc = viewControllerWithIdentifier(storyboardId: String(describing: classForCoder()), onStoryboardWithName:storyboardName) as? T
        return vc
    }
    
    
    //MARK:- init from xib
    static func instanceWithDefaultNib() -> Self {
        let className = String(describing: classForCoder())
        let bundle = Bundle(for: self as AnyClass)
        return self.init(nibName: className, bundle: bundle)
    }
    
    
    
    //MARK:- ViewController from storyboard
    class func viewControllerWithIdentifier(storyboardId:String, onStoryboardWithName name:String)->UIViewController{
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        return viewController
    }
    
    
    //MARK:- Show alert views
    func showThreeActionsAlert(title:String?,firstActionButtontitle:String?, secondActionButtontitle:String?,thirdActionButtontitle:String?,message:String?,onCancel:(()->Void)? = nil,secondActionClosure:@escaping ()->Void,thirdActionClosure: @escaping ()->Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let alertSecondAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            secondActionClosure()
        }
        
        alertController.addAction(alertSecondAction)
        
        let alertThirdAction = UIAlertAction(title:thirdActionButtontitle, style: .default) { (action) in
            thirdActionClosure()
        }
        
        alertController.addAction(alertThirdAction)
        
        
        let cancelAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true) {
        }
    }
    
    func showTwoActionsAlert(title:String?,firstActionButtontitle:String?, secondActionButtontitle:String?,mesage:String?,onCancel:(()->Void)?
        
        
        = nil,actionClosure:@escaping ()->Void){
        
        let alertController = UIAlertController(title: title, message: mesage, preferredStyle: .alert)
        
        
        let alertAction = UIAlertAction(title:firstActionButtontitle, style: .default) { (action) in
            actionClosure()
        }
        
        alertController.addAction(alertAction)
        
        
        let cancelAction = UIAlertAction(title:secondActionButtontitle, style: .default) { (action) in
            onCancel?()
        }
        
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true) {
        }
    }
    
    func showDefaultAlert(title:String?,message:String?, actionBlock:(()->Void)? = nil){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: StringConstant.Ok, style: .cancel
        ) { (action) in
            alertController.dismiss(animated: true){
            }
            actionBlock?()
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
        }
    }


    
    func showLoadingIndicator(_ blockView: Bool = true) {
        if let view = UIApplication.shared.keyWindow?.rootViewController?.view {
            SVProgressHUD.setContainerView(view)
            view.isUserInteractionEnabled = !blockView
            SVProgressHUD.show()
            
        }
    }
    
    
    func hideLoadingIndicatorWithError(){
        SVProgressHUD.showError(withStatus: "    ")
        UIApplication.shared.keyWindow?.rootViewController?.view.isUserInteractionEnabled = true
    }
    
    func hideLoadingIndicator(){
        SVProgressHUD.dismiss()
        UIApplication.shared.keyWindow?.rootViewController?.view.isUserInteractionEnabled = true
    }
    
    
    
    func askUserToEnableLocationService(){
        

        showTwoActionsAlert(title: StringConstant.EnableGPS, firstActionButtontitle: StringConstant.Settings, secondActionButtontitle: StringConstant.Cancel, mesage: StringConstant.MsgEnableLocationService) {
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }        
    }
    
}

















