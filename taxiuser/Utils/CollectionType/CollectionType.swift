//
//  CollectionType.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 12/22/15.
//   All rights reserved.
//

import Foundation

func randomUpTo(n: Int) -> Int {
    return Int(arc4random_uniform(UInt32(n)))
}

extension Collection {
    func chooseRandom(n : Int = Int.max) -> [Generator.Element] {
        var values = Array(self)
        for index in values.indices.dropFirst().reversed().prefix(n) {
            swap(&values[randomUpTo(n: index)], &values[index])
        }
        return Array(values.suffix(n))
    }
}

