//
//  Double.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 3/18/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation


extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension Int {
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

