//
//  Extensions.swift
//  taxiuser
//
//  Created by abonabih abonabih on 2/25/19.
//

import Foundation
import UIKit

extension UILabel{

    var defaultFont: UIFont? {
        get { return self.font }
        set {
            let oldFontSize = self.font.pointSize
            let oldFontName = self.font.fontName
            var newFontName  = newValue?.fontName
//            if oldFontName == Constants.AppInfo.fontAwesomeName || self.tag == -5{
//                newFontName = oldFontName
//            }
            self.font = UIFont(name: newFontName!, size: oldFontSize)
            
        }
    }
}
