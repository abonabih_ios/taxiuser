//
//  MapsUtils.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 1/6/16.
//  Copyright © 2016 IDS. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapsUtils: NSObject
{
    
    
    static func openAppleMapsWithLocation(location:CLLocationCoordinate2D,placeName:String?){
        
        let mapMark = MKPlacemark(coordinate: location,addressDictionary:nil)
        
        let regionDistance: CLLocationDistance = 1000
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(location, regionDistance, regionDistance)
        
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        
        let mapItem = MKMapItem(placemark: mapMark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
    
    
    static func directionsToLocationUsingAppleMaps(currentLocation: CLLocationCoordinate2D, location:CLLocationCoordinate2D, placeName:String){
        
        let locationMark = MKPlacemark(coordinate: location,addressDictionary:nil)
        
        let regionDistance: CLLocationDistance = 1000
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(location, regionDistance, regionDistance)
        
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        
        let locationMapItem = MKMapItem(placemark: locationMark)
        locationMapItem.name = placeName
        
        MKMapItem.openMaps(with: [locationMapItem], launchOptions: options)
    }
    
    
    static func directionsToLocationUsingAppleMaps(location1: CLLocationCoordinate2D, location2:CLLocationCoordinate2D, firstPlaceName:String, secondPlaceName:String){
        
        let locationMark1 = MKPlacemark(coordinate: location1,addressDictionary:nil)
        let locationMark2 = MKPlacemark(coordinate: location2,addressDictionary:nil)

        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        
        let locationMapItem = MKMapItem(placemark: locationMark1)
        locationMapItem.name = firstPlaceName
        
        let locationMapItem2 = MKMapItem(placemark: locationMark2)
        locationMapItem2.name = secondPlaceName

        
        MKMapItem.openMaps(with: [locationMapItem, locationMapItem2], launchOptions: options)
    }

    
    
    //MARK:- Google Maps
    static func openGoogleMapsWithLocation(location:CLLocationCoordinate2D,placeName:String?){
                
        UIApplication.shared.tryOpenUrl(urls: ["comgooglemaps://?daddr=\(location.latitude),\(location.longitude)"
            ,"https://maps.google.com/maps?f=d&daddr=\(location.latitude),\(location.longitude)&sspn=0.2,0.1&nav=1"])
    }
    
    
    static func directionsToLocationUsingGoogleMaps(location1: CLLocationCoordinate2D, location2:CLLocationCoordinate2D, firstPlaceName:String, secondPlaceName:String){
        
        UIApplication.shared.tryOpenUrl(urls:["comgooglemaps://?saddr=\(location1.latitude),\(location1.longitude)&daddr=\(location2.latitude),\(location2.longitude)"
            ,"https://maps.google.com/maps?f=d&saddr=\(location1.latitude),\(location1.longitude)&daddr=\(location2.latitude),\(location2.longitude)&sspn=0.2,0.1&nav=1"])
    }
}
