//
//  Log.swift
//  Studio
//
//  Created by Abdelrahman Ahmed on 2/25/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation

enum LogTag: String {
       case info = "Info"
        case error = "Error"
}


func printLog(tag:LogTag, data:Any){
    if AppConfigurations.appMode == .debug {
        
        let sign = tag == .info ? "😎" : "😱"
        print("\(sign):\(data)")
    }
}
