//
//  MathUtility.swift
//  icity
//
//  Created by Abdelrahman Ahmed on 6/13/16.
//   All rights reserved.
//

import UIKit

class MathUtility
{
    class func chordRadii(chordWidth: CGFloat, dh:CGFloat) -> CGFloat {
        return (pow(dh, 2) +  pow(chordWidth, 2) / 4) / (2 * dh)
    }
    
    class func frameWith(radius:CGFloat, andChordWidth width:CGFloat, andHieght height:CGFloat)->CGRect {
        return CGRect(x: (width / 2) - radius, y: height -  2 * radius, width:  2 * radius, height:  2 * radius)
    }

    class func frameWith(radius:CGFloat, andChordWidth width:CGFloat)->CGRect {
        return CGRect(x: (width / 2) - radius, y: 0, width:  2 * radius, height:  2 * radius)
    }
}


