//
//  Notification.swift
//  Studio
//
//  Created by Chestnut User on 2/14/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation


extension Notification
{
    
    
    init(name:String) {
            let notificationName = Notification.Name(rawValue: name)
            self =  Notification(name: notificationName)
    }
    
    
    init(name:String, userInfo:[String:Any]) {
        let notificationName = Notification.Name(rawValue: name)
        self =  Notification(name: notificationName, userInfo:userInfo)
    }

    
}




extension NSNotification.Name
{
    
    static let reachable = Notification.Name(rawValue:"Reachable")
    static let notReachable = Notification.Name(rawValue:"NotReachable")
    
}
