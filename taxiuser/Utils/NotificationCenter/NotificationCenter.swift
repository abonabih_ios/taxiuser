//
//  NotificationCenter.swift
//  Studio
//
//  Created by Chestnut User on 2/14/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Foundation


import Foundation

extension NotificationCenter
{
    func post(nameString:String) {
        NotificationCenter.default.post(Notification(name:nameString))
    }

    func post(name:Notification.Name) {
        NotificationCenter.default.post(Notification(name:name))
    }

}
