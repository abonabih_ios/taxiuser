//
//  Regex.swift
//  Studio
//
//  Created by Chestnut User on 2/20/17.
//  Copyright © 2017 Chestnut Learning. All rights reserved.
//

import Cent

extension Regex
{
    static let emailRegex = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$"
    
    static let passwordRegex = "^.{6,15}"

    
    static let nameRegex = "^.{2,15}$"
    
    
    static let userNameRegex = "^[0-9a-zA-Z\\_]{7,18}$"

    
    

}
