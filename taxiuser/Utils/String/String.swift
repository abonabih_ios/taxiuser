//
//  String.swift
//  taxiuser
//
//  Created by Abdelrahman Ahmed on 1/26/17.
//
//

import Foundation
import Cent


extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}



extension String {
    
    func validateWith(pattern:String)->Bool {
        let regex = Cent.Regex.init(pattern)
        return regex.test(testStr: self)
    }
    
    
    
    func isValidEmail()->Bool {
        return validateWith(pattern: Regex.emailRegex)
    }
    
    
    func isValidPassword()->Bool {
        return validateWith(pattern: Regex.passwordRegex)
    }

    
    func isValidName()->Bool {
        return validateWith(pattern: Regex.nameRegex)
    }
    
    
    func isValidUserName()->Bool {
        return validateWith(pattern: Regex.userNameRegex)
    }
}
